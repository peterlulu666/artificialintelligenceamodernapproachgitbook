<html>
<head><meta http-equiv=Content-Type content="text/html; charset=UTF-8">
<style type="text/css">
<!--
span.cls_002{font-family:"Calibri",serif;font-size:44.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_002{font-family:"Calibri",serif;font-size:44.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_003{font-family:"Calibri",serif;font-size:32.0px;color:rgb(137,136,137);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_003{font-family:"Calibri",serif;font-size:32.0px;color:rgb(137,136,137);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_004{font-family:Arial,serif;font-size:27.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_004{font-family:Arial,serif;font-size:27.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_005{font-family:"Calibri",serif;font-size:27.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_005{font-family:"Calibri",serif;font-size:27.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_029{font-family:"Calibri",serif;font-size:27.1px;color:rgb(0,0,255);font-weight:normal;font-style:normal;text-decoration: underline}
div.cls_029{font-family:"Calibri",serif;font-size:27.1px;color:rgb(0,0,255);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_007{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_007{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_008{font-family:"Calibri",serif;font-size:24.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_008{font-family:"Calibri",serif;font-size:24.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_030{font-family:"Calibri",serif;font-size:24.1px;color:rgb(0,0,255);font-weight:normal;font-style:normal;text-decoration: underline}
div.cls_030{font-family:"Calibri",serif;font-size:24.1px;color:rgb(0,0,255);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_010{font-family:"Calibri",serif;font-size:16.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_010{font-family:"Calibri",serif;font-size:16.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_011{font-family:Arial,serif;font-size:30.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_011{font-family:Arial,serif;font-size:30.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_012{font-family:"Calibri",serif;font-size:30.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_012{font-family:"Calibri",serif;font-size:30.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_013{font-family:Arial,serif;font-size:26.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_013{font-family:Arial,serif;font-size:26.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_014{font-family:"Calibri",serif;font-size:26.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_014{font-family:"Calibri",serif;font-size:26.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_015{font-family:Arial,serif;font-size:22.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_015{font-family:Arial,serif;font-size:22.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_016{font-family:"Calibri",serif;font-size:22.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_016{font-family:"Calibri",serif;font-size:22.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_017{font-family:"Calibri",serif;font-size:26.0px;color:rgb(0,0,255);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_017{font-family:"Calibri",serif;font-size:26.0px;color:rgb(0,0,255);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_031{font-family:"Calibri",serif;font-size:26.0px;color:rgb(0,0,255);font-weight:normal;font-style:normal;text-decoration: underline}
div.cls_031{font-family:"Calibri",serif;font-size:26.0px;color:rgb(0,0,255);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_018{font-family:Arial,serif;font-size:18.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_018{font-family:Arial,serif;font-size:18.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_019{font-family:"Calibri",serif;font-size:18.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_019{font-family:"Calibri",serif;font-size:18.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_032{font-family:"Calibri",serif;font-size:18.1px;color:rgb(0,0,255);font-weight:normal;font-style:normal;text-decoration: underline}
div.cls_032{font-family:"Calibri",serif;font-size:18.1px;color:rgb(0,0,255);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_021{font-family:Arial,serif;font-size:15.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_021{font-family:Arial,serif;font-size:15.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_022{font-family:"Calibri",serif;font-size:15.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_022{font-family:"Calibri",serif;font-size:15.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_023{font-family:Arial,serif;font-size:13.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_023{font-family:Arial,serif;font-size:13.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_024{font-family:"Calibri",serif;font-size:13.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_024{font-family:"Calibri",serif;font-size:13.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_025{font-family:Arial,serif;font-size:32.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_025{font-family:Arial,serif;font-size:32.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_026{font-family:"Calibri",serif;font-size:32.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_026{font-family:"Calibri",serif;font-size:32.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_027{font-family:Arial,serif;font-size:28.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_027{font-family:Arial,serif;font-size:28.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_028{font-family:"Calibri",serif;font-size:28.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_028{font-family:"Calibri",serif;font-size:28.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
-->
</style>
<script type="text/javascript" src="CourseDetails/wz_jsgraphics.js"></script>
</head>
<body>
<div style="position:absolute;left:50%;margin-left:-360px;top:0px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="CourseDetails/background1.jpg" width=720 height=540></div>
<div style="position:absolute;left:84.25px;top:169.84px" class="cls_002"><span class="cls_002">CSE 4308: Artificial Intelligence</span></div>
<div style="position:absolute;left:73.69px;top:222.88px" class="cls_002"><span class="cls_002">CSE 5360: Artificial Intelligence I</span></div>
<div style="position:absolute;left:150.51px;top:307.60px" class="cls_003"><span class="cls_003">Vamsikrishna Gopikrishna, Ph.D.</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:550px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="CourseDetails/background2.jpg" width=720 height=540></div>
<div style="position:absolute;left:150.51px;top:37.12px" class="cls_002"><span class="cls_002">Welcome to the Course</span></div>
<div style="position:absolute;left:43.20px;top:121.56px" class="cls_004"><span class="cls_004">•</span><span class="cls_005"> Course Website:</span></div>
<div style="position:absolute;left:70.20px;top:147.48px" class="cls_029"><span class="cls_029"> </span><A HREF="http://crystal.uta.edu/~gopikrishnav/classes/2021/fall/4308_5360/">http://crystal.uta.edu/~gopikrishnav/classes/2022/spri</A> </div>
<div style="position:absolute;left:70.20px;top:173.64px" class="cls_029"><span class="cls_029"> </span><A HREF="http://crystal.uta.edu/~gopikrishnav/classes/2021/fall/4308_5360/">ng/4308_5360/</A> </div>
<div style="position:absolute;left:79.20px;top:206.64px" class="cls_007"><span class="cls_007">-</span><span class="cls_008"> Too Long?: Try </span><A HREF="http://crystal.uta.edu/~gopikrishnav/">http://crystal.uta.edu/~gopikrishnav/</A> </span></div>
<div style="position:absolute;left:101.70px;top:229.68px" class="cls_008"><span class="cls_008">Textbook: Artificial Intelligence: A Modern Approach (4</span><span class="cls_010"><sup>th</sup></span></div>
<div style="position:absolute;left:101.70px;top:252.48px" class="cls_008"><span class="cls_008">Edition) - Stuart Russel, Peter Norvig.</span></div>
<div style="position:absolute;left:79.20px;top:281.52px" class="cls_007"><span class="cls_007">-</span><span class="cls_008"> 3</span><span class="cls_010"><sup>rd</sup></span><span class="cls_008"> or 2</span><span class="cls_010"><sup>nd</sup></span><span class="cls_008"> Edition is also OK</span></div>
<div style="position:absolute;left:43.20px;top:309.48px" class="cls_004"><span class="cls_004">•</span><span class="cls_005"> Instructor: Vamsikrishna Gopikrishna</span></div>
<div style="position:absolute;left:79.20px;top:342.48px" class="cls_007"><span class="cls_007">-</span><span class="cls_008"> PhD (CS), UTA (2016); MS (CE), UTA (2008); BE (CSE), Anna</span></div>
<div style="position:absolute;left:101.70px;top:365.52px" class="cls_008"><span class="cls_008">Univ. (2006)</span></div>
<div style="position:absolute;left:79.20px;top:394.56px" class="cls_007"><span class="cls_007">-</span><span class="cls_008"> Research Areas: Pattern Recognition, Neural Networks,</span></div>
<div style="position:absolute;left:101.70px;top:417.60px" class="cls_008"><span class="cls_008">Computer Vision, AI.</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:1100px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="CourseDetails/background3.jpg" width=720 height=540></div>
<div style="position:absolute;left:150.51px;top:37.12px" class="cls_002"><span class="cls_002">Welcome to the Course</span></div>
<div style="position:absolute;left:43.20px;top:121.68px" class="cls_011"><span class="cls_011">•</span><span class="cls_012"> Office Hours</span></div>
<div style="position:absolute;left:79.20px;top:157.60px" class="cls_013"><span class="cls_013">-</span><span class="cls_014"> Mon, Wed:</span></div>
<div style="position:absolute;left:115.20px;top:188.48px" class="cls_015"><span class="cls_015">•</span></div>
<div style="position:absolute;left:133.20px;top:188.48px" class="cls_016"><span class="cls_016">11:00 AM to 12:30 PM in ERB 553</span></div>
<div style="position:absolute;left:79.20px;top:214.48px" class="cls_013"><span class="cls_013">–</span><span class="cls_014"> Want to meet over TEAMS instead. Follow link on</span></div>
<div style="position:absolute;left:101.70px;top:239.68px" class="cls_014"><span class="cls_014">canvas to get to the Meeting room.</span></div>
<div style="position:absolute;left:79.20px;top:271.60px" class="cls_013"><span class="cls_013">-</span><span class="cls_014"> Can’t make it?: email me for an appointment or just</span></div>
<div style="position:absolute;left:101.70px;top:295.60px" class="cls_014"><span class="cls_014">message me on teams</span></div>
<div style="position:absolute;left:43.20px;top:327.60px" class="cls_011"><span class="cls_011">•</span><span class="cls_012"> Contact Email</span></div>
<div style="position:absolute;left:79.20px;top:363.52px" class="cls_013"><span class="cls_013">-</span><span class="cls_017"> </span><span class="cls_031">vamsikrishna.gopikrishna@uta.edu</span></div>
<div style="position:absolute;left:79.20px;top:394.48px" class="cls_013"><span class="cls_013">-</span><span class="cls_014"> Make sure to put CSE4308-001, CSE4308-002,</span></div>
<div style="position:absolute;left:101.70px;top:419.68px" class="cls_014"><span class="cls_014">CSE4308-003, CSE5360-001, CSE5360-002, CSE5360-</span></div>
<div style="position:absolute;left:101.70px;top:444.64px" class="cls_014"><span class="cls_014">003, CSE5360-004 in the subject line</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:1650px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="CourseDetails/background4.jpg" width=720 height=540></div>
<div style="position:absolute;left:150.51px;top:37.12px" class="cls_002"><span class="cls_002">Welcome to the Course</span></div>
<div style="position:absolute;left:43.20px;top:124.56px" class="cls_018"><span class="cls_018">•</span></div>
<div style="position:absolute;left:70.20px;top:124.56px" class="cls_019"><span class="cls_019">All assignments must be electronically submitted through canvas</span></div>
<div style="position:absolute;left:70.20px;top:141.60px" class="cls_019"><span class="cls_019">(</span><A HREF="uta.instructure.com">uta.instructure.com</A> </span><span class="cls_019">)</span></div>
<div style="position:absolute;left:43.20px;top:163.68px" class="cls_018"><span class="cls_018">•</span></div>
<div style="position:absolute;left:70.20px;top:163.68px" class="cls_019"><span class="cls_019">Written Assignments</span></div>
<div style="position:absolute;left:79.20px;top:185.64px" class="cls_021"><span class="cls_021">-</span><span class="cls_022">  Type in Word or Typeset in LaTeX</span></div>
<div style="position:absolute;left:115.20px;top:203.48px" class="cls_023"><span class="cls_023">•</span><span class="cls_024">  Save as PDF</span></div>
<div style="position:absolute;left:79.20px;top:218.52px" class="cls_021"><span class="cls_021">-</span><span class="cls_022">  or Handwrite and Scan</span></div>
<div style="position:absolute;left:115.20px;top:236.60px" class="cls_023"><span class="cls_023">•</span><span class="cls_024">  Make sure text is legible on written tasks (Scan at at-least 300 dpi resolution)</span></div>
<div style="position:absolute;left:115.20px;top:252.68px" class="cls_023"><span class="cls_023">•</span><span class="cls_024">  Scan to PDF (preferred) or PNG images.</span></div>
<div style="position:absolute;left:79.20px;top:267.48px" class="cls_021"><span class="cls_021">-</span><span class="cls_022">  If multiple files use zip format to compress</span></div>
<div style="position:absolute;left:43.20px;top:285.60px" class="cls_018"><span class="cls_018">•</span></div>
<div style="position:absolute;left:70.20px;top:285.60px" class="cls_019"><span class="cls_019">Programming Mini-Projects</span></div>
<div style="position:absolute;left:79.20px;top:307.56px" class="cls_021"><span class="cls_021">-</span><span class="cls_022">  Programming tasks must be coded in base versions of C, C++, Java, Python 2 or Python 3 (No</span></div>
<div style="position:absolute;left:101.70px;top:321.48px" class="cls_022"><span class="cls_022">additional packages or APIs unless cleared with instructor/TA first)</span></div>
<div style="position:absolute;left:79.20px;top:339.48px" class="cls_021"><span class="cls_021">-</span><span class="cls_022">  All files must be compressed into zip archives</span></div>
<div style="position:absolute;left:79.20px;top:357.48px" class="cls_021"><span class="cls_021">-</span><span class="cls_022">  Recommendation: Try and make sure your code runs on omega for ease of testing/evaluation</span></div>
<div style="position:absolute;left:101.70px;top:372.60px" class="cls_022"><span class="cls_022">(not required for full credit).</span></div>
<div style="position:absolute;left:43.20px;top:390.48px" class="cls_018"><span class="cls_018">•</span></div>
<div style="position:absolute;left:70.20px;top:390.48px" class="cls_019"><span class="cls_019">Late submissions will be penalized 5% of assmt credit for every hour late</span></div>
<div style="position:absolute;left:79.20px;top:411.48px" class="cls_021"><span class="cls_021">-</span><span class="cls_022">  No Late submission after 12 hours after deadline.</span></div>
<div style="position:absolute;left:79.20px;top:429.48px" class="cls_021"><span class="cls_021">-</span><span class="cls_022">  Some assignments will not allow late submissions (will be notified in class)</span></div>
<div style="position:absolute;left:43.20px;top:447.60px" class="cls_018"><span class="cls_018">•</span></div>
<div style="position:absolute;left:70.20px;top:447.60px" class="cls_019"><span class="cls_019">Any additional instructions will be provided in assignments</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:2200px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="CourseDetails/background5.jpg" width=720 height=540></div>
<div style="position:absolute;left:150.51px;top:37.12px" class="cls_002"><span class="cls_002">Welcome to the Course</span></div>
<div style="position:absolute;left:43.20px;top:127.60px" class="cls_025"><span class="cls_025">•</span><span class="cls_026"> Attendance</span></div>
<div style="position:absolute;left:79.20px;top:172.64px" class="cls_027"><span class="cls_027">-</span><span class="cls_028"> Lectures till census date are optional attendance.</span></div>
<div style="position:absolute;left:79.20px;top:213.68px" class="cls_027"><span class="cls_027">-</span><span class="cls_028"> The lecture attendance (taken for lectures after</span></div>
<div style="position:absolute;left:101.70px;top:246.56px" class="cls_028"><span class="cls_028">census date) forms 5% of your final grade.</span></div>
<div style="position:absolute;left:79.20px;top:287.60px" class="cls_027"><span class="cls_027">-</span><span class="cls_028"> At random points during the lecture, in class</span></div>
<div style="position:absolute;left:101.70px;top:320.48px" class="cls_028"><span class="cls_028">quizzes will be conducted. These quizzes will also</span></div>
<div style="position:absolute;left:101.70px;top:354.56px" class="cls_028"><span class="cls_028">be included in the 5% of your final grade for</span></div>
<div style="position:absolute;left:101.70px;top:387.68px" class="cls_028"><span class="cls_028">attendance.</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:2750px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="CourseDetails/background6.jpg" width=720 height=540></div>
<div style="position:absolute;left:150.51px;top:37.12px" class="cls_002"><span class="cls_002">Welcome to the Course</span></div>
<div style="position:absolute;left:43.20px;top:124.68px" class="cls_004"><span class="cls_004">•</span><span class="cls_005"> No Cumulative Final!</span></div>
<div style="position:absolute;left:79.20px;top:160.56px" class="cls_007"><span class="cls_007">-</span><span class="cls_008"> Three Exams covering roughly 1/3 of class material</span></div>
<div style="position:absolute;left:43.20px;top:191.64px" class="cls_004"><span class="cls_004">•</span><span class="cls_005"> Final grade is combination of exams (20% each),</span></div>
<div style="position:absolute;left:70.20px;top:221.64px" class="cls_005"><span class="cls_005">written assignments (20%), programming mini projects</span></div>
<div style="position:absolute;left:70.20px;top:250.68px" class="cls_005"><span class="cls_005">(15%) and attendance/ in class quizzes(5%)</span></div>
<div style="position:absolute;left:43.20px;top:285.48px" class="cls_004"><span class="cls_004">•</span><span class="cls_005"> Assignment 0 will be posted by end of first day of</span></div>
<div style="position:absolute;left:70.20px;top:314.52px" class="cls_005"><span class="cls_005">classes!!</span></div>
<div style="position:absolute;left:79.20px;top:350.64px" class="cls_007"><span class="cls_007">-</span><span class="cls_008"> Don’t worry, It is a form acknowledging class policies</span></div>
<div style="position:absolute;left:79.20px;top:382.56px" class="cls_007"><span class="cls_007">-</span><span class="cls_008"> Make sure you have Canvas ASAP.</span></div>
<div style="position:absolute;left:79.20px;top:413.52px" class="cls_007"><span class="cls_007">-</span><span class="cls_008"> Recommended: Check omega access too (You need to use</span></div>
<div style="position:absolute;left:101.70px;top:439.68px" class="cls_008"><span class="cls_008">UTA VPN for this).</span></div>
</div>

</body>
</html>
