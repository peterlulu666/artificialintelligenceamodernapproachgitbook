<html>
<head><meta http-equiv=Content-Type content="text/html; charset=UTF-8">
<style type="text/css">
<!--
span.cls_002{font-family:Arial,serif;font-size:24.8px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_002{font-family:Arial,serif;font-size:24.8px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_003{font-family:Arial,serif;font-size:20.7px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_003{font-family:Arial,serif;font-size:20.7px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_004{font-family:Arial,serif;font-size:8.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_004{font-family:Arial,serif;font-size:8.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_005{font-family:Arial,serif;font-size:25.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_005{font-family:Arial,serif;font-size:25.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_006{font-family:Arial,serif;font-size:17.3px;color:rgb(0,0,126);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_006{font-family:Arial,serif;font-size:17.3px;color:rgb(0,0,126);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_007{font-family:Arial,serif;font-size:17.3px;color:rgb(179,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_007{font-family:Arial,serif;font-size:17.3px;color:rgb(179,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_008{font-family:Arial,serif;font-size:17.3px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_008{font-family:Arial,serif;font-size:17.3px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_009{font-family:Arial,serif;font-size:17.3px;color:rgb(0,75,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_009{font-family:Arial,serif;font-size:17.3px;color:rgb(0,75,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_010{font-family:Arial,serif;font-size:20.7px;color:rgb(0,75,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_010{font-family:Arial,serif;font-size:20.7px;color:rgb(0,75,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_012{font-family:Arial,serif;font-size:14.8px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_012{font-family:Arial,serif;font-size:14.8px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_011{font-family:Times,serif;font-size:14.8px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_011{font-family:Times,serif;font-size:14.8px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_013{font-family:Arial,serif;font-size:20.7px;color:rgb(0,0,126);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_013{font-family:Arial,serif;font-size:20.7px;color:rgb(0,0,126);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_014{font-family:Arial,serif;font-size:20.7px;color:rgb(153,0,153);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_014{font-family:Arial,serif;font-size:20.7px;color:rgb(153,0,153);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_015{font-family:Arial,serif;font-size:20.7px;color:rgb(126,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_015{font-family:Arial,serif;font-size:20.7px;color:rgb(126,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_016{font-family:Arial,serif;font-size:19.2px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_016{font-family:Arial,serif;font-size:19.2px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_017{font-family:Times,serif;font-size:12.5px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_017{font-family:Times,serif;font-size:12.5px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_018{font-family:Arial,serif;font-size:20.7px;color:rgb(255,0,255);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_018{font-family:Arial,serif;font-size:20.7px;color:rgb(255,0,255);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_019{font-family:Arial,serif;font-size:10.5px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_019{font-family:Arial,serif;font-size:10.5px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_020{font-family:Arial,serif;font-size:10.7px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_020{font-family:Arial,serif;font-size:10.7px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_021{font-family:Arial,serif;font-size:10.7px;color:rgb(0,255,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_021{font-family:Arial,serif;font-size:10.7px;color:rgb(0,255,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_022{font-family:Arial,serif;font-size:8.6px;color:rgb(0,255,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_022{font-family:Arial,serif;font-size:8.6px;color:rgb(0,255,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_023{font-family:Arial,serif;font-size:10.7px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_023{font-family:Arial,serif;font-size:10.7px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_024{font-family:Arial,serif;font-size:10.7px;color:rgb(0,255,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_024{font-family:Arial,serif;font-size:10.7px;color:rgb(0,255,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_025{font-family:Arial,serif;font-size:8.6px;color:rgb(0,255,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_025{font-family:Arial,serif;font-size:8.6px;color:rgb(0,255,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_026{font-family:Arial,serif;font-size:8.6px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_026{font-family:Arial,serif;font-size:8.6px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_027{font-family:Arial,serif;font-size:14.9px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_027{font-family:Arial,serif;font-size:14.9px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_029{font-family:Times,serif;font-size:17.4px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_029{font-family:Times,serif;font-size:17.4px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_028{font-family:Arial,serif;font-size:13.4px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_028{font-family:Arial,serif;font-size:13.4px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_030{font-family:Times,serif;font-size:25.0px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
div.cls_030{font-family:Times,serif;font-size:25.0px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
span.cls_031{font-family:Times,serif;font-size:25.0px;color:rgb(0,255,0);font-weight:normal;font-style:italic;text-decoration: none}
div.cls_031{font-family:Times,serif;font-size:25.0px;color:rgb(0,255,0);font-weight:normal;font-style:italic;text-decoration: none}
span.cls_032{font-family:Times,serif;font-size:24.9px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
div.cls_032{font-family:Times,serif;font-size:24.9px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
span.cls_033{font-family:Times,serif;font-size:24.9px;color:rgb(0,255,0);font-weight:normal;font-style:italic;text-decoration: none}
div.cls_033{font-family:Times,serif;font-size:24.9px;color:rgb(0,255,0);font-weight:normal;font-style:italic;text-decoration: none}
span.cls_034{font-family:Times,serif;font-size:24.7px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
div.cls_034{font-family:Times,serif;font-size:24.7px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
span.cls_035{font-family:Times,serif;font-size:24.7px;color:rgb(0,255,0);font-weight:normal;font-style:italic;text-decoration: none}
div.cls_035{font-family:Times,serif;font-size:24.7px;color:rgb(0,255,0);font-weight:normal;font-style:italic;text-decoration: none}
span.cls_036{font-family:Times,serif;font-size:22.7px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
div.cls_036{font-family:Times,serif;font-size:22.7px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
span.cls_037{font-family:Arial,serif;font-size:14.4px;color:rgb(153,0,153);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_037{font-family:Arial,serif;font-size:14.4px;color:rgb(153,0,153);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_038{font-family:Arial,serif;font-size:12.0px;color:rgb(153,0,153);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_038{font-family:Arial,serif;font-size:12.0px;color:rgb(153,0,153);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_039{font-family:Times,serif;font-size:16.7px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
div.cls_039{font-family:Times,serif;font-size:16.7px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
span.cls_040{font-family:Times,serif;font-size:16.7px;color:rgb(0,255,0);font-weight:normal;font-style:italic;text-decoration: none}
div.cls_040{font-family:Times,serif;font-size:16.7px;color:rgb(0,255,0);font-weight:normal;font-style:italic;text-decoration: none}
span.cls_041{font-family:Times,serif;font-size:16.8px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
div.cls_041{font-family:Times,serif;font-size:16.8px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
span.cls_042{font-family:Times,serif;font-size:16.8px;color:rgb(0,255,0);font-weight:normal;font-style:italic;text-decoration: none}
div.cls_042{font-family:Times,serif;font-size:16.8px;color:rgb(0,255,0);font-weight:normal;font-style:italic;text-decoration: none}
span.cls_043{font-family:Times,serif;font-size:16.1px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
div.cls_043{font-family:Times,serif;font-size:16.1px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
span.cls_044{font-family:Times,serif;font-size:16.1px;color:rgb(0,255,0);font-weight:normal;font-style:italic;text-decoration: none}
div.cls_044{font-family:Times,serif;font-size:16.1px;color:rgb(0,255,0);font-weight:normal;font-style:italic;text-decoration: none}
span.cls_045{font-family:Times,serif;font-size:13.7px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_045{font-family:Times,serif;font-size:13.7px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_046{font-family:Times,serif;font-size:8.2px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
div.cls_046{font-family:Times,serif;font-size:8.2px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
span.cls_047{font-family:Times,serif;font-size:8.2px;color:rgb(0,255,0);font-weight:normal;font-style:italic;text-decoration: none}
div.cls_047{font-family:Times,serif;font-size:8.2px;color:rgb(0,255,0);font-weight:normal;font-style:italic;text-decoration: none}
span.cls_048{font-family:Arial,serif;font-size:14.4px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_048{font-family:Arial,serif;font-size:14.4px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_049{font-family:Arial,serif;font-size:12.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_049{font-family:Arial,serif;font-size:12.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_050{font-family:Arial,serif;font-size:12.7px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_050{font-family:Arial,serif;font-size:12.7px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
-->
</style>
<script type="text/javascript" src="SolvingProblemsbySearch/wz_jsgraphics.js"></script>
</head>
<body>
<div style="position:absolute;left:50%;margin-left:-396px;top:0px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="SolvingProblemsbySearch/background01.jpg" width=792 height=612></div>
<div style="position:absolute;left:167.18px;top:195.55px" class="cls_002"><span class="cls_002">Problem solving and search</span></div>
<div style="position:absolute;left:291.86px;top:310.08px" class="cls_003"><span class="cls_003">Chapter 3</span></div>
<div style="position:absolute;left:593.78px;top:567.57px" class="cls_004"><span class="cls_004">Chapter 3</span></div>
<div style="position:absolute;left:647.86px;top:567.57px" class="cls_004"><span class="cls_004">1</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:622px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="SolvingProblemsbySearch/background02.jpg" width=792 height=612></div>
<div style="position:absolute;left:299.30px;top:76.03px" class="cls_005"><span class="cls_005">Outline</span></div>
<div style="position:absolute;left:40.10px;top:126.36px" class="cls_003"><span class="cls_003">♦ Problem-solving agents</span></div>
<div style="position:absolute;left:40.10px;top:166.56px" class="cls_003"><span class="cls_003">♦ Problem types</span></div>
<div style="position:absolute;left:40.10px;top:206.76px" class="cls_003"><span class="cls_003">♦ Problem formulation</span></div>
<div style="position:absolute;left:40.10px;top:246.96px" class="cls_003"><span class="cls_003">♦ Example problems</span></div>
<div style="position:absolute;left:40.10px;top:287.16px" class="cls_003"><span class="cls_003">♦ Basic search algorithms</span></div>
<div style="position:absolute;left:593.78px;top:567.57px" class="cls_004"><span class="cls_004">Chapter 3</span></div>
<div style="position:absolute;left:647.86px;top:567.57px" class="cls_004"><span class="cls_004">3</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:1244px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="SolvingProblemsbySearch/background03.jpg" width=792 height=612></div>
<div style="position:absolute;left:200.42px;top:76.03px" class="cls_005"><span class="cls_005">Problem-solving agents</span></div>
<div style="position:absolute;left:40.10px;top:127.68px" class="cls_003"><span class="cls_003">Restricted form of general agent:</span></div>
<div style="position:absolute;left:56.54px;top:176.36px" class="cls_006"><span class="cls_006">function</span><span class="cls_007"> Simple-Problem-Solving-Agent</span><span class="cls_008">(</span><span class="cls_009"> percept</span><span class="cls_008">)</span><span class="cls_006"> returns</span><span class="cls_008"> an action</span></div>
<div style="position:absolute;left:78.02px;top:198.20px" class="cls_006"><span class="cls_006">static</span><span class="cls_008">:</span><span class="cls_009"> seq</span><span class="cls_008">, an action sequence, initially empty</span></div>
<div style="position:absolute;left:135.86px;top:220.16px" class="cls_009"><span class="cls_009">state</span><span class="cls_008">, some description of the current world state</span></div>
<div style="position:absolute;left:135.86px;top:242.12px" class="cls_009"><span class="cls_009">goal</span><span class="cls_008">, a goal, initially null</span></div>
<div style="position:absolute;left:135.86px;top:263.96px" class="cls_009"><span class="cls_009">problem</span><span class="cls_008">, a problem formulation</span></div>
<div style="position:absolute;left:78.02px;top:293.12px" class="cls_009"><span class="cls_009">state</span><span class="cls_008"> ← Update-State(</span><span class="cls_009">state, percept</span><span class="cls_008">)</span></div>
<div style="position:absolute;left:78.02px;top:315.08px" class="cls_006"><span class="cls_006">if</span><span class="cls_009"> seq</span><span class="cls_008"> is empty</span><span class="cls_006"> then</span></div>
<div style="position:absolute;left:110.42px;top:336.92px" class="cls_009"><span class="cls_009">goal</span><span class="cls_008"> ← Formulate-Goal(</span><span class="cls_009">state</span><span class="cls_008">)</span></div>
<div style="position:absolute;left:110.42px;top:358.88px" class="cls_009"><span class="cls_009">problem</span><span class="cls_008"> ← Formulate-Problem(</span><span class="cls_009">state, goal</span><span class="cls_008">)</span></div>
<div style="position:absolute;left:110.42px;top:380.72px" class="cls_009"><span class="cls_009">seq</span><span class="cls_008"> ← Search(</span><span class="cls_009"> problem</span><span class="cls_008">)</span></div>
<div style="position:absolute;left:78.02px;top:402.68px" class="cls_009"><span class="cls_009">action</span><span class="cls_008"> ← Recommendation(</span><span class="cls_009">seq, state</span><span class="cls_008">)</span></div>
<div style="position:absolute;left:78.02px;top:424.64px" class="cls_009"><span class="cls_009">seq</span><span class="cls_008"> ← Remainder(</span><span class="cls_009">seq, state</span><span class="cls_008">)</span></div>
<div style="position:absolute;left:78.02px;top:446.48px" class="cls_006"><span class="cls_006">return</span><span class="cls_009"> action</span></div>
<div style="position:absolute;left:40.10px;top:498.60px" class="cls_003"><span class="cls_003">Note: this is</span><span class="cls_010"> offline</span><span class="cls_003"> problem solving; solution executed “eyes closed.”</span></div>
<div style="position:absolute;left:40.10px;top:523.56px" class="cls_010"><span class="cls_010">Online</span><span class="cls_003"> problem solving involves acting without complete knowledge.</span></div>
<div style="position:absolute;left:593.78px;top:567.57px" class="cls_004"><span class="cls_004">Chapter 3</span></div>
<div style="position:absolute;left:647.86px;top:567.57px" class="cls_004"><span class="cls_004">4</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:1866px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="SolvingProblemsbySearch/background04.jpg" width=792 height=612></div>
<div style="position:absolute;left:225.74px;top:76.03px" class="cls_005"><span class="cls_005">Example: Romania</span></div>
<div style="position:absolute;left:40.10px;top:127.68px" class="cls_003"><span class="cls_003">On holiday in Romania; currently in Arad.</span></div>
<div style="position:absolute;left:40.10px;top:152.52px" class="cls_003"><span class="cls_003">Flight leaves tomorrow from Bucharest</span></div>
<div style="position:absolute;left:40.10px;top:192.72px" class="cls_010"><span class="cls_010">Formulate goal</span><span class="cls_003">:</span></div>
<div style="position:absolute;left:97.70px;top:217.68px" class="cls_003"><span class="cls_003">be in Bucharest</span></div>
<div style="position:absolute;left:40.10px;top:257.88px" class="cls_010"><span class="cls_010">Formulate problem</span><span class="cls_003">:</span></div>
<div style="position:absolute;left:97.70px;top:282.84px" class="cls_010"><span class="cls_010">states</span><span class="cls_003">: various cities</span></div>
<div style="position:absolute;left:97.70px;top:307.68px" class="cls_010"><span class="cls_010">actions</span><span class="cls_003">: drive between cities</span></div>
<div style="position:absolute;left:40.10px;top:347.88px" class="cls_010"><span class="cls_010">Find solution</span><span class="cls_003">:</span></div>
<div style="position:absolute;left:97.70px;top:372.84px" class="cls_003"><span class="cls_003">sequence of cities, e.g., Arad, Sibiu, Fagaras, Bucharest</span></div>
<div style="position:absolute;left:593.78px;top:567.57px" class="cls_004"><span class="cls_004">Chapter 3</span></div>
<div style="position:absolute;left:647.86px;top:567.57px" class="cls_004"><span class="cls_004">5</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:2488px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="SolvingProblemsbySearch/background05.jpg" width=792 height=612></div>
<div style="position:absolute;left:225.74px;top:76.03px" class="cls_005"><span class="cls_005">Example: Romania</span></div>
<div style="position:absolute;left:121.33px;top:118.35px" class="cls_012"><span class="cls_012">Oradea</span></div>
<div style="position:absolute;left:80.47px;top:134.21px" class="cls_011"><span class="cls_011">71</span></div>
<div style="position:absolute;left:429.23px;top:142.20px" class="cls_012"><span class="cls_012">Neamt</span></div>
<div style="position:absolute;left:493.53px;top:170.36px" class="cls_011"><span class="cls_011">87</span></div>
<div style="position:absolute;left:93.52px;top:173.97px" class="cls_012"><span class="cls_012">Zerind</span></div>
<div style="position:absolute;left:163.72px;top:180.52px" class="cls_011"><span class="cls_011">151</span></div>
<div style="position:absolute;left:54.30px;top:186.98px" class="cls_011"><span class="cls_011">75</span></div>
<div style="position:absolute;left:545.31px;top:198.94px" class="cls_012"><span class="cls_012">Iasi</span></div>
<div style="position:absolute;left:21.18px;top:215.96px" class="cls_012"><span class="cls_012">Arad</span></div>
<div style="position:absolute;left:122.89px;top:223.07px" class="cls_011"><span class="cls_011">140</span></div>
<div style="position:absolute;left:562.95px;top:233.16px" class="cls_011"><span class="cls_011">92</span></div>
<div style="position:absolute;left:213.25px;top:244.34px" class="cls_012"><span class="cls_012">Sibiu</span></div>
<div style="position:absolute;left:308.85px;top:247.17px" class="cls_012"><span class="cls_012">Fagaras</span></div>
<div style="position:absolute;left:268.10px;top:253.49px" class="cls_011"><span class="cls_011">99</span></div>
<div style="position:absolute;left:37.47px;top:267.99px" class="cls_011"><span class="cls_011">118</span></div>
<div style="position:absolute;left:586.18px;top:274.98px" class="cls_012"><span class="cls_012">Vaslui</span></div>
<div style="position:absolute;left:227.90px;top:285.29px" class="cls_011"><span class="cls_011">80</span></div>
<div style="position:absolute;left:239.33px;top:303.35px" class="cls_012"><span class="cls_012">Rimnicu Vilcea</span></div>
<div style="position:absolute;left:71.33px;top:310.17px" class="cls_012"><span class="cls_012">Timisoara</span></div>
<div style="position:absolute;left:553.15px;top:335.07px" class="cls_011"><span class="cls_011">142</span></div>
<div style="position:absolute;left:395.47px;top:343.56px" class="cls_011"><span class="cls_011">211</span></div>
<div style="position:absolute;left:91.15px;top:347.11px" class="cls_011"><span class="cls_011">111</span></div>
<div style="position:absolute;left:327.33px;top:349.48px" class="cls_012"><span class="cls_012">Pitesti</span></div>
<div style="position:absolute;left:164.29px;top:355.56px" class="cls_012"><span class="cls_012">Lugoj</span></div>
<div style="position:absolute;left:277.86px;top:357.02px" class="cls_011"><span class="cls_011">97</span></div>
<div style="position:absolute;left:135.77px;top:380.23px" class="cls_011"><span class="cls_011">70</span></div>
<div style="position:absolute;left:557.42px;top:382.04px" class="cls_011"><span class="cls_011">98</span></div>
<div style="position:absolute;left:616.51px;top:390.75px" class="cls_012"><span class="cls_012">Hirsova</span></div>
<div style="position:absolute;left:255.74px;top:398.05px" class="cls_011"><span class="cls_011">146</span></div>
<div style="position:absolute;left:468.04px;top:396.36px" class="cls_011"><span class="cls_011">85</span></div>
<div style="position:absolute;left:166.25px;top:404.37px" class="cls_012"><span class="cls_012">Mehadia</span></div>
<div style="position:absolute;left:370.75px;top:402.68px" class="cls_011"><span class="cls_011">101</span></div>
<div style="position:absolute;left:504.07px;top:405.04px" class="cls_012"><span class="cls_012">Urziceni</span></div>
<div style="position:absolute;left:631.70px;top:423.24px" class="cls_011"><span class="cls_011">86</span></div>
<div style="position:absolute;left:135.37px;top:428.72px" class="cls_011"><span class="cls_011">75</span></div>
<div style="position:absolute;left:305.27px;top:429.52px" class="cls_011"><span class="cls_011">138</span></div>
<div style="position:absolute;left:449.60px;top:437.47px" class="cls_012"><span class="cls_012">Bucharest</span></div>
<div style="position:absolute;left:196.35px;top:449.58px" class="cls_011"><span class="cls_011">120</span></div>
<div style="position:absolute;left:87.78px;top:453.17px" class="cls_012"><span class="cls_012">Dobreta</span></div>
<div style="position:absolute;left:431.84px;top:463.42px" class="cls_011"><span class="cls_011">90</span></div>
<div style="position:absolute;left:267.58px;top:474.74px" class="cls_012"><span class="cls_012">Craiova</span></div>
<div style="position:absolute;left:628.17px;top:478.14px" class="cls_012"><span class="cls_012">Eforie</span></div>
<div style="position:absolute;left:421.05px;top:488.36px" class="cls_012"><span class="cls_012">Giurgiu</span></div>
<div style="position:absolute;left:593.78px;top:567.57px" class="cls_004"><span class="cls_004">Chapter 3</span></div>
<div style="position:absolute;left:647.86px;top:567.57px" class="cls_004"><span class="cls_004">6</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:3110px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="SolvingProblemsbySearch/background06.jpg" width=792 height=612></div>
<div style="position:absolute;left:139.94px;top:76.03px" class="cls_005"><span class="cls_005">Single-state problem formulation</span></div>
<div style="position:absolute;left:40.10px;top:127.68px" class="cls_003"><span class="cls_003">A</span><span class="cls_013"> problem</span><span class="cls_003"> is defined by four items:</span></div>
<div style="position:absolute;left:40.10px;top:167.88px" class="cls_013"><span class="cls_013">initial state</span></div>
<div style="position:absolute;left:156.46px;top:167.88px" class="cls_003"><span class="cls_003">e.g., “at Arad”</span></div>
<div style="position:absolute;left:40.10px;top:208.08px" class="cls_013"><span class="cls_013">successor function</span><span class="cls_014"> S(x)</span><span class="cls_003"> = set of action-state pairs</span></div>
<div style="position:absolute;left:97.70px;top:232.92px" class="cls_003"><span class="cls_003">e.g.,</span><span class="cls_014"> S(Arad) = {〈Arad → Zerind, Zerind〉, . . .}</span></div>
<div style="position:absolute;left:40.10px;top:273.12px" class="cls_013"><span class="cls_013">goal test</span><span class="cls_003">, can be</span></div>
<div style="position:absolute;left:97.70px;top:298.08px" class="cls_010"><span class="cls_010">explicit</span><span class="cls_003">, e.g.,</span><span class="cls_014"> x</span><span class="cls_003"> = “at Bucharest”</span></div>
<div style="position:absolute;left:97.70px;top:323.04px" class="cls_010"><span class="cls_010">implicit</span><span class="cls_003">, e.g.,</span><span class="cls_014"> N oDirt(x)</span></div>
<div style="position:absolute;left:40.10px;top:363.24px" class="cls_013"><span class="cls_013">path cost</span><span class="cls_003"> (additive)</span></div>
<div style="position:absolute;left:97.70px;top:388.08px" class="cls_003"><span class="cls_003">e.g., sum of distances, number of actions executed, etc.</span></div>
<div style="position:absolute;left:97.70px;top:413.04px" class="cls_014"><span class="cls_014">c(x, a, y)</span><span class="cls_003"> is the</span><span class="cls_013"> step cost</span><span class="cls_003">, assumed to be</span><span class="cls_014"> ≥ 0</span></div>
<div style="position:absolute;left:40.10px;top:453.24px" class="cls_003"><span class="cls_003">A</span><span class="cls_013"> solution</span><span class="cls_003"> is a sequence of actions</span></div>
<div style="position:absolute;left:40.10px;top:478.08px" class="cls_003"><span class="cls_003">leading from the initial state to a goal state</span></div>
<div style="position:absolute;left:589.58px;top:567.57px" class="cls_004"><span class="cls_004">Chapter 3</span></div>
<div style="position:absolute;left:643.54px;top:567.57px" class="cls_004"><span class="cls_004">12</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:3732px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="SolvingProblemsbySearch/background07.jpg" width=792 height=612></div>
<div style="position:absolute;left:204.86px;top:76.03px" class="cls_005"><span class="cls_005">Selecting a state space</span></div>
<div style="position:absolute;left:40.10px;top:127.68px" class="cls_003"><span class="cls_003">Real world is absurdly complex</span></div>
<div style="position:absolute;left:97.70px;top:152.52px" class="cls_003"><span class="cls_003">⇒ state space must be</span><span class="cls_015"> abstracted</span><span class="cls_003"> for problem solving</span></div>
<div style="position:absolute;left:40.10px;top:192.72px" class="cls_003"><span class="cls_003">(Abstract) state = set of real states</span></div>
<div style="position:absolute;left:40.10px;top:232.92px" class="cls_003"><span class="cls_003">(Abstract) action = complex combination of real actions</span></div>
<div style="position:absolute;left:97.70px;top:257.88px" class="cls_003"><span class="cls_003">e.g., “Arad → Zerind” represents a complex set</span></div>
<div style="position:absolute;left:126.50px;top:282.84px" class="cls_003"><span class="cls_003">of possible routes, detours, rest stops, etc.</span></div>
<div style="position:absolute;left:40.10px;top:307.68px" class="cls_003"><span class="cls_003">For guaranteed realizability,</span><span class="cls_015"> any</span><span class="cls_003"> real state “in Arad”</span></div>
<div style="position:absolute;left:68.90px;top:332.64px" class="cls_003"><span class="cls_003">must get to</span><span class="cls_010"> some</span><span class="cls_003"> real state “in Zerind”</span></div>
<div style="position:absolute;left:40.10px;top:372.84px" class="cls_003"><span class="cls_003">(Abstract) solution =</span></div>
<div style="position:absolute;left:97.70px;top:397.68px" class="cls_003"><span class="cls_003">set of real paths that are solutions in the real world</span></div>
<div style="position:absolute;left:40.10px;top:437.88px" class="cls_003"><span class="cls_003">Each abstract action should be “easier” than the original problem!</span></div>
<div style="position:absolute;left:589.58px;top:567.57px" class="cls_004"><span class="cls_004">Chapter 3</span></div>
<div style="position:absolute;left:643.54px;top:567.57px" class="cls_004"><span class="cls_004">13</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:4354px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="SolvingProblemsbySearch/background08.jpg" width=792 height=612></div>
<div style="position:absolute;left:201.02px;top:76.03px" class="cls_005"><span class="cls_005">Example: The 8-puzzle</span></div>
<div style="position:absolute;left:185.76px;top:131.57px" class="cls_016"><span class="cls_016">7</span></div>
<div style="position:absolute;left:237.89px;top:131.57px" class="cls_016"><span class="cls_016">2</span></div>
<div style="position:absolute;left:289.64px;top:131.57px" class="cls_016"><span class="cls_016">4</span></div>
<div style="position:absolute;left:393.30px;top:131.57px" class="cls_016"><span class="cls_016">1</span></div>
<div style="position:absolute;left:445.43px;top:131.57px" class="cls_016"><span class="cls_016">2</span></div>
<div style="position:absolute;left:497.18px;top:131.57px" class="cls_016"><span class="cls_016">3</span></div>
<div style="position:absolute;left:185.76px;top:183.89px" class="cls_016"><span class="cls_016">5</span></div>
<div style="position:absolute;left:288.77px;top:183.89px" class="cls_016"><span class="cls_016">6</span></div>
<div style="position:absolute;left:393.30px;top:183.89px" class="cls_016"><span class="cls_016">4</span></div>
<div style="position:absolute;left:445.43px;top:183.89px" class="cls_016"><span class="cls_016">5</span></div>
<div style="position:absolute;left:496.31px;top:183.89px" class="cls_016"><span class="cls_016">6</span></div>
<div style="position:absolute;left:185.76px;top:236.59px" class="cls_016"><span class="cls_016">8</span></div>
<div style="position:absolute;left:237.89px;top:236.59px" class="cls_016"><span class="cls_016">3</span></div>
<div style="position:absolute;left:289.64px;top:236.59px" class="cls_016"><span class="cls_016">1</span></div>
<div style="position:absolute;left:393.30px;top:236.59px" class="cls_016"><span class="cls_016">7</span></div>
<div style="position:absolute;left:445.43px;top:236.59px" class="cls_016"><span class="cls_016">8</span></div>
<div style="position:absolute;left:207.68px;top:286.65px" class="cls_017"><span class="cls_017">Start State</span></div>
<div style="position:absolute;left:421.34px;top:286.52px" class="cls_017"><span class="cls_017">Goal State</span></div>
<div style="position:absolute;left:40.10px;top:324.72px" class="cls_018"><span class="cls_018">states??</span></div>
<div style="position:absolute;left:40.10px;top:349.56px" class="cls_018"><span class="cls_018">actions??</span></div>
<div style="position:absolute;left:40.10px;top:374.52px" class="cls_018"><span class="cls_018">goal test??</span></div>
<div style="position:absolute;left:40.10px;top:399.36px" class="cls_018"><span class="cls_018">path cost??</span></div>
<div style="position:absolute;left:589.58px;top:567.57px" class="cls_004"><span class="cls_004">Chapter 3</span></div>
<div style="position:absolute;left:643.54px;top:567.57px" class="cls_004"><span class="cls_004">19</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:4976px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="SolvingProblemsbySearch/background09.jpg" width=792 height=612></div>
<div style="position:absolute;left:201.02px;top:76.03px" class="cls_005"><span class="cls_005">Example: The 8-puzzle</span></div>
<div style="position:absolute;left:185.76px;top:131.57px" class="cls_016"><span class="cls_016">7</span></div>
<div style="position:absolute;left:237.89px;top:131.57px" class="cls_016"><span class="cls_016">2</span></div>
<div style="position:absolute;left:289.64px;top:131.57px" class="cls_016"><span class="cls_016">4</span></div>
<div style="position:absolute;left:393.30px;top:131.57px" class="cls_016"><span class="cls_016">1</span></div>
<div style="position:absolute;left:445.43px;top:131.57px" class="cls_016"><span class="cls_016">2</span></div>
<div style="position:absolute;left:497.18px;top:131.57px" class="cls_016"><span class="cls_016">3</span></div>
<div style="position:absolute;left:185.76px;top:183.89px" class="cls_016"><span class="cls_016">5</span></div>
<div style="position:absolute;left:288.77px;top:183.89px" class="cls_016"><span class="cls_016">6</span></div>
<div style="position:absolute;left:393.30px;top:183.89px" class="cls_016"><span class="cls_016">4</span></div>
<div style="position:absolute;left:445.43px;top:183.89px" class="cls_016"><span class="cls_016">5</span></div>
<div style="position:absolute;left:496.31px;top:183.89px" class="cls_016"><span class="cls_016">6</span></div>
<div style="position:absolute;left:185.76px;top:236.59px" class="cls_016"><span class="cls_016">8</span></div>
<div style="position:absolute;left:237.89px;top:236.59px" class="cls_016"><span class="cls_016">3</span></div>
<div style="position:absolute;left:289.64px;top:236.59px" class="cls_016"><span class="cls_016">1</span></div>
<div style="position:absolute;left:393.30px;top:236.59px" class="cls_016"><span class="cls_016">7</span></div>
<div style="position:absolute;left:445.43px;top:236.59px" class="cls_016"><span class="cls_016">8</span></div>
<div style="position:absolute;left:207.68px;top:286.65px" class="cls_017"><span class="cls_017">Start State</span></div>
<div style="position:absolute;left:421.34px;top:286.52px" class="cls_017"><span class="cls_017">Goal State</span></div>
<div style="position:absolute;left:40.10px;top:324.72px" class="cls_018"><span class="cls_018">states??</span><span class="cls_003">: integer locations of tiles (ignore intermediate positions)</span></div>
<div style="position:absolute;left:40.10px;top:349.56px" class="cls_018"><span class="cls_018">actions??</span></div>
<div style="position:absolute;left:40.10px;top:374.52px" class="cls_018"><span class="cls_018">goal test??</span></div>
<div style="position:absolute;left:40.10px;top:399.36px" class="cls_018"><span class="cls_018">path cost??</span></div>
<div style="position:absolute;left:589.58px;top:567.57px" class="cls_004"><span class="cls_004">Chapter 3</span></div>
<div style="position:absolute;left:643.54px;top:567.57px" class="cls_004"><span class="cls_004">20</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:5598px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="SolvingProblemsbySearch/background10.jpg" width=792 height=612></div>
<div style="position:absolute;left:201.02px;top:76.03px" class="cls_005"><span class="cls_005">Example: The 8-puzzle</span></div>
<div style="position:absolute;left:185.76px;top:131.57px" class="cls_016"><span class="cls_016">7</span></div>
<div style="position:absolute;left:237.89px;top:131.57px" class="cls_016"><span class="cls_016">2</span></div>
<div style="position:absolute;left:289.64px;top:131.57px" class="cls_016"><span class="cls_016">4</span></div>
<div style="position:absolute;left:393.30px;top:131.57px" class="cls_016"><span class="cls_016">1</span></div>
<div style="position:absolute;left:445.43px;top:131.57px" class="cls_016"><span class="cls_016">2</span></div>
<div style="position:absolute;left:497.18px;top:131.57px" class="cls_016"><span class="cls_016">3</span></div>
<div style="position:absolute;left:185.76px;top:183.89px" class="cls_016"><span class="cls_016">5</span></div>
<div style="position:absolute;left:288.77px;top:183.89px" class="cls_016"><span class="cls_016">6</span></div>
<div style="position:absolute;left:393.30px;top:183.89px" class="cls_016"><span class="cls_016">4</span></div>
<div style="position:absolute;left:445.43px;top:183.89px" class="cls_016"><span class="cls_016">5</span></div>
<div style="position:absolute;left:496.31px;top:183.89px" class="cls_016"><span class="cls_016">6</span></div>
<div style="position:absolute;left:185.76px;top:236.59px" class="cls_016"><span class="cls_016">8</span></div>
<div style="position:absolute;left:237.89px;top:236.59px" class="cls_016"><span class="cls_016">3</span></div>
<div style="position:absolute;left:289.64px;top:236.59px" class="cls_016"><span class="cls_016">1</span></div>
<div style="position:absolute;left:393.30px;top:236.59px" class="cls_016"><span class="cls_016">7</span></div>
<div style="position:absolute;left:445.43px;top:236.59px" class="cls_016"><span class="cls_016">8</span></div>
<div style="position:absolute;left:207.68px;top:286.65px" class="cls_017"><span class="cls_017">Start State</span></div>
<div style="position:absolute;left:421.34px;top:286.52px" class="cls_017"><span class="cls_017">Goal State</span></div>
<div style="position:absolute;left:40.10px;top:324.72px" class="cls_018"><span class="cls_018">states??</span><span class="cls_003">: integer locations of tiles (ignore intermediate positions)</span></div>
<div style="position:absolute;left:40.10px;top:349.56px" class="cls_018"><span class="cls_018">actions??</span><span class="cls_003">: move blank left, right, up, down (ignore unjamming etc.)</span></div>
<div style="position:absolute;left:40.10px;top:374.52px" class="cls_018"><span class="cls_018">goal test??</span></div>
<div style="position:absolute;left:40.10px;top:399.36px" class="cls_018"><span class="cls_018">path cost??</span></div>
<div style="position:absolute;left:589.58px;top:567.57px" class="cls_004"><span class="cls_004">Chapter 3</span></div>
<div style="position:absolute;left:643.54px;top:567.57px" class="cls_004"><span class="cls_004">21</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:6220px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="SolvingProblemsbySearch/background11.jpg" width=792 height=612></div>
<div style="position:absolute;left:201.02px;top:76.03px" class="cls_005"><span class="cls_005">Example: The 8-puzzle</span></div>
<div style="position:absolute;left:185.76px;top:131.57px" class="cls_016"><span class="cls_016">7</span></div>
<div style="position:absolute;left:237.89px;top:131.57px" class="cls_016"><span class="cls_016">2</span></div>
<div style="position:absolute;left:289.64px;top:131.57px" class="cls_016"><span class="cls_016">4</span></div>
<div style="position:absolute;left:393.30px;top:131.57px" class="cls_016"><span class="cls_016">1</span></div>
<div style="position:absolute;left:445.43px;top:131.57px" class="cls_016"><span class="cls_016">2</span></div>
<div style="position:absolute;left:497.18px;top:131.57px" class="cls_016"><span class="cls_016">3</span></div>
<div style="position:absolute;left:185.76px;top:183.89px" class="cls_016"><span class="cls_016">5</span></div>
<div style="position:absolute;left:288.77px;top:183.89px" class="cls_016"><span class="cls_016">6</span></div>
<div style="position:absolute;left:393.30px;top:183.89px" class="cls_016"><span class="cls_016">4</span></div>
<div style="position:absolute;left:445.43px;top:183.89px" class="cls_016"><span class="cls_016">5</span></div>
<div style="position:absolute;left:496.31px;top:183.89px" class="cls_016"><span class="cls_016">6</span></div>
<div style="position:absolute;left:185.76px;top:236.59px" class="cls_016"><span class="cls_016">8</span></div>
<div style="position:absolute;left:237.89px;top:236.59px" class="cls_016"><span class="cls_016">3</span></div>
<div style="position:absolute;left:289.64px;top:236.59px" class="cls_016"><span class="cls_016">1</span></div>
<div style="position:absolute;left:393.30px;top:236.59px" class="cls_016"><span class="cls_016">7</span></div>
<div style="position:absolute;left:445.43px;top:236.59px" class="cls_016"><span class="cls_016">8</span></div>
<div style="position:absolute;left:207.68px;top:286.65px" class="cls_017"><span class="cls_017">Start State</span></div>
<div style="position:absolute;left:421.34px;top:286.52px" class="cls_017"><span class="cls_017">Goal State</span></div>
<div style="position:absolute;left:40.10px;top:324.72px" class="cls_018"><span class="cls_018">states??</span><span class="cls_003">: integer locations of tiles (ignore intermediate positions)</span></div>
<div style="position:absolute;left:40.10px;top:349.56px" class="cls_018"><span class="cls_018">actions??</span><span class="cls_003">: move blank left, right, up, down (ignore unjamming etc.)</span></div>
<div style="position:absolute;left:40.10px;top:374.52px" class="cls_018"><span class="cls_018">goal test??</span><span class="cls_003">: = goal state (given)</span></div>
<div style="position:absolute;left:40.10px;top:399.36px" class="cls_018"><span class="cls_018">path cost??</span></div>
<div style="position:absolute;left:589.58px;top:567.57px" class="cls_004"><span class="cls_004">Chapter 3</span></div>
<div style="position:absolute;left:643.54px;top:567.57px" class="cls_004"><span class="cls_004">22</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:6842px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="SolvingProblemsbySearch/background12.jpg" width=792 height=612></div>
<div style="position:absolute;left:201.02px;top:76.03px" class="cls_005"><span class="cls_005">Example: The 8-puzzle</span></div>
<div style="position:absolute;left:185.76px;top:131.57px" class="cls_016"><span class="cls_016">7</span></div>
<div style="position:absolute;left:237.89px;top:131.57px" class="cls_016"><span class="cls_016">2</span></div>
<div style="position:absolute;left:289.64px;top:131.57px" class="cls_016"><span class="cls_016">4</span></div>
<div style="position:absolute;left:393.30px;top:131.57px" class="cls_016"><span class="cls_016">1</span></div>
<div style="position:absolute;left:445.43px;top:131.57px" class="cls_016"><span class="cls_016">2</span></div>
<div style="position:absolute;left:497.18px;top:131.57px" class="cls_016"><span class="cls_016">3</span></div>
<div style="position:absolute;left:185.76px;top:183.89px" class="cls_016"><span class="cls_016">5</span></div>
<div style="position:absolute;left:288.77px;top:183.89px" class="cls_016"><span class="cls_016">6</span></div>
<div style="position:absolute;left:393.30px;top:183.89px" class="cls_016"><span class="cls_016">4</span></div>
<div style="position:absolute;left:445.43px;top:183.89px" class="cls_016"><span class="cls_016">5</span></div>
<div style="position:absolute;left:496.31px;top:183.89px" class="cls_016"><span class="cls_016">6</span></div>
<div style="position:absolute;left:185.76px;top:236.59px" class="cls_016"><span class="cls_016">8</span></div>
<div style="position:absolute;left:237.89px;top:236.59px" class="cls_016"><span class="cls_016">3</span></div>
<div style="position:absolute;left:289.64px;top:236.59px" class="cls_016"><span class="cls_016">1</span></div>
<div style="position:absolute;left:393.30px;top:236.59px" class="cls_016"><span class="cls_016">7</span></div>
<div style="position:absolute;left:445.43px;top:236.59px" class="cls_016"><span class="cls_016">8</span></div>
<div style="position:absolute;left:207.68px;top:286.65px" class="cls_017"><span class="cls_017">Start State</span></div>
<div style="position:absolute;left:421.34px;top:286.52px" class="cls_017"><span class="cls_017">Goal State</span></div>
<div style="position:absolute;left:40.10px;top:324.72px" class="cls_018"><span class="cls_018">states??</span><span class="cls_003">: integer locations of tiles (ignore intermediate positions)</span></div>
<div style="position:absolute;left:40.10px;top:349.56px" class="cls_018"><span class="cls_018">actions??</span><span class="cls_003">: move blank left, right, up, down (ignore unjamming etc.)</span></div>
<div style="position:absolute;left:40.10px;top:374.52px" class="cls_018"><span class="cls_018">goal test??</span><span class="cls_003">: = goal state (given)</span></div>
<div style="position:absolute;left:40.10px;top:399.36px" class="cls_018"><span class="cls_018">path cost??</span><span class="cls_003">: 1 per move</span></div>
<div style="position:absolute;left:40.10px;top:439.56px" class="cls_003"><span class="cls_003">[Note: optimal solution of n-Puzzle family is NP-hard]</span></div>
<div style="position:absolute;left:589.58px;top:567.57px" class="cls_004"><span class="cls_004">Chapter 3</span></div>
<div style="position:absolute;left:643.54px;top:567.57px" class="cls_004"><span class="cls_004">23</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:7464px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="SolvingProblemsbySearch/background13.jpg" width=792 height=612></div>
<div style="position:absolute;left:176.30px;top:76.03px" class="cls_005"><span class="cls_005">Example: robotic assembly</span></div>
<div style="position:absolute;left:276.73px;top:118.20px" class="cls_019"><span class="cls_019">P</span></div>
<div style="position:absolute;left:418.65px;top:129.93px" class="cls_019"><span class="cls_019">R</span></div>
<div style="position:absolute;left:458.95px;top:130.90px" class="cls_019"><span class="cls_019">R</span></div>
<div style="position:absolute;left:290.71px;top:162.10px" class="cls_019"><span class="cls_019">R</span></div>
<div style="position:absolute;left:490.36px;top:163.56px" class="cls_019"><span class="cls_019">R</span></div>
<div style="position:absolute;left:290.24px;top:230.41px" class="cls_019"><span class="cls_019">R</span></div>
<div style="position:absolute;left:40.10px;top:309.84px" class="cls_018"><span class="cls_018">states??</span><span class="cls_003">: real-valued coordinates of robot joint angles</span></div>
<div style="position:absolute;left:97.70px;top:334.80px" class="cls_003"><span class="cls_003">parts of the object to be assembled</span></div>
<div style="position:absolute;left:40.10px;top:375.00px" class="cls_018"><span class="cls_018">actions??</span><span class="cls_003">: continuous motions of robot joints</span></div>
<div style="position:absolute;left:40.10px;top:415.20px" class="cls_018"><span class="cls_018">goal test??</span><span class="cls_003">: complete assembly</span><span class="cls_015"> with no robot included!</span></div>
<div style="position:absolute;left:40.10px;top:455.40px" class="cls_018"><span class="cls_018">path cost??</span><span class="cls_003">: time to execute</span></div>
<div style="position:absolute;left:589.58px;top:567.57px" class="cls_004"><span class="cls_004">Chapter 3</span></div>
<div style="position:absolute;left:643.54px;top:567.57px" class="cls_004"><span class="cls_004">24</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:8086px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="SolvingProblemsbySearch/background14.jpg" width=792 height=612></div>
<div style="position:absolute;left:203.54px;top:76.03px" class="cls_005"><span class="cls_005">Tree search algorithms</span></div>
<div style="position:absolute;left:40.10px;top:127.68px" class="cls_003"><span class="cls_003">Basic idea:</span></div>
<div style="position:absolute;left:68.90px;top:152.52px" class="cls_003"><span class="cls_003">offline, simulated exploration of state space</span></div>
<div style="position:absolute;left:68.90px;top:177.48px" class="cls_003"><span class="cls_003">by generating successors of already-explored states</span></div>
<div style="position:absolute;left:126.50px;top:202.32px" class="cls_003"><span class="cls_003">(a.k.a.</span><span class="cls_013"> expanding</span><span class="cls_003"> states)</span></div>
<div style="position:absolute;left:56.54px;top:252.20px" class="cls_006"><span class="cls_006">function</span><span class="cls_007"> Tree-Search</span><span class="cls_008">(</span><span class="cls_009"> problem, strategy</span><span class="cls_008">)</span><span class="cls_006"> returns</span><span class="cls_008"> a solution, or failure</span></div>
<div style="position:absolute;left:78.02px;top:274.04px" class="cls_008"><span class="cls_008">initialize the search tree using the initial state of</span><span class="cls_009"> problem</span></div>
<div style="position:absolute;left:78.02px;top:296.00px" class="cls_006"><span class="cls_006">loop do</span></div>
<div style="position:absolute;left:110.42px;top:317.96px" class="cls_006"><span class="cls_006">if</span><span class="cls_008"> there are no candidates for expansion</span><span class="cls_006"> then return</span><span class="cls_008"> failure</span></div>
<div style="position:absolute;left:110.42px;top:339.80px" class="cls_008"><span class="cls_008">choose a leaf node for expansion according to</span><span class="cls_009"> strategy</span></div>
<div style="position:absolute;left:110.42px;top:361.76px" class="cls_006"><span class="cls_006">if</span><span class="cls_008"> the node contains a goal state</span><span class="cls_006"> then return</span><span class="cls_008"> the corresponding solution</span></div>
<div style="position:absolute;left:110.42px;top:383.72px" class="cls_006"><span class="cls_006">else</span><span class="cls_008"> expand the node and add the resulting nodes to the search tree</span></div>
<div style="position:absolute;left:78.02px;top:405.56px" class="cls_006"><span class="cls_006">end</span></div>
<div style="position:absolute;left:589.58px;top:567.57px" class="cls_004"><span class="cls_004">Chapter 3</span></div>
<div style="position:absolute;left:643.54px;top:567.57px" class="cls_004"><span class="cls_004">25</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:8708px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="SolvingProblemsbySearch/background15.jpg" width=792 height=612></div>
<div style="position:absolute;left:218.30px;top:76.03px" class="cls_005"><span class="cls_005">Tree search example</span></div>
<div style="position:absolute;left:354.53px;top:143.06px" class="cls_020"><span class="cls_020">Arad</span></div>
<div style="position:absolute;left:177.13px;top:200.25px" class="cls_021"><span class="cls_021">Sibiu</span></div>
<div style="position:absolute;left:396.63px;top:200.25px" class="cls_021"><span class="cls_021">Timisoara</span></div>
<div style="position:absolute;left:561.87px;top:200.25px" class="cls_021"><span class="cls_021">Zerind</span></div>
<div style="position:absolute;left:65.66px;top:257.45px" class="cls_021"><span class="cls_021">Arad</span></div>
<div style="position:absolute;left:132.15px;top:257.45px" class="cls_021"><span class="cls_021">Fagaras</span></div>
<div style="position:absolute;left:206.95px;top:257.45px" class="cls_021"><span class="cls_021">Oradea</span></div>
<div style="position:absolute;left:273.44px;top:259.83px" class="cls_022"><span class="cls_022">Rimnicu Vilcea</span></div>
<div style="position:absolute;left:372.68px;top:257.45px" class="cls_021"><span class="cls_021">Arad</span></div>
<div style="position:absolute;left:444.54px;top:257.45px" class="cls_021"><span class="cls_021">Lugoj</span></div>
<div style="position:absolute;left:532.05px;top:257.45px" class="cls_021"><span class="cls_021">Arad</span></div>
<div style="position:absolute;left:598.05px;top:257.45px" class="cls_021"><span class="cls_021">Oradea</span></div>
<div style="position:absolute;left:589.58px;top:567.57px" class="cls_004"><span class="cls_004">Chapter 3</span></div>
<div style="position:absolute;left:643.54px;top:567.57px" class="cls_004"><span class="cls_004">26</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:9330px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="SolvingProblemsbySearch/background16.jpg" width=792 height=612></div>
<div style="position:absolute;left:218.30px;top:76.03px" class="cls_005"><span class="cls_005">Tree search example</span></div>
<div style="position:absolute;left:356.79px;top:143.30px" class="cls_023"><span class="cls_023">Arad</span></div>
<div style="position:absolute;left:177.20px;top:200.56px" class="cls_023"><span class="cls_023">Sibiu</span></div>
<div style="position:absolute;left:396.92px;top:200.56px" class="cls_023"><span class="cls_023">Timisoara</span></div>
<div style="position:absolute;left:562.33px;top:200.56px" class="cls_023"><span class="cls_023">Zerind</span></div>
<div style="position:absolute;left:65.62px;top:257.81px" class="cls_024"><span class="cls_024">Arad</span></div>
<div style="position:absolute;left:132.17px;top:257.81px" class="cls_024"><span class="cls_024">Fagaras</span></div>
<div style="position:absolute;left:207.05px;top:257.81px" class="cls_024"><span class="cls_024">Oradea</span></div>
<div style="position:absolute;left:273.60px;top:260.19px" class="cls_025"><span class="cls_025">Rimnicu Vilcea</span></div>
<div style="position:absolute;left:372.94px;top:257.81px" class="cls_024"><span class="cls_024">Arad</span></div>
<div style="position:absolute;left:444.88px;top:257.81px" class="cls_024"><span class="cls_024">Lugoj</span></div>
<div style="position:absolute;left:532.48px;top:257.81px" class="cls_024"><span class="cls_024">Arad</span></div>
<div style="position:absolute;left:598.54px;top:257.81px" class="cls_024"><span class="cls_024">Oradea</span></div>
<div style="position:absolute;left:589.58px;top:567.57px" class="cls_004"><span class="cls_004">Chapter 3</span></div>
<div style="position:absolute;left:643.54px;top:567.57px" class="cls_004"><span class="cls_004">27</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:9952px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="SolvingProblemsbySearch/background17.jpg" width=792 height=612></div>
<div style="position:absolute;left:218.30px;top:76.03px" class="cls_005"><span class="cls_005">Tree search example</span></div>
<div style="position:absolute;left:356.79px;top:143.28px" class="cls_023"><span class="cls_023">Arad</span></div>
<div style="position:absolute;left:177.20px;top:200.53px" class="cls_023"><span class="cls_023">Sibiu</span></div>
<div style="position:absolute;left:396.92px;top:200.53px" class="cls_023"><span class="cls_023">Timisoara</span></div>
<div style="position:absolute;left:562.33px;top:200.53px" class="cls_023"><span class="cls_023">Zerind</span></div>
<div style="position:absolute;left:65.62px;top:257.79px" class="cls_023"><span class="cls_023">Arad</span></div>
<div style="position:absolute;left:132.17px;top:257.79px" class="cls_023"><span class="cls_023">Fagaras</span></div>
<div style="position:absolute;left:207.05px;top:257.79px" class="cls_023"><span class="cls_023">Oradea</span></div>
<div style="position:absolute;left:273.60px;top:260.17px" class="cls_026"><span class="cls_026">Rimnicu Vilcea</span></div>
<div style="position:absolute;left:372.94px;top:257.79px" class="cls_024"><span class="cls_024">Arad</span></div>
<div style="position:absolute;left:444.88px;top:257.79px" class="cls_024"><span class="cls_024">Lugoj</span></div>
<div style="position:absolute;left:532.48px;top:257.79px" class="cls_024"><span class="cls_024">Arad</span></div>
<div style="position:absolute;left:598.54px;top:257.79px" class="cls_024"><span class="cls_024">Oradea</span></div>
<div style="position:absolute;left:589.58px;top:567.57px" class="cls_004"><span class="cls_004">Chapter 3</span></div>
<div style="position:absolute;left:643.54px;top:567.57px" class="cls_004"><span class="cls_004">28</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:10574px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="SolvingProblemsbySearch/background18.jpg" width=792 height=612></div>
<div style="position:absolute;left:138.26px;top:76.03px" class="cls_005"><span class="cls_005">Implementation: states vs. nodes</span></div>
<div style="position:absolute;left:40.10px;top:128.76px" class="cls_003"><span class="cls_003">A</span><span class="cls_013"> state</span><span class="cls_003"> is a (representation of) a physical configuration</span></div>
<div style="position:absolute;left:40.10px;top:153.72px" class="cls_003"><span class="cls_003">A</span><span class="cls_013"> node</span><span class="cls_003"> is a data structure constituting part of a search tree</span></div>
<div style="position:absolute;left:97.70px;top:178.56px" class="cls_003"><span class="cls_003">includes</span><span class="cls_010"> parent</span><span class="cls_003">,</span><span class="cls_010"> children</span><span class="cls_003">,</span><span class="cls_010"> depth</span><span class="cls_003">,</span><span class="cls_010"> path cost</span><span class="cls_014"> g(x)</span></div>
<div style="position:absolute;left:40.10px;top:203.52px" class="cls_003"><span class="cls_003">States do not have parents, children, depth, or path cost!</span></div>
<div style="position:absolute;left:430.95px;top:228.12px" class="cls_027"><span class="cls_027">parent, action</span></div>
<div style="position:absolute;left:480.64px;top:286.89px" class="cls_027"><span class="cls_027">depth = 6</span></div>
<div style="position:absolute;left:148.37px;top:293.21px" class="cls_029"><span class="cls_029">State</span></div>
<div style="position:absolute;left:212.31px;top:296.10px" class="cls_028"><span class="cls_028">5</span></div>
<div style="position:absolute;left:248.82px;top:296.10px" class="cls_028"><span class="cls_028">4</span></div>
<div style="position:absolute;left:383.66px;top:293.21px" class="cls_029"><span class="cls_029">Node</span></div>
<div style="position:absolute;left:480.64px;top:316.52px" class="cls_027"><span class="cls_027">g = 6</span></div>
<div style="position:absolute;left:212.31px;top:332.61px" class="cls_028"><span class="cls_028">6</span></div>
<div style="position:absolute;left:248.56px;top:332.61px" class="cls_028"><span class="cls_028">1</span></div>
<div style="position:absolute;left:285.07px;top:332.61px" class="cls_028"><span class="cls_028">8</span></div>
<div style="position:absolute;left:211.78px;top:369.38px" class="cls_028"><span class="cls_028">7</span></div>
<div style="position:absolute;left:248.82px;top:369.38px" class="cls_028"><span class="cls_028">3</span></div>
<div style="position:absolute;left:284.80px;top:369.38px" class="cls_028"><span class="cls_028">2</span></div>
<div style="position:absolute;left:40.10px;top:418.44px" class="cls_003"><span class="cls_003">The Expand function creates new nodes, filling in the various fields and</span></div>
<div style="position:absolute;left:40.10px;top:443.28px" class="cls_003"><span class="cls_003">using the SuccessorFn of the problem to create the corresponding states.</span></div>
<div style="position:absolute;left:589.58px;top:567.57px" class="cls_004"><span class="cls_004">Chapter 3</span></div>
<div style="position:absolute;left:643.54px;top:567.57px" class="cls_004"><span class="cls_004">29</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:11196px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="SolvingProblemsbySearch/background19.jpg" width=792 height=612></div>
<div style="position:absolute;left:118.46px;top:76.03px" class="cls_005"><span class="cls_005">Implementation: general tree search</span></div>
<div style="position:absolute;left:56.54px;top:141.68px" class="cls_006"><span class="cls_006">function</span><span class="cls_007"> Tree-Search</span><span class="cls_008">(</span><span class="cls_009"> problem, fringe</span><span class="cls_008">)</span><span class="cls_006"> returns</span><span class="cls_008"> a solution, or failure</span></div>
<div style="position:absolute;left:78.02px;top:163.52px" class="cls_009"><span class="cls_009">fringe</span><span class="cls_008"> ← Insert(Make-Node(Initial-State[</span><span class="cls_009">problem</span><span class="cls_008">]),</span><span class="cls_009"> fringe</span><span class="cls_008">)</span></div>
<div style="position:absolute;left:78.02px;top:185.48px" class="cls_006"><span class="cls_006">loop do</span></div>
<div style="position:absolute;left:110.42px;top:207.44px" class="cls_006"><span class="cls_006">if</span><span class="cls_009"> fringe</span><span class="cls_008"> is empty</span><span class="cls_006"> then return</span><span class="cls_008"> failure</span></div>
<div style="position:absolute;left:110.42px;top:229.28px" class="cls_009"><span class="cls_009">node</span><span class="cls_008"> ← Remove-Front(</span><span class="cls_009">fringe</span><span class="cls_008">)</span></div>
<div style="position:absolute;left:110.42px;top:251.24px" class="cls_006"><span class="cls_006">if</span><span class="cls_008"> Goal-Test(</span><span class="cls_009">problem</span><span class="cls_008">, State(</span><span class="cls_009">node</span><span class="cls_008">))</span><span class="cls_006"> then return</span><span class="cls_009"> node</span></div>
<div style="position:absolute;left:110.42px;top:273.20px" class="cls_009"><span class="cls_009">fringe</span><span class="cls_008"> ← InsertAll(Expand(</span><span class="cls_009">node</span><span class="cls_008">,</span><span class="cls_009"> problem</span><span class="cls_008">),</span><span class="cls_009"> fringe</span><span class="cls_008">)</span></div>
<div style="position:absolute;left:56.54px;top:317.00px" class="cls_006"><span class="cls_006">function</span><span class="cls_007"> Expand</span><span class="cls_008">(</span><span class="cls_009"> node, problem</span><span class="cls_008">)</span><span class="cls_006"> returns</span><span class="cls_008"> a set of nodes</span></div>
<div style="position:absolute;left:78.02px;top:338.96px" class="cls_009"><span class="cls_009">successors</span><span class="cls_008"> ← the empty set</span></div>
<div style="position:absolute;left:78.02px;top:360.80px" class="cls_006"><span class="cls_006">for each</span><span class="cls_009"> action, result</span><span class="cls_006"> in</span><span class="cls_008"> Successor-Fn(</span><span class="cls_009">problem</span><span class="cls_008">, State[</span><span class="cls_009">node</span><span class="cls_008">])</span><span class="cls_006"> do</span></div>
<div style="position:absolute;left:110.42px;top:382.76px" class="cls_009"><span class="cls_009">s</span><span class="cls_008">←a new Node</span></div>
<div style="position:absolute;left:110.42px;top:404.72px" class="cls_008"><span class="cls_008">Parent-Node[</span><span class="cls_009">s</span><span class="cls_008">] ←</span><span class="cls_009"> node</span><span class="cls_008">;  Action[</span><span class="cls_009">s</span><span class="cls_008">] ←</span><span class="cls_009"> action</span><span class="cls_008">;  State[</span><span class="cls_009">s</span><span class="cls_008">] ←</span><span class="cls_009"> result</span></div>
<div style="position:absolute;left:110.42px;top:426.56px" class="cls_008"><span class="cls_008">Path-Cost[</span><span class="cls_009">s</span><span class="cls_008">] ← Path-Cost[</span><span class="cls_009">node</span><span class="cls_008">] + Step-Cost(</span><span class="cls_009">node</span><span class="cls_008">,</span><span class="cls_009"> action</span><span class="cls_008">,</span><span class="cls_009"> s</span><span class="cls_008">)</span></div>
<div style="position:absolute;left:110.42px;top:448.52px" class="cls_008"><span class="cls_008">Depth[</span><span class="cls_009">s</span><span class="cls_008">] ← Depth[</span><span class="cls_009">node</span><span class="cls_008">] + 1</span></div>
<div style="position:absolute;left:110.42px;top:470.48px" class="cls_008"><span class="cls_008">add</span><span class="cls_009"> s</span><span class="cls_008"> to</span><span class="cls_009"> successors</span></div>
<div style="position:absolute;left:78.02px;top:492.32px" class="cls_006"><span class="cls_006">return</span><span class="cls_009"> successors</span></div>
<div style="position:absolute;left:589.58px;top:567.57px" class="cls_004"><span class="cls_004">Chapter 3</span></div>
<div style="position:absolute;left:643.54px;top:567.57px" class="cls_004"><span class="cls_004">30</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:11818px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="SolvingProblemsbySearch/background20.jpg" width=792 height=612></div>
<div style="position:absolute;left:240.50px;top:76.03px" class="cls_005"><span class="cls_005">Search strategies</span></div>
<div style="position:absolute;left:40.10px;top:127.68px" class="cls_003"><span class="cls_003">A strategy is defined by picking the</span><span class="cls_015"> order of node expansion</span></div>
<div style="position:absolute;left:40.10px;top:167.88px" class="cls_003"><span class="cls_003">Strategies are evaluated along the following dimensions:</span></div>
<div style="position:absolute;left:97.70px;top:192.72px" class="cls_013"><span class="cls_013">completeness</span><span class="cls_003">—does it always find a solution if one exists?</span></div>
<div style="position:absolute;left:97.70px;top:217.68px" class="cls_013"><span class="cls_013">time complexity</span><span class="cls_003">—number of nodes generated/expanded</span></div>
<div style="position:absolute;left:97.70px;top:242.64px" class="cls_013"><span class="cls_013">space complexity</span><span class="cls_003">—maximum number of nodes in memory</span></div>
<div style="position:absolute;left:97.70px;top:267.48px" class="cls_013"><span class="cls_013">optimality</span><span class="cls_003">—does it always find a least-cost solution?</span></div>
<div style="position:absolute;left:40.10px;top:307.68px" class="cls_003"><span class="cls_003">Time and space complexity are measured in terms of</span></div>
<div style="position:absolute;left:97.70px;top:332.64px" class="cls_014"><span class="cls_014">b</span><span class="cls_003">—maximum branching factor of the search tree</span></div>
<div style="position:absolute;left:97.70px;top:357.48px" class="cls_014"><span class="cls_014">d</span><span class="cls_003">—depth of the least-cost solution</span></div>
<div style="position:absolute;left:97.70px;top:382.44px" class="cls_014"><span class="cls_014">m</span><span class="cls_003">—maximum depth of the state space (may be</span><span class="cls_014"> ∞</span><span class="cls_003">)</span></div>
<div style="position:absolute;left:589.58px;top:567.57px" class="cls_004"><span class="cls_004">Chapter 3</span></div>
<div style="position:absolute;left:643.54px;top:567.57px" class="cls_004"><span class="cls_004">31</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:12440px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="SolvingProblemsbySearch/background21.jpg" width=792 height=612></div>
<div style="position:absolute;left:163.22px;top:76.03px" class="cls_005"><span class="cls_005">Uninformed search strategies</span></div>
<div style="position:absolute;left:40.10px;top:127.68px" class="cls_013"><span class="cls_013">Uninformed</span><span class="cls_003"> strategies use only the information available</span></div>
<div style="position:absolute;left:40.10px;top:152.52px" class="cls_003"><span class="cls_003">in the problem definition</span></div>
<div style="position:absolute;left:40.10px;top:192.72px" class="cls_003"><span class="cls_003">Breadth-first search</span></div>
<div style="position:absolute;left:40.10px;top:232.92px" class="cls_003"><span class="cls_003">Uniform-cost search</span></div>
<div style="position:absolute;left:40.10px;top:273.12px" class="cls_003"><span class="cls_003">Depth-first search</span></div>
<div style="position:absolute;left:40.10px;top:313.32px" class="cls_003"><span class="cls_003">Depth-limited search</span></div>
<div style="position:absolute;left:40.10px;top:353.64px" class="cls_003"><span class="cls_003">Iterative deepening search</span></div>
<div style="position:absolute;left:589.58px;top:567.57px" class="cls_004"><span class="cls_004">Chapter 3</span></div>
<div style="position:absolute;left:643.54px;top:567.57px" class="cls_004"><span class="cls_004">32</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:13062px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="SolvingProblemsbySearch/background22.jpg" width=792 height=612></div>
<div style="position:absolute;left:221.42px;top:76.03px" class="cls_005"><span class="cls_005">Breadth-first search</span></div>
<div style="position:absolute;left:40.10px;top:126.36px" class="cls_003"><span class="cls_003">Expand shallowest unexpanded node</span></div>
<div style="position:absolute;left:40.10px;top:166.56px" class="cls_015"><span class="cls_015">Implementation</span><span class="cls_003">:</span></div>
<div style="position:absolute;left:97.70px;top:191.52px" class="cls_010"><span class="cls_010">fringe</span><span class="cls_003"> is a FIFO queue, i.e., new successors go at end</span></div>
<div style="position:absolute;left:338.55px;top:218.81px" class="cls_030"><span class="cls_030">A</span></div>
<div style="position:absolute;left:251.04px;top:298.37px" class="cls_031"><span class="cls_031">B</span></div>
<div style="position:absolute;left:426.95px;top:298.37px" class="cls_031"><span class="cls_031">C</span></div>
<div style="position:absolute;left:205.05px;top:377.93px" class="cls_031"><span class="cls_031">D</span></div>
<div style="position:absolute;left:294.35px;top:377.93px" class="cls_031"><span class="cls_031">E</span></div>
<div style="position:absolute;left:382.75px;top:377.93px" class="cls_031"><span class="cls_031">F</span></div>
<div style="position:absolute;left:470.26px;top:377.93px" class="cls_031"><span class="cls_031">G</span></div>
<div style="position:absolute;left:589.58px;top:567.57px" class="cls_004"><span class="cls_004">Chapter 3</span></div>
<div style="position:absolute;left:643.54px;top:567.57px" class="cls_004"><span class="cls_004">33</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:13684px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="SolvingProblemsbySearch/background23.jpg" width=792 height=612></div>
<div style="position:absolute;left:221.42px;top:76.03px" class="cls_005"><span class="cls_005">Breadth-first search</span></div>
<div style="position:absolute;left:40.10px;top:126.36px" class="cls_003"><span class="cls_003">Expand shallowest unexpanded node</span></div>
<div style="position:absolute;left:40.10px;top:166.56px" class="cls_015"><span class="cls_015">Implementation</span><span class="cls_003">:</span></div>
<div style="position:absolute;left:97.70px;top:191.52px" class="cls_010"><span class="cls_010">fringe</span><span class="cls_003"> is a FIFO queue, i.e., new successors go at end</span></div>
<div style="position:absolute;left:340.31px;top:218.74px" class="cls_032"><span class="cls_032">A</span></div>
<div style="position:absolute;left:253.34px;top:297.80px" class="cls_032"><span class="cls_032">B</span></div>
<div style="position:absolute;left:428.16px;top:297.80px" class="cls_032"><span class="cls_032">C</span></div>
<div style="position:absolute;left:207.65px;top:376.87px" class="cls_033"><span class="cls_033">D</span></div>
<div style="position:absolute;left:296.38px;top:376.87px" class="cls_033"><span class="cls_033">E</span></div>
<div style="position:absolute;left:384.23px;top:376.87px" class="cls_033"><span class="cls_033">F</span></div>
<div style="position:absolute;left:471.20px;top:376.87px" class="cls_033"><span class="cls_033">G</span></div>
<div style="position:absolute;left:589.58px;top:567.57px" class="cls_004"><span class="cls_004">Chapter 3</span></div>
<div style="position:absolute;left:643.54px;top:567.57px" class="cls_004"><span class="cls_004">34</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:14306px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="SolvingProblemsbySearch/background24.jpg" width=792 height=612></div>
<div style="position:absolute;left:221.42px;top:76.03px" class="cls_005"><span class="cls_005">Breadth-first search</span></div>
<div style="position:absolute;left:40.10px;top:126.36px" class="cls_003"><span class="cls_003">Expand shallowest unexpanded node</span></div>
<div style="position:absolute;left:40.10px;top:166.56px" class="cls_015"><span class="cls_015">Implementation</span><span class="cls_003">:</span></div>
<div style="position:absolute;left:97.70px;top:191.52px" class="cls_010"><span class="cls_010">fringe</span><span class="cls_003"> is a FIFO queue, i.e., new successors go at end</span></div>
<div style="position:absolute;left:340.15px;top:218.78px" class="cls_034"><span class="cls_034">A</span></div>
<div style="position:absolute;left:253.72px;top:297.35px" class="cls_034"><span class="cls_034">B</span></div>
<div style="position:absolute;left:427.45px;top:297.35px" class="cls_034"><span class="cls_034">C</span></div>
<div style="position:absolute;left:208.31px;top:375.93px" class="cls_034"><span class="cls_034">D</span></div>
<div style="position:absolute;left:296.49px;top:375.93px" class="cls_034"><span class="cls_034">E</span></div>
<div style="position:absolute;left:383.80px;top:375.93px" class="cls_035"><span class="cls_035">F</span></div>
<div style="position:absolute;left:470.22px;top:375.93px" class="cls_035"><span class="cls_035">G</span></div>
<div style="position:absolute;left:589.58px;top:567.57px" class="cls_004"><span class="cls_004">Chapter 3</span></div>
<div style="position:absolute;left:643.54px;top:567.57px" class="cls_004"><span class="cls_004">35</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:14928px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="SolvingProblemsbySearch/background25.jpg" width=792 height=612></div>
<div style="position:absolute;left:221.42px;top:76.03px" class="cls_005"><span class="cls_005">Breadth-first search</span></div>
<div style="position:absolute;left:40.10px;top:126.36px" class="cls_003"><span class="cls_003">Expand shallowest unexpanded node</span></div>
<div style="position:absolute;left:40.10px;top:166.56px" class="cls_015"><span class="cls_015">Implementation</span><span class="cls_003">:</span></div>
<div style="position:absolute;left:97.70px;top:191.52px" class="cls_010"><span class="cls_010">fringe</span><span class="cls_003"> is a FIFO queue, i.e., new successors go at end</span></div>
<div style="position:absolute;left:353.55px;top:218.66px" class="cls_036"><span class="cls_036">A</span></div>
<div style="position:absolute;left:274.04px;top:290.95px" class="cls_036"><span class="cls_036">B</span></div>
<div style="position:absolute;left:433.87px;top:290.95px" class="cls_036"><span class="cls_036">C</span></div>
<div style="position:absolute;left:232.26px;top:363.23px" class="cls_036"><span class="cls_036">D</span></div>
<div style="position:absolute;left:313.39px;top:363.23px" class="cls_036"><span class="cls_036">E</span></div>
<div style="position:absolute;left:393.71px;top:363.23px" class="cls_036"><span class="cls_036">F</span></div>
<div style="position:absolute;left:473.23px;top:363.23px" class="cls_036"><span class="cls_036">G</span></div>
<div style="position:absolute;left:589.58px;top:567.57px" class="cls_004"><span class="cls_004">Chapter 3</span></div>
<div style="position:absolute;left:643.54px;top:567.57px" class="cls_004"><span class="cls_004">36</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:15550px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="SolvingProblemsbySearch/background26.jpg" width=792 height=612></div>
<div style="position:absolute;left:137.06px;top:76.03px" class="cls_005"><span class="cls_005">Properties of breadth-first search</span></div>
<div style="position:absolute;left:40.10px;top:127.68px" class="cls_018"><span class="cls_018">Complete??</span></div>
<div style="position:absolute;left:589.58px;top:567.57px" class="cls_004"><span class="cls_004">Chapter 3</span></div>
<div style="position:absolute;left:643.54px;top:567.57px" class="cls_004"><span class="cls_004">37</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:16172px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="SolvingProblemsbySearch/background27.jpg" width=792 height=612></div>
<div style="position:absolute;left:137.06px;top:76.03px" class="cls_005"><span class="cls_005">Properties of breadth-first search</span></div>
<div style="position:absolute;left:40.10px;top:128.76px" class="cls_018"><span class="cls_018">Complete??</span><span class="cls_003"> Yes (if</span><span class="cls_014"> b</span><span class="cls_003"> is finite)</span></div>
<div style="position:absolute;left:40.10px;top:168.96px" class="cls_018"><span class="cls_018">Time??</span></div>
<div style="position:absolute;left:589.58px;top:567.57px" class="cls_004"><span class="cls_004">Chapter 3</span></div>
<div style="position:absolute;left:643.54px;top:567.57px" class="cls_004"><span class="cls_004">38</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:16794px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="SolvingProblemsbySearch/background28.jpg" width=792 height=612></div>
<div style="position:absolute;left:137.06px;top:76.03px" class="cls_005"><span class="cls_005">Properties of breadth-first search</span></div>
<div style="position:absolute;left:40.10px;top:128.76px" class="cls_018"><span class="cls_018">Complete??</span><span class="cls_003"> Yes (if</span><span class="cls_014"> b</span><span class="cls_003"> is finite)</span></div>
<div style="position:absolute;left:40.10px;top:167.83px" class="cls_018"><span class="cls_018">Time??</span><span class="cls_014"> 1 + b + b</span><span class="cls_037"><sup>2</sup></span><span class="cls_014"> + b</span><span class="cls_037"><sup>3</sup></span><span class="cls_014"> + . . . + b</span><span class="cls_037"><sup>d</sup></span><span class="cls_014"> + b(b</span><span class="cls_037"><sup>d</sup></span><span class="cls_014"> − 1) = O(b</span><span class="cls_037"><sup>d+1</sup></span><span class="cls_014">)</span><span class="cls_003">, i.e., exp. in</span><span class="cls_014"> d</span></div>
<div style="position:absolute;left:40.10px;top:209.16px" class="cls_018"><span class="cls_018">Space??</span></div>
<div style="position:absolute;left:589.58px;top:567.57px" class="cls_004"><span class="cls_004">Chapter 3</span></div>
<div style="position:absolute;left:643.54px;top:567.57px" class="cls_004"><span class="cls_004">39</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:17416px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="SolvingProblemsbySearch/background29.jpg" width=792 height=612></div>
<div style="position:absolute;left:137.06px;top:76.03px" class="cls_005"><span class="cls_005">Properties of breadth-first search</span></div>
<div style="position:absolute;left:40.10px;top:128.76px" class="cls_018"><span class="cls_018">Complete??</span><span class="cls_003"> Yes (if</span><span class="cls_014"> b</span><span class="cls_003"> is finite)</span></div>
<div style="position:absolute;left:40.10px;top:167.83px" class="cls_018"><span class="cls_018">Time??</span><span class="cls_014"> 1 + b + b</span><span class="cls_037"><sup>2</sup></span><span class="cls_014"> + b</span><span class="cls_037"><sup>3</sup></span><span class="cls_014"> + . . . + b</span><span class="cls_037"><sup>d</sup></span><span class="cls_014"> + b(b</span><span class="cls_037"><sup>d</sup></span><span class="cls_014"> − 1) = O(b</span><span class="cls_037"><sup>d+1</sup></span><span class="cls_014">)</span><span class="cls_003">, i.e., exp. in</span><span class="cls_014"> d</span></div>
<div style="position:absolute;left:40.10px;top:208.03px" class="cls_018"><span class="cls_018">Space??</span><span class="cls_014"> O(b</span><span class="cls_037"><sup>d+1</sup></span><span class="cls_014">)</span><span class="cls_003"> (keeps every node in memory)</span></div>
<div style="position:absolute;left:40.10px;top:249.36px" class="cls_018"><span class="cls_018">Optimal??</span></div>
<div style="position:absolute;left:589.58px;top:567.57px" class="cls_004"><span class="cls_004">Chapter 3</span></div>
<div style="position:absolute;left:643.54px;top:567.57px" class="cls_004"><span class="cls_004">40</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:18038px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="SolvingProblemsbySearch/background30.jpg" width=792 height=612></div>
<div style="position:absolute;left:137.06px;top:76.03px" class="cls_005"><span class="cls_005">Properties of breadth-first search</span></div>
<div style="position:absolute;left:40.10px;top:128.76px" class="cls_018"><span class="cls_018">Complete??</span><span class="cls_003"> Yes (if</span><span class="cls_014"> b</span><span class="cls_003"> is finite)</span></div>
<div style="position:absolute;left:40.10px;top:167.83px" class="cls_018"><span class="cls_018">Time??</span><span class="cls_014"> 1 + b + b</span><span class="cls_037"><sup>2</sup></span><span class="cls_014"> + b</span><span class="cls_037"><sup>3</sup></span><span class="cls_014"> + . . . + b</span><span class="cls_037"><sup>d</sup></span><span class="cls_014"> + b(b</span><span class="cls_037"><sup>d</sup></span><span class="cls_014"> − 1) = O(b</span><span class="cls_037"><sup>d+1</sup></span><span class="cls_014">)</span><span class="cls_003">, i.e., exp. in</span><span class="cls_014"> d</span></div>
<div style="position:absolute;left:40.10px;top:208.03px" class="cls_018"><span class="cls_018">Space??</span><span class="cls_014"> O(b</span><span class="cls_037"><sup>d+1</sup></span><span class="cls_014">)</span><span class="cls_003"> (keeps every node in memory)</span></div>
<div style="position:absolute;left:40.10px;top:249.36px" class="cls_018"><span class="cls_018">Optimal??</span><span class="cls_003"> Yes (if cost = 1 per step); not optimal in general</span></div>
<div style="position:absolute;left:40.10px;top:289.56px" class="cls_015"><span class="cls_015">Space</span><span class="cls_003"> is the big problem; can easily generate nodes at 100MB/sec</span></div>
<div style="position:absolute;left:97.70px;top:314.52px" class="cls_003"><span class="cls_003">so 24hrs = 8640GB.</span></div>
<div style="position:absolute;left:589.58px;top:567.57px" class="cls_004"><span class="cls_004">Chapter 3</span></div>
<div style="position:absolute;left:643.54px;top:567.57px" class="cls_004"><span class="cls_004">41</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:18660px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="SolvingProblemsbySearch/background31.jpg" width=792 height=612></div>
<div style="position:absolute;left:220.46px;top:76.03px" class="cls_005"><span class="cls_005">Uniform-cost search</span></div>
<div style="position:absolute;left:40.10px;top:126.36px" class="cls_003"><span class="cls_003">Expand least-cost unexpanded node</span></div>
<div style="position:absolute;left:40.10px;top:166.56px" class="cls_015"><span class="cls_015">Implementation</span><span class="cls_003">:</span></div>
<div style="position:absolute;left:97.70px;top:191.52px" class="cls_010"><span class="cls_010">fringe</span><span class="cls_003"> = queue ordered by path cost, lowest first</span></div>
<div style="position:absolute;left:40.10px;top:231.72px" class="cls_003"><span class="cls_003">Equivalent to breadth-first if step costs all equal</span></div>
<div style="position:absolute;left:40.10px;top:271.92px" class="cls_018"><span class="cls_018">Complete??</span><span class="cls_003"> Yes, if step cost</span><span class="cls_014"> ≥ ϵ</span></div>
<div style="position:absolute;left:40.10px;top:308.10px" class="cls_018"><span class="cls_018">Time??</span><span class="cls_003"> # of nodes with</span><span class="cls_014"> g ≤</span><span class="cls_003">  cost of optimal solution,</span><span class="cls_014"> O(b</span><span class="cls_037">⌈C</span><span class="cls_038">∗</span><span class="cls_037">/ϵ⌉</span><span class="cls_014">)</span></div>
<div style="position:absolute;left:68.90px;top:335.83px" class="cls_003"><span class="cls_003">where</span><span class="cls_014"> C</span><span class="cls_037"><sup>∗</sup></span><span class="cls_003"> is the cost of the optimal solution</span></div>
<div style="position:absolute;left:40.10px;top:373.26px" class="cls_018"><span class="cls_018">Space??</span><span class="cls_003"> # of nodes with</span><span class="cls_014"> g ≤</span><span class="cls_003">  cost of optimal solution,</span><span class="cls_014"> O(b</span><span class="cls_037">⌈C</span><span class="cls_038">∗</span><span class="cls_037">/ϵ⌉</span><span class="cls_014">)</span></div>
<div style="position:absolute;left:40.10px;top:417.48px" class="cls_018"><span class="cls_018">Optimal??</span><span class="cls_003"> Yes—nodes expanded in increasing order of</span><span class="cls_014"> g(n)</span></div>
<div style="position:absolute;left:589.58px;top:567.57px" class="cls_004"><span class="cls_004">Chapter 3</span></div>
<div style="position:absolute;left:643.54px;top:567.57px" class="cls_004"><span class="cls_004">42</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:19282px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="SolvingProblemsbySearch/background32.jpg" width=792 height=612></div>
<div style="position:absolute;left:233.42px;top:76.03px" class="cls_005"><span class="cls_005">Depth-first search</span></div>
<div style="position:absolute;left:40.10px;top:127.68px" class="cls_003"><span class="cls_003">Expand deepest unexpanded node</span></div>
<div style="position:absolute;left:40.10px;top:167.88px" class="cls_015"><span class="cls_015">Implementation</span><span class="cls_003">:</span></div>
<div style="position:absolute;left:97.70px;top:192.72px" class="cls_010"><span class="cls_010">fringe</span><span class="cls_003"> = LIFO queue, i.e., put successors at front</span></div>
<div style="position:absolute;left:341.98px;top:221.43px" class="cls_039"><span class="cls_039">A</span></div>
<div style="position:absolute;left:264.05px;top:270.51px" class="cls_040"><span class="cls_040">B</span></div>
<div style="position:absolute;left:420.51px;top:270.51px" class="cls_040"><span class="cls_040">C</span></div>
<div style="position:absolute;left:223.60px;top:319.59px" class="cls_040"><span class="cls_040">D</span></div>
<div style="position:absolute;left:302.72px;top:319.59px" class="cls_040"><span class="cls_040">E</span></div>
<div style="position:absolute;left:381.24px;top:319.59px" class="cls_040"><span class="cls_040">F</span></div>
<div style="position:absolute;left:459.17px;top:319.59px" class="cls_040"><span class="cls_040">G</span></div>
<div style="position:absolute;left:203.96px;top:368.66px" class="cls_040"><span class="cls_040">H    I    J   K   L   M   N   O</span></div>
<div style="position:absolute;left:589.58px;top:567.57px" class="cls_004"><span class="cls_004">Chapter 3</span></div>
<div style="position:absolute;left:643.54px;top:567.57px" class="cls_004"><span class="cls_004">43</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:19904px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="SolvingProblemsbySearch/background33.jpg" width=792 height=612></div>
<div style="position:absolute;left:233.42px;top:76.03px" class="cls_005"><span class="cls_005">Depth-first search</span></div>
<div style="position:absolute;left:40.10px;top:127.68px" class="cls_003"><span class="cls_003">Expand deepest unexpanded node</span></div>
<div style="position:absolute;left:40.10px;top:167.88px" class="cls_015"><span class="cls_015">Implementation</span><span class="cls_003">:</span></div>
<div style="position:absolute;left:97.70px;top:192.72px" class="cls_010"><span class="cls_010">fringe</span><span class="cls_003"> = LIFO queue, i.e., put successors at front</span></div>
<div style="position:absolute;left:342.66px;top:221.43px" class="cls_039"><span class="cls_039">A</span></div>
<div style="position:absolute;left:264.73px;top:270.51px" class="cls_039"><span class="cls_039">B</span></div>
<div style="position:absolute;left:421.18px;top:270.51px" class="cls_039"><span class="cls_039">C</span></div>
<div style="position:absolute;left:224.27px;top:319.58px" class="cls_040"><span class="cls_040">D</span></div>
<div style="position:absolute;left:303.39px;top:319.58px" class="cls_040"><span class="cls_040">E</span></div>
<div style="position:absolute;left:381.92px;top:319.58px" class="cls_040"><span class="cls_040">F</span></div>
<div style="position:absolute;left:459.85px;top:319.58px" class="cls_040"><span class="cls_040">G</span></div>
<div style="position:absolute;left:204.64px;top:368.66px" class="cls_040"><span class="cls_040">H    I    J   K   L   M   N   O</span></div>
<div style="position:absolute;left:589.58px;top:567.57px" class="cls_004"><span class="cls_004">Chapter 3</span></div>
<div style="position:absolute;left:643.54px;top:567.57px" class="cls_004"><span class="cls_004">44</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:20526px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="SolvingProblemsbySearch/background34.jpg" width=792 height=612></div>
<div style="position:absolute;left:233.42px;top:76.03px" class="cls_005"><span class="cls_005">Depth-first search</span></div>
<div style="position:absolute;left:40.10px;top:127.68px" class="cls_003"><span class="cls_003">Expand deepest unexpanded node</span></div>
<div style="position:absolute;left:40.10px;top:167.88px" class="cls_015"><span class="cls_015">Implementation</span><span class="cls_003">:</span></div>
<div style="position:absolute;left:97.70px;top:192.72px" class="cls_010"><span class="cls_010">fringe</span><span class="cls_003"> = LIFO queue, i.e., put successors at front</span></div>
<div style="position:absolute;left:342.46px;top:221.40px" class="cls_041"><span class="cls_041">A</span></div>
<div style="position:absolute;left:264.09px;top:270.76px" class="cls_041"><span class="cls_041">B</span></div>
<div style="position:absolute;left:421.42px;top:270.76px" class="cls_041"><span class="cls_041">C</span></div>
<div style="position:absolute;left:223.41px;top:320.11px" class="cls_041"><span class="cls_041">D</span></div>
<div style="position:absolute;left:302.97px;top:320.11px" class="cls_041"><span class="cls_041">E</span></div>
<div style="position:absolute;left:381.94px;top:320.11px" class="cls_042"><span class="cls_042">F</span></div>
<div style="position:absolute;left:460.31px;top:320.11px" class="cls_042"><span class="cls_042">G</span></div>
<div style="position:absolute;left:203.67px;top:369.47px" class="cls_042"><span class="cls_042">H    I    J   K   L   M   N   O</span></div>
<div style="position:absolute;left:589.58px;top:567.57px" class="cls_004"><span class="cls_004">Chapter 3</span></div>
<div style="position:absolute;left:643.54px;top:567.57px" class="cls_004"><span class="cls_004">45</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:21148px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="SolvingProblemsbySearch/background35.jpg" width=792 height=612></div>
<div style="position:absolute;left:233.42px;top:76.03px" class="cls_005"><span class="cls_005">Depth-first search</span></div>
<div style="position:absolute;left:40.10px;top:127.68px" class="cls_003"><span class="cls_003">Expand deepest unexpanded node</span></div>
<div style="position:absolute;left:40.10px;top:167.88px" class="cls_015"><span class="cls_015">Implementation</span><span class="cls_003">:</span></div>
<div style="position:absolute;left:97.70px;top:192.72px" class="cls_010"><span class="cls_010">fringe</span><span class="cls_003"> = LIFO queue, i.e., put successors at front</span></div>
<div style="position:absolute;left:347.08px;top:221.32px" class="cls_043"><span class="cls_043">A</span></div>
<div style="position:absolute;left:271.67px;top:268.81px" class="cls_043"><span class="cls_043">B</span></div>
<div style="position:absolute;left:423.05px;top:268.81px" class="cls_043"><span class="cls_043">C</span></div>
<div style="position:absolute;left:232.53px;top:316.30px" class="cls_043"><span class="cls_043">D</span></div>
<div style="position:absolute;left:309.09px;top:316.30px" class="cls_043"><span class="cls_043">E</span></div>
<div style="position:absolute;left:385.07px;top:316.30px" class="cls_044"><span class="cls_044">F</span></div>
<div style="position:absolute;left:460.47px;top:316.30px" class="cls_044"><span class="cls_044">G</span></div>
<div style="position:absolute;left:213.54px;top:363.78px" class="cls_043"><span class="cls_043">H    I</span><span class="cls_044">    J   K   L   M   N   O</span></div>
<div style="position:absolute;left:589.58px;top:567.57px" class="cls_004"><span class="cls_004">Chapter 3</span></div>
<div style="position:absolute;left:643.54px;top:567.57px" class="cls_004"><span class="cls_004">46</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:21770px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="SolvingProblemsbySearch/background36.jpg" width=792 height=612></div>
<div style="position:absolute;left:233.42px;top:76.03px" class="cls_005"><span class="cls_005">Depth-first search</span></div>
<div style="position:absolute;left:40.10px;top:127.68px" class="cls_003"><span class="cls_003">Expand deepest unexpanded node</span></div>
<div style="position:absolute;left:40.10px;top:167.88px" class="cls_015"><span class="cls_015">Implementation</span><span class="cls_003">:</span></div>
<div style="position:absolute;left:97.70px;top:192.72px" class="cls_010"><span class="cls_010">fringe</span><span class="cls_003"> = LIFO queue, i.e., put successors at front</span></div>
<div style="position:absolute;left:342.66px;top:221.41px" class="cls_039"><span class="cls_039">A</span></div>
<div style="position:absolute;left:264.73px;top:270.49px" class="cls_039"><span class="cls_039">B</span></div>
<div style="position:absolute;left:421.18px;top:270.49px" class="cls_039"><span class="cls_039">C</span></div>
<div style="position:absolute;left:224.27px;top:319.57px" class="cls_039"><span class="cls_039">D</span></div>
<div style="position:absolute;left:303.39px;top:319.57px" class="cls_039"><span class="cls_039">E</span></div>
<div style="position:absolute;left:381.92px;top:319.57px" class="cls_040"><span class="cls_040">F</span></div>
<div style="position:absolute;left:459.85px;top:319.57px" class="cls_040"><span class="cls_040">G</span></div>
<div style="position:absolute;left:204.64px;top:368.65px" class="cls_039"><span class="cls_039">H</span></div>
<div style="position:absolute;left:246.88px;top:368.65px" class="cls_039"><span class="cls_039">I</span></div>
<div style="position:absolute;left:285.55px;top:368.65px" class="cls_040"><span class="cls_040">J</span></div>
<div style="position:absolute;left:322.43px;top:368.65px" class="cls_040"><span class="cls_040">K   L   M   N   O</span></div>
<div style="position:absolute;left:589.58px;top:567.57px" class="cls_004"><span class="cls_004">Chapter 3</span></div>
<div style="position:absolute;left:643.54px;top:567.57px" class="cls_004"><span class="cls_004">47</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:22392px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="SolvingProblemsbySearch/background37.jpg" width=792 height=612></div>
<div style="position:absolute;left:233.42px;top:76.03px" class="cls_005"><span class="cls_005">Depth-first search</span></div>
<div style="position:absolute;left:40.10px;top:127.68px" class="cls_003"><span class="cls_003">Expand deepest unexpanded node</span></div>
<div style="position:absolute;left:40.10px;top:167.88px" class="cls_015"><span class="cls_015">Implementation</span><span class="cls_003">:</span></div>
<div style="position:absolute;left:97.70px;top:192.72px" class="cls_010"><span class="cls_010">fringe</span><span class="cls_003"> = LIFO queue, i.e., put successors at front</span></div>
<div style="position:absolute;left:342.46px;top:221.39px" class="cls_041"><span class="cls_041">A</span></div>
<div style="position:absolute;left:264.09px;top:270.74px" class="cls_041"><span class="cls_041">B</span></div>
<div style="position:absolute;left:421.42px;top:270.74px" class="cls_041"><span class="cls_041">C</span></div>
<div style="position:absolute;left:223.41px;top:320.10px" class="cls_041"><span class="cls_041">D</span></div>
<div style="position:absolute;left:302.98px;top:320.10px" class="cls_041"><span class="cls_041">E</span></div>
<div style="position:absolute;left:381.94px;top:320.10px" class="cls_042"><span class="cls_042">F</span></div>
<div style="position:absolute;left:460.31px;top:320.10px" class="cls_042"><span class="cls_042">G</span></div>
<div style="position:absolute;left:203.67px;top:369.45px" class="cls_041"><span class="cls_041">H    I</span><span class="cls_042">    J   K   L   M   N   O</span></div>
<div style="position:absolute;left:589.58px;top:567.57px" class="cls_004"><span class="cls_004">Chapter 3</span></div>
<div style="position:absolute;left:643.54px;top:567.57px" class="cls_004"><span class="cls_004">48</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:23014px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="SolvingProblemsbySearch/background38.jpg" width=792 height=612></div>
<div style="position:absolute;left:233.42px;top:76.03px" class="cls_005"><span class="cls_005">Depth-first search</span></div>
<div style="position:absolute;left:40.10px;top:127.68px" class="cls_003"><span class="cls_003">Expand deepest unexpanded node</span></div>
<div style="position:absolute;left:40.10px;top:167.88px" class="cls_015"><span class="cls_015">Implementation</span><span class="cls_003">:</span></div>
<div style="position:absolute;left:97.70px;top:192.72px" class="cls_010"><span class="cls_010">fringe</span><span class="cls_003"> = LIFO queue, i.e., put successors at front</span></div>
<div style="position:absolute;left:341.98px;top:220.13px" class="cls_039"><span class="cls_039">A</span></div>
<div style="position:absolute;left:264.05px;top:269.21px" class="cls_039"><span class="cls_039">B</span></div>
<div style="position:absolute;left:420.50px;top:269.21px" class="cls_039"><span class="cls_039">C</span></div>
<div style="position:absolute;left:223.60px;top:318.28px" class="cls_039"><span class="cls_039">D</span></div>
<div style="position:absolute;left:302.72px;top:318.28px" class="cls_039"><span class="cls_039">E</span></div>
<div style="position:absolute;left:381.24px;top:318.28px" class="cls_040"><span class="cls_040">F</span></div>
<div style="position:absolute;left:459.17px;top:318.28px" class="cls_040"><span class="cls_040">G</span></div>
<div style="position:absolute;left:203.96px;top:367.36px" class="cls_039"><span class="cls_039">H    I</span></div>
<div style="position:absolute;left:284.87px;top:367.36px" class="cls_039"><span class="cls_039">J</span></div>
<div style="position:absolute;left:321.75px;top:367.36px" class="cls_039"><span class="cls_039">K</span><span class="cls_040">   L   M   N   O</span></div>
<div style="position:absolute;left:589.58px;top:567.57px" class="cls_004"><span class="cls_004">Chapter 3</span></div>
<div style="position:absolute;left:643.54px;top:567.57px" class="cls_004"><span class="cls_004">49</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:23636px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="SolvingProblemsbySearch/background39.jpg" width=792 height=612></div>
<div style="position:absolute;left:233.42px;top:76.03px" class="cls_005"><span class="cls_005">Depth-first search</span></div>
<div style="position:absolute;left:40.10px;top:127.68px" class="cls_003"><span class="cls_003">Expand deepest unexpanded node</span></div>
<div style="position:absolute;left:40.10px;top:167.88px" class="cls_015"><span class="cls_015">Implementation</span><span class="cls_003">:</span></div>
<div style="position:absolute;left:97.70px;top:192.72px" class="cls_010"><span class="cls_010">fringe</span><span class="cls_003"> = LIFO queue, i.e., put successors at front</span></div>
<div style="position:absolute;left:342.66px;top:220.13px" class="cls_039"><span class="cls_039">A</span></div>
<div style="position:absolute;left:264.73px;top:269.21px" class="cls_039"><span class="cls_039">B</span></div>
<div style="position:absolute;left:421.18px;top:269.21px" class="cls_039"><span class="cls_039">C</span></div>
<div style="position:absolute;left:224.27px;top:318.28px" class="cls_039"><span class="cls_039">D</span></div>
<div style="position:absolute;left:303.39px;top:318.28px" class="cls_039"><span class="cls_039">E</span></div>
<div style="position:absolute;left:381.92px;top:318.28px" class="cls_040"><span class="cls_040">F</span></div>
<div style="position:absolute;left:459.85px;top:318.28px" class="cls_040"><span class="cls_040">G</span></div>
<div style="position:absolute;left:204.64px;top:367.36px" class="cls_039"><span class="cls_039">H    I    J</span></div>
<div style="position:absolute;left:322.43px;top:367.36px" class="cls_039"><span class="cls_039">K</span><span class="cls_040">   L   M   N   O</span></div>
<div style="position:absolute;left:589.58px;top:567.57px" class="cls_004"><span class="cls_004">Chapter 3</span></div>
<div style="position:absolute;left:643.54px;top:567.57px" class="cls_004"><span class="cls_004">50</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:24258px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="SolvingProblemsbySearch/background40.jpg" width=792 height=612></div>
<div style="position:absolute;left:233.42px;top:76.03px" class="cls_005"><span class="cls_005">Depth-first search</span></div>
<div style="position:absolute;left:40.10px;top:127.68px" class="cls_003"><span class="cls_003">Expand deepest unexpanded node</span></div>
<div style="position:absolute;left:40.10px;top:167.88px" class="cls_015"><span class="cls_015">Implementation</span><span class="cls_003">:</span></div>
<div style="position:absolute;left:97.70px;top:192.72px" class="cls_010"><span class="cls_010">fringe</span><span class="cls_003"> = LIFO queue, i.e., put successors at front</span></div>
<div style="position:absolute;left:342.46px;top:220.10px" class="cls_041"><span class="cls_041">A</span></div>
<div style="position:absolute;left:264.09px;top:269.45px" class="cls_041"><span class="cls_041">B</span></div>
<div style="position:absolute;left:421.42px;top:269.45px" class="cls_041"><span class="cls_041">C</span></div>
<div style="position:absolute;left:223.41px;top:318.81px" class="cls_041"><span class="cls_041">D</span></div>
<div style="position:absolute;left:302.98px;top:318.81px" class="cls_041"><span class="cls_041">E</span></div>
<div style="position:absolute;left:381.94px;top:318.81px" class="cls_042"><span class="cls_042">F</span></div>
<div style="position:absolute;left:460.31px;top:318.81px" class="cls_042"><span class="cls_042">G</span></div>
<div style="position:absolute;left:203.67px;top:368.16px" class="cls_041"><span class="cls_041">H    I    J   K</span><span class="cls_042">   L   M   N   O</span></div>
<div style="position:absolute;left:589.58px;top:567.57px" class="cls_004"><span class="cls_004">Chapter 3</span></div>
<div style="position:absolute;left:643.54px;top:567.57px" class="cls_004"><span class="cls_004">51</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:24880px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="SolvingProblemsbySearch/background41.jpg" width=792 height=612></div>
<div style="position:absolute;left:233.42px;top:76.03px" class="cls_005"><span class="cls_005">Depth-first search</span></div>
<div style="position:absolute;left:40.10px;top:127.68px" class="cls_003"><span class="cls_003">Expand deepest unexpanded node</span></div>
<div style="position:absolute;left:40.10px;top:167.88px" class="cls_015"><span class="cls_015">Implementation</span><span class="cls_003">:</span></div>
<div style="position:absolute;left:97.70px;top:192.72px" class="cls_010"><span class="cls_010">fringe</span><span class="cls_003"> = LIFO queue, i.e., put successors at front</span></div>
<div style="position:absolute;left:341.98px;top:223.13px" class="cls_039"><span class="cls_039">A</span></div>
<div style="position:absolute;left:264.05px;top:272.21px" class="cls_039"><span class="cls_039">B</span></div>
<div style="position:absolute;left:420.50px;top:272.21px" class="cls_039"><span class="cls_039">C</span></div>
<div style="position:absolute;left:223.60px;top:321.29px" class="cls_039"><span class="cls_039">D</span></div>
<div style="position:absolute;left:302.72px;top:321.29px" class="cls_039"><span class="cls_039">E</span></div>
<div style="position:absolute;left:381.24px;top:321.29px" class="cls_039"><span class="cls_039">F</span></div>
<div style="position:absolute;left:459.17px;top:321.29px" class="cls_039"><span class="cls_039">G</span></div>
<div style="position:absolute;left:203.97px;top:370.37px" class="cls_039"><span class="cls_039">H    I    J   K</span><span class="cls_040">   L   M   N   O</span></div>
<div style="position:absolute;left:589.58px;top:567.57px" class="cls_004"><span class="cls_004">Chapter 3</span></div>
<div style="position:absolute;left:643.54px;top:567.57px" class="cls_004"><span class="cls_004">52</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:25502px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="SolvingProblemsbySearch/background42.jpg" width=792 height=612></div>
<div style="position:absolute;left:233.42px;top:76.03px" class="cls_005"><span class="cls_005">Depth-first search</span></div>
<div style="position:absolute;left:40.10px;top:127.68px" class="cls_003"><span class="cls_003">Expand deepest unexpanded node</span></div>
<div style="position:absolute;left:40.10px;top:167.88px" class="cls_015"><span class="cls_015">Implementation</span><span class="cls_003">:</span></div>
<div style="position:absolute;left:97.70px;top:192.72px" class="cls_010"><span class="cls_010">fringe</span><span class="cls_003"> = LIFO queue, i.e., put successors at front</span></div>
<div style="position:absolute;left:342.65px;top:223.13px" class="cls_039"><span class="cls_039">A</span></div>
<div style="position:absolute;left:264.72px;top:272.21px" class="cls_039"><span class="cls_039">B</span></div>
<div style="position:absolute;left:421.18px;top:272.21px" class="cls_039"><span class="cls_039">C</span></div>
<div style="position:absolute;left:224.27px;top:321.29px" class="cls_039"><span class="cls_039">D</span></div>
<div style="position:absolute;left:303.39px;top:321.29px" class="cls_039"><span class="cls_039">E</span></div>
<div style="position:absolute;left:381.92px;top:321.29px" class="cls_039"><span class="cls_039">F</span></div>
<div style="position:absolute;left:459.85px;top:321.29px" class="cls_039"><span class="cls_039">G</span></div>
<div style="position:absolute;left:204.64px;top:370.37px" class="cls_039"><span class="cls_039">H    I    J   K</span></div>
<div style="position:absolute;left:362.88px;top:370.37px" class="cls_039"><span class="cls_039">L   M</span><span class="cls_040">   N   O</span></div>
<div style="position:absolute;left:589.58px;top:567.57px" class="cls_004"><span class="cls_004">Chapter 3</span></div>
<div style="position:absolute;left:643.54px;top:567.57px" class="cls_004"><span class="cls_004">53</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:26124px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="SolvingProblemsbySearch/background43.jpg" width=792 height=612></div>
<div style="position:absolute;left:233.42px;top:76.03px" class="cls_005"><span class="cls_005">Depth-first search</span></div>
<div style="position:absolute;left:40.10px;top:127.68px" class="cls_003"><span class="cls_003">Expand deepest unexpanded node</span></div>
<div style="position:absolute;left:40.10px;top:167.88px" class="cls_015"><span class="cls_015">Implementation</span><span class="cls_003">:</span></div>
<div style="position:absolute;left:97.70px;top:192.72px" class="cls_010"><span class="cls_010">fringe</span><span class="cls_003"> = LIFO queue, i.e., put successors at front</span></div>
<div style="position:absolute;left:342.46px;top:223.12px" class="cls_041"><span class="cls_041">A</span></div>
<div style="position:absolute;left:264.09px;top:272.47px" class="cls_041"><span class="cls_041">B</span></div>
<div style="position:absolute;left:421.42px;top:272.47px" class="cls_041"><span class="cls_041">C</span></div>
<div style="position:absolute;left:223.41px;top:321.83px" class="cls_041"><span class="cls_041">D</span></div>
<div style="position:absolute;left:302.97px;top:321.83px" class="cls_041"><span class="cls_041">E</span></div>
<div style="position:absolute;left:381.94px;top:321.83px" class="cls_041"><span class="cls_041">F</span></div>
<div style="position:absolute;left:460.31px;top:321.83px" class="cls_041"><span class="cls_041">G</span></div>
<div style="position:absolute;left:203.67px;top:371.18px" class="cls_041"><span class="cls_041">H    I    J   K   L</span></div>
<div style="position:absolute;left:399.89px;top:371.18px" class="cls_041"><span class="cls_041">M</span><span class="cls_042">   N   O</span></div>
<div style="position:absolute;left:589.58px;top:567.57px" class="cls_004"><span class="cls_004">Chapter 3</span></div>
<div style="position:absolute;left:643.54px;top:567.57px" class="cls_004"><span class="cls_004">54</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:26746px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="SolvingProblemsbySearch/background44.jpg" width=792 height=612></div>
<div style="position:absolute;left:150.02px;top:76.03px" class="cls_005"><span class="cls_005">Properties of depth-first search</span></div>
<div style="position:absolute;left:40.10px;top:127.68px" class="cls_018"><span class="cls_018">Complete??</span></div>
<div style="position:absolute;left:589.58px;top:567.57px" class="cls_004"><span class="cls_004">Chapter 3</span></div>
<div style="position:absolute;left:643.54px;top:567.57px" class="cls_004"><span class="cls_004">55</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:27368px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="SolvingProblemsbySearch/background45.jpg" width=792 height=612></div>
<div style="position:absolute;left:150.02px;top:76.03px" class="cls_005"><span class="cls_005">Properties of depth-first search</span></div>
<div style="position:absolute;left:40.10px;top:127.68px" class="cls_018"><span class="cls_018">Complete??</span><span class="cls_003"> No: fails in infinite-depth spaces, spaces with loops</span></div>
<div style="position:absolute;left:97.70px;top:152.52px" class="cls_003"><span class="cls_003">Modify to avoid repeated states along path</span></div>
<div style="position:absolute;left:126.50px;top:177.48px" class="cls_003"><span class="cls_003">⇒ complete in finite spaces</span></div>
<div style="position:absolute;left:40.10px;top:217.68px" class="cls_018"><span class="cls_018">Time??</span></div>
<div style="position:absolute;left:589.58px;top:567.57px" class="cls_004"><span class="cls_004">Chapter 3</span></div>
<div style="position:absolute;left:643.54px;top:567.57px" class="cls_004"><span class="cls_004">56</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:27990px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="SolvingProblemsbySearch/background46.jpg" width=792 height=612></div>
<div style="position:absolute;left:150.02px;top:76.03px" class="cls_005"><span class="cls_005">Properties of depth-first search</span></div>
<div style="position:absolute;left:40.10px;top:127.68px" class="cls_018"><span class="cls_018">Complete??</span><span class="cls_003"> No: fails in infinite-depth spaces, spaces with loops</span></div>
<div style="position:absolute;left:97.70px;top:152.52px" class="cls_003"><span class="cls_003">Modify to avoid repeated states along path</span></div>
<div style="position:absolute;left:126.50px;top:177.48px" class="cls_003"><span class="cls_003">⇒ complete in finite spaces</span></div>
<div style="position:absolute;left:40.10px;top:216.43px" class="cls_018"><span class="cls_018">Time??</span><span class="cls_014"> O(b</span><span class="cls_037"><sup>m</sup></span><span class="cls_014">)</span><span class="cls_003">: terrible if</span><span class="cls_014"> m</span><span class="cls_003"> is much larger than</span><span class="cls_014"> d</span></div>
<div style="position:absolute;left:97.70px;top:242.64px" class="cls_003"><span class="cls_003">but if solutions are dense, may be much faster than breadth-first</span></div>
<div style="position:absolute;left:40.10px;top:282.84px" class="cls_018"><span class="cls_018">Space??</span></div>
<div style="position:absolute;left:589.58px;top:567.57px" class="cls_004"><span class="cls_004">Chapter 3</span></div>
<div style="position:absolute;left:643.54px;top:567.57px" class="cls_004"><span class="cls_004">57</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:28612px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="SolvingProblemsbySearch/background47.jpg" width=792 height=612></div>
<div style="position:absolute;left:150.02px;top:76.03px" class="cls_005"><span class="cls_005">Properties of depth-first search</span></div>
<div style="position:absolute;left:40.10px;top:127.68px" class="cls_018"><span class="cls_018">Complete??</span><span class="cls_003"> No: fails in infinite-depth spaces, spaces with loops</span></div>
<div style="position:absolute;left:97.70px;top:152.52px" class="cls_003"><span class="cls_003">Modify to avoid repeated states along path</span></div>
<div style="position:absolute;left:126.50px;top:177.48px" class="cls_003"><span class="cls_003">⇒ complete in finite spaces</span></div>
<div style="position:absolute;left:40.10px;top:216.43px" class="cls_018"><span class="cls_018">Time??</span><span class="cls_014"> O(b</span><span class="cls_037"><sup>m</sup></span><span class="cls_014">)</span><span class="cls_003">: terrible if</span><span class="cls_014"> m</span><span class="cls_003"> is much larger than</span><span class="cls_014"> d</span></div>
<div style="position:absolute;left:97.70px;top:242.64px" class="cls_003"><span class="cls_003">but if solutions are dense, may be much faster than breadth-first</span></div>
<div style="position:absolute;left:40.10px;top:282.84px" class="cls_018"><span class="cls_018">Space??</span><span class="cls_014"> O(bm)</span><span class="cls_003">, i.e., linear space!</span></div>
<div style="position:absolute;left:40.10px;top:323.04px" class="cls_018"><span class="cls_018">Optimal??</span></div>
<div style="position:absolute;left:589.58px;top:567.57px" class="cls_004"><span class="cls_004">Chapter 3</span></div>
<div style="position:absolute;left:643.54px;top:567.57px" class="cls_004"><span class="cls_004">58</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:29234px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="SolvingProblemsbySearch/background48.jpg" width=792 height=612></div>
<div style="position:absolute;left:150.02px;top:76.03px" class="cls_005"><span class="cls_005">Properties of depth-first search</span></div>
<div style="position:absolute;left:40.10px;top:127.68px" class="cls_018"><span class="cls_018">Complete??</span><span class="cls_003"> No: fails in infinite-depth spaces, spaces with loops</span></div>
<div style="position:absolute;left:97.70px;top:152.52px" class="cls_003"><span class="cls_003">Modify to avoid repeated states along path</span></div>
<div style="position:absolute;left:126.50px;top:177.48px" class="cls_003"><span class="cls_003">⇒ complete in finite spaces</span></div>
<div style="position:absolute;left:40.10px;top:216.43px" class="cls_018"><span class="cls_018">Time??</span><span class="cls_014"> O(b</span><span class="cls_037"><sup>m</sup></span><span class="cls_014">)</span><span class="cls_003">: terrible if</span><span class="cls_014"> m</span><span class="cls_003"> is much larger than</span><span class="cls_014"> d</span></div>
<div style="position:absolute;left:97.70px;top:242.64px" class="cls_003"><span class="cls_003">but if solutions are dense, may be much faster than breadth-first</span></div>
<div style="position:absolute;left:40.10px;top:282.84px" class="cls_018"><span class="cls_018">Space??</span><span class="cls_014"> O(bm)</span><span class="cls_003">, i.e., linear space!</span></div>
<div style="position:absolute;left:40.10px;top:323.04px" class="cls_018"><span class="cls_018">Optimal??</span><span class="cls_003"> No</span></div>
<div style="position:absolute;left:589.58px;top:567.57px" class="cls_004"><span class="cls_004">Chapter 3</span></div>
<div style="position:absolute;left:643.54px;top:567.57px" class="cls_004"><span class="cls_004">59</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:29856px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="SolvingProblemsbySearch/background49.jpg" width=792 height=612></div>
<div style="position:absolute;left:214.58px;top:76.03px" class="cls_005"><span class="cls_005">Depth-limited search</span></div>
<div style="position:absolute;left:40.10px;top:127.68px" class="cls_003"><span class="cls_003">= depth-first search with depth limit</span><span class="cls_014"> l</span><span class="cls_003">,</span></div>
<div style="position:absolute;left:40.10px;top:152.52px" class="cls_003"><span class="cls_003">i.e., nodes at depth</span><span class="cls_014"> l</span><span class="cls_003"> have no successors</span></div>
<div style="position:absolute;left:40.10px;top:192.72px" class="cls_015"><span class="cls_015">Recursive implementation</span><span class="cls_003">:</span></div>
<div style="position:absolute;left:56.54px;top:241.40px" class="cls_006"><span class="cls_006">function</span><span class="cls_007"> Depth-Limited-Search</span><span class="cls_008">(</span><span class="cls_009"> problem, limit</span><span class="cls_008">)</span><span class="cls_006"> returns</span><span class="cls_008"> soln/fail/cutoff</span></div>
<div style="position:absolute;left:78.02px;top:263.36px" class="cls_008"><span class="cls_008">Recursive-DLS(Make-Node(Initial-State[</span><span class="cls_009">problem</span><span class="cls_008">]),</span><span class="cls_009"> problem</span><span class="cls_008">,</span><span class="cls_009"> limit</span><span class="cls_008">)</span></div>
<div style="position:absolute;left:56.54px;top:292.52px" class="cls_006"><span class="cls_006">function</span><span class="cls_007"> Recursive-DLS</span><span class="cls_008">(</span><span class="cls_009">node, problem, limit</span><span class="cls_008">)</span><span class="cls_006"> returns</span><span class="cls_008"> soln/fail/cutoff</span></div>
<div style="position:absolute;left:78.02px;top:314.36px" class="cls_009"><span class="cls_009">cutoff-occurred?</span><span class="cls_008"> ← false</span></div>
<div style="position:absolute;left:78.02px;top:336.32px" class="cls_006"><span class="cls_006">if</span><span class="cls_008"> Goal-Test(</span><span class="cls_009">problem</span><span class="cls_008">, State[</span><span class="cls_009">node</span><span class="cls_008">])</span><span class="cls_006"> then return</span><span class="cls_009"> node</span></div>
<div style="position:absolute;left:78.02px;top:358.16px" class="cls_006"><span class="cls_006">else if</span><span class="cls_008"> Depth[</span><span class="cls_009">node</span><span class="cls_008">] =</span><span class="cls_009"> limit</span><span class="cls_006"> then return</span><span class="cls_009"> cutoff</span></div>
<div style="position:absolute;left:78.02px;top:380.12px" class="cls_006"><span class="cls_006">else for each</span><span class="cls_009"> successor</span><span class="cls_006"> in</span><span class="cls_008"> Expand(</span><span class="cls_009">node</span><span class="cls_008">,</span><span class="cls_009"> problem</span><span class="cls_008">)</span><span class="cls_006"> do</span></div>
<div style="position:absolute;left:110.42px;top:402.08px" class="cls_009"><span class="cls_009">result</span><span class="cls_008"> ← Recursive-DLS(</span><span class="cls_009">successor</span><span class="cls_008">,</span><span class="cls_009"> problem</span><span class="cls_008">,</span><span class="cls_009"> limit</span><span class="cls_008">)</span></div>
<div style="position:absolute;left:110.42px;top:423.92px" class="cls_006"><span class="cls_006">if</span><span class="cls_009"> result</span><span class="cls_008"> =</span><span class="cls_009"> cutoff</span><span class="cls_006"> then</span><span class="cls_009"> cutoff-occurred?</span><span class="cls_008"> ← true</span></div>
<div style="position:absolute;left:110.42px;top:445.88px" class="cls_006"><span class="cls_006">else if</span><span class="cls_009"> result</span><span class="cls_008"> =</span><span class="cls_009"> failure</span><span class="cls_006"> then return</span><span class="cls_009"> result</span></div>
<div style="position:absolute;left:78.02px;top:467.84px" class="cls_006"><span class="cls_006">if</span><span class="cls_009"> cutoff-occurred?</span><span class="cls_006"> then return</span><span class="cls_009"> cutoff</span><span class="cls_006"> else return</span><span class="cls_009"> failure</span></div>
<div style="position:absolute;left:589.58px;top:567.57px" class="cls_004"><span class="cls_004">Chapter 3</span></div>
<div style="position:absolute;left:643.54px;top:567.57px" class="cls_004"><span class="cls_004">60</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:30478px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="SolvingProblemsbySearch/background50.jpg" width=792 height=612></div>
<div style="position:absolute;left:180.86px;top:76.03px" class="cls_005"><span class="cls_005">Iterative deepening search</span></div>
<div style="position:absolute;left:56.54px;top:141.68px" class="cls_006"><span class="cls_006">function</span><span class="cls_007"> Iterative-Deepening-Search</span><span class="cls_008">(</span><span class="cls_009"> problem</span><span class="cls_008">)</span><span class="cls_006"> returns</span><span class="cls_008"> a solution</span></div>
<div style="position:absolute;left:88.82px;top:163.52px" class="cls_006"><span class="cls_006">inputs</span><span class="cls_008">:</span><span class="cls_009"> problem</span><span class="cls_008">, a problem</span></div>
<div style="position:absolute;left:88.82px;top:192.68px" class="cls_006"><span class="cls_006">for</span><span class="cls_009"> depth</span><span class="cls_008"> ← 0</span><span class="cls_006"> to</span><span class="cls_008"> ∞</span><span class="cls_006"> do</span></div>
<div style="position:absolute;left:110.42px;top:214.64px" class="cls_009"><span class="cls_009">result</span><span class="cls_008"> ← Depth-Limited-Search(</span><span class="cls_009"> problem, depth</span><span class="cls_008">)</span></div>
<div style="position:absolute;left:110.42px;top:236.48px" class="cls_006"><span class="cls_006">if</span><span class="cls_009"> result</span><span class="cls_008"> = cutoff</span><span class="cls_006"> then return</span><span class="cls_009"> result</span></div>
<div style="position:absolute;left:88.82px;top:258.44px" class="cls_006"><span class="cls_006">end</span></div>
<div style="position:absolute;left:589.58px;top:567.57px" class="cls_004"><span class="cls_004">Chapter 3</span></div>
<div style="position:absolute;left:643.54px;top:567.57px" class="cls_004"><span class="cls_004">61</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:31100px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="SolvingProblemsbySearch/background51.jpg" width=792 height=612></div>
<div style="position:absolute;left:154.82px;top:76.03px" class="cls_005"><span class="cls_005">Iterative deepening search</span><span class="cls_014"> l = 0</span></div>
<div style="position:absolute;left:12.51px;top:145.02px" class="cls_045"><span class="cls_045">Limit = 0</span></div>
<div style="position:absolute;left:112.24px;top:148.88px" class="cls_046"><span class="cls_046">A</span></div>
<div style="position:absolute;left:273.30px;top:148.88px" class="cls_046"><span class="cls_046">A</span></div>
<div style="position:absolute;left:589.58px;top:567.57px" class="cls_004"><span class="cls_004">Chapter 3</span></div>
<div style="position:absolute;left:643.54px;top:567.57px" class="cls_004"><span class="cls_004">62</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:31722px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="SolvingProblemsbySearch/background52.jpg" width=792 height=612></div>
<div style="position:absolute;left:154.82px;top:76.03px" class="cls_005"><span class="cls_005">Iterative deepening search</span><span class="cls_014"> l = 1</span></div>
<div style="position:absolute;left:12.51px;top:147.92px" class="cls_045"><span class="cls_045">Limit = 1</span></div>
<div style="position:absolute;left:112.24px;top:151.77px" class="cls_046"><span class="cls_046">A</span></div>
<div style="position:absolute;left:273.30px;top:151.77px" class="cls_046"><span class="cls_046">A</span></div>
<div style="position:absolute;left:434.36px;top:151.77px" class="cls_046"><span class="cls_046">A</span></div>
<div style="position:absolute;left:596.05px;top:151.77px" class="cls_046"><span class="cls_046">A</span></div>
<div style="position:absolute;left:73.88px;top:175.93px" class="cls_047"><span class="cls_047">B</span></div>
<div style="position:absolute;left:150.89px;top:175.93px" class="cls_047"><span class="cls_047">C</span></div>
<div style="position:absolute;left:234.94px;top:175.93px" class="cls_046"><span class="cls_046">B</span></div>
<div style="position:absolute;left:311.95px;top:175.93px" class="cls_046"><span class="cls_046">C</span></div>
<div style="position:absolute;left:396.00px;top:175.93px" class="cls_046"><span class="cls_046">B</span></div>
<div style="position:absolute;left:473.01px;top:175.93px" class="cls_046"><span class="cls_046">C</span></div>
<div style="position:absolute;left:557.69px;top:175.93px" class="cls_046"><span class="cls_046">B</span></div>
<div style="position:absolute;left:634.71px;top:175.93px" class="cls_046"><span class="cls_046">C</span></div>
<div style="position:absolute;left:589.58px;top:567.57px" class="cls_004"><span class="cls_004">Chapter 3</span></div>
<div style="position:absolute;left:643.54px;top:567.57px" class="cls_004"><span class="cls_004">63</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:32344px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="SolvingProblemsbySearch/background53.jpg" width=792 height=612></div>
<div style="position:absolute;left:154.82px;top:76.03px" class="cls_005"><span class="cls_005">Iterative deepening search</span><span class="cls_014"> l = 2</span></div>
<div style="position:absolute;left:12.51px;top:145.88px" class="cls_045"><span class="cls_045">Limit = 2</span></div>
<div style="position:absolute;left:112.24px;top:149.74px" class="cls_046"><span class="cls_046">A</span></div>
<div style="position:absolute;left:273.30px;top:149.74px" class="cls_046"><span class="cls_046">A</span></div>
<div style="position:absolute;left:434.36px;top:149.74px" class="cls_046"><span class="cls_046">A</span></div>
<div style="position:absolute;left:596.05px;top:149.74px" class="cls_046"><span class="cls_046">A</span></div>
<div style="position:absolute;left:73.88px;top:173.89px" class="cls_047"><span class="cls_047">B</span></div>
<div style="position:absolute;left:150.89px;top:173.89px" class="cls_047"><span class="cls_047">C</span></div>
<div style="position:absolute;left:234.94px;top:173.89px" class="cls_046"><span class="cls_046">B</span></div>
<div style="position:absolute;left:311.95px;top:173.89px" class="cls_046"><span class="cls_046">C</span></div>
<div style="position:absolute;left:396.00px;top:173.89px" class="cls_046"><span class="cls_046">B</span></div>
<div style="position:absolute;left:473.01px;top:173.89px" class="cls_046"><span class="cls_046">C</span></div>
<div style="position:absolute;left:557.69px;top:173.89px" class="cls_046"><span class="cls_046">B</span></div>
<div style="position:absolute;left:634.71px;top:173.89px" class="cls_046"><span class="cls_046">C</span></div>
<div style="position:absolute;left:53.97px;top:198.05px" class="cls_047"><span class="cls_047">D</span></div>
<div style="position:absolute;left:92.91px;top:198.05px" class="cls_047"><span class="cls_047">E</span></div>
<div style="position:absolute;left:131.57px;top:198.05px" class="cls_047"><span class="cls_047">F</span></div>
<div style="position:absolute;left:169.93px;top:198.05px" class="cls_047"><span class="cls_047">G</span></div>
<div style="position:absolute;left:215.02px;top:198.05px" class="cls_047"><span class="cls_047">D</span></div>
<div style="position:absolute;left:253.97px;top:198.05px" class="cls_047"><span class="cls_047">E</span></div>
<div style="position:absolute;left:292.63px;top:198.05px" class="cls_047"><span class="cls_047">F</span></div>
<div style="position:absolute;left:330.99px;top:198.05px" class="cls_047"><span class="cls_047">G</span></div>
<div style="position:absolute;left:376.08px;top:198.05px" class="cls_046"><span class="cls_046">D</span></div>
<div style="position:absolute;left:415.03px;top:198.05px" class="cls_046"><span class="cls_046">E</span></div>
<div style="position:absolute;left:453.68px;top:198.05px" class="cls_047"><span class="cls_047">F</span></div>
<div style="position:absolute;left:492.05px;top:198.05px" class="cls_047"><span class="cls_047">G</span></div>
<div style="position:absolute;left:537.78px;top:198.05px" class="cls_046"><span class="cls_046">D</span></div>
<div style="position:absolute;left:576.72px;top:198.05px" class="cls_046"><span class="cls_046">E</span></div>
<div style="position:absolute;left:615.38px;top:198.05px" class="cls_047"><span class="cls_047">F</span></div>
<div style="position:absolute;left:653.74px;top:198.05px" class="cls_047"><span class="cls_047">G</span></div>
<div style="position:absolute;left:112.24px;top:236.09px" class="cls_046"><span class="cls_046">A</span></div>
<div style="position:absolute;left:273.30px;top:236.09px" class="cls_046"><span class="cls_046">A</span></div>
<div style="position:absolute;left:434.36px;top:236.09px" class="cls_046"><span class="cls_046">A</span></div>
<div style="position:absolute;left:598.40px;top:236.09px" class="cls_046"><span class="cls_046">A</span></div>
<div style="position:absolute;left:73.88px;top:260.25px" class="cls_046"><span class="cls_046">B</span></div>
<div style="position:absolute;left:150.89px;top:260.25px" class="cls_046"><span class="cls_046">C</span></div>
<div style="position:absolute;left:234.94px;top:260.25px" class="cls_046"><span class="cls_046">B</span></div>
<div style="position:absolute;left:311.95px;top:260.25px" class="cls_046"><span class="cls_046">C</span></div>
<div style="position:absolute;left:396.00px;top:260.25px" class="cls_046"><span class="cls_046">B</span></div>
<div style="position:absolute;left:473.01px;top:260.25px" class="cls_046"><span class="cls_046">C</span></div>
<div style="position:absolute;left:560.03px;top:260.25px" class="cls_046"><span class="cls_046">B</span></div>
<div style="position:absolute;left:637.05px;top:260.25px" class="cls_046"><span class="cls_046">C</span></div>
<div style="position:absolute;left:53.96px;top:284.41px" class="cls_046"><span class="cls_046">D</span></div>
<div style="position:absolute;left:92.91px;top:284.41px" class="cls_046"><span class="cls_046">E</span></div>
<div style="position:absolute;left:131.57px;top:284.41px" class="cls_047"><span class="cls_047">F</span></div>
<div style="position:absolute;left:169.93px;top:284.41px" class="cls_047"><span class="cls_047">G</span></div>
<div style="position:absolute;left:215.02px;top:284.41px" class="cls_046"><span class="cls_046">D</span></div>
<div style="position:absolute;left:253.97px;top:284.41px" class="cls_046"><span class="cls_046">E</span></div>
<div style="position:absolute;left:292.62px;top:284.41px" class="cls_046"><span class="cls_046">F</span></div>
<div style="position:absolute;left:330.99px;top:284.41px" class="cls_046"><span class="cls_046">G</span></div>
<div style="position:absolute;left:376.08px;top:284.41px" class="cls_046"><span class="cls_046">D</span></div>
<div style="position:absolute;left:415.03px;top:284.41px" class="cls_046"><span class="cls_046">E</span></div>
<div style="position:absolute;left:453.68px;top:284.41px" class="cls_046"><span class="cls_046">F</span></div>
<div style="position:absolute;left:492.05px;top:284.41px" class="cls_046"><span class="cls_046">G</span></div>
<div style="position:absolute;left:540.12px;top:284.41px" class="cls_046"><span class="cls_046">D</span></div>
<div style="position:absolute;left:579.07px;top:284.41px" class="cls_046"><span class="cls_046">E</span></div>
<div style="position:absolute;left:617.72px;top:284.41px" class="cls_046"><span class="cls_046">F</span></div>
<div style="position:absolute;left:656.08px;top:284.41px" class="cls_046"><span class="cls_046">G</span></div>
<div style="position:absolute;left:589.58px;top:567.57px" class="cls_004"><span class="cls_004">Chapter 3</span></div>
<div style="position:absolute;left:643.54px;top:567.57px" class="cls_004"><span class="cls_004">64</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:32966px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="SolvingProblemsbySearch/background54.jpg" width=792 height=612></div>
<div style="position:absolute;left:154.82px;top:76.03px" class="cls_005"><span class="cls_005">Iterative deepening search</span><span class="cls_014"> l = 3</span></div>
<div style="position:absolute;left:12.51px;top:145.36px" class="cls_045"><span class="cls_045">Limit = 3</span></div>
<div style="position:absolute;left:112.24px;top:149.22px" class="cls_046"><span class="cls_046">A</span></div>
<div style="position:absolute;left:273.30px;top:149.22px" class="cls_046"><span class="cls_046">A</span></div>
<div style="position:absolute;left:434.36px;top:149.22px" class="cls_046"><span class="cls_046">A</span></div>
<div style="position:absolute;left:595.42px;top:149.22px" class="cls_046"><span class="cls_046">A</span></div>
<div style="position:absolute;left:73.88px;top:173.38px" class="cls_047"><span class="cls_047">B</span></div>
<div style="position:absolute;left:150.89px;top:173.38px" class="cls_047"><span class="cls_047">C</span></div>
<div style="position:absolute;left:234.94px;top:173.38px" class="cls_046"><span class="cls_046">B</span></div>
<div style="position:absolute;left:311.95px;top:173.38px" class="cls_046"><span class="cls_046">C</span></div>
<div style="position:absolute;left:396.00px;top:173.38px" class="cls_046"><span class="cls_046">B</span></div>
<div style="position:absolute;left:473.01px;top:173.38px" class="cls_046"><span class="cls_046">C</span></div>
<div style="position:absolute;left:557.06px;top:173.38px" class="cls_046"><span class="cls_046">B</span></div>
<div style="position:absolute;left:634.07px;top:173.38px" class="cls_046"><span class="cls_046">C</span></div>
<div style="position:absolute;left:53.97px;top:197.54px" class="cls_047"><span class="cls_047">D</span></div>
<div style="position:absolute;left:92.91px;top:197.54px" class="cls_047"><span class="cls_047">E</span></div>
<div style="position:absolute;left:131.57px;top:197.54px" class="cls_047"><span class="cls_047">F</span></div>
<div style="position:absolute;left:169.93px;top:197.54px" class="cls_047"><span class="cls_047">G</span></div>
<div style="position:absolute;left:215.02px;top:197.54px" class="cls_047"><span class="cls_047">D</span></div>
<div style="position:absolute;left:253.97px;top:197.54px" class="cls_047"><span class="cls_047">E</span></div>
<div style="position:absolute;left:292.63px;top:197.54px" class="cls_047"><span class="cls_047">F</span></div>
<div style="position:absolute;left:330.99px;top:197.54px" class="cls_047"><span class="cls_047">G</span></div>
<div style="position:absolute;left:376.08px;top:197.54px" class="cls_046"><span class="cls_046">D</span></div>
<div style="position:absolute;left:415.03px;top:197.54px" class="cls_046"><span class="cls_046">E</span></div>
<div style="position:absolute;left:453.68px;top:197.54px" class="cls_047"><span class="cls_047">F</span></div>
<div style="position:absolute;left:492.05px;top:197.54px" class="cls_047"><span class="cls_047">G</span></div>
<div style="position:absolute;left:537.14px;top:197.54px" class="cls_046"><span class="cls_046">D</span></div>
<div style="position:absolute;left:576.09px;top:197.54px" class="cls_046"><span class="cls_046">E</span></div>
<div style="position:absolute;left:614.74px;top:197.54px" class="cls_047"><span class="cls_047">F</span></div>
<div style="position:absolute;left:653.11px;top:197.54px" class="cls_047"><span class="cls_047">G</span></div>
<div style="position:absolute;left:44.30px;top:221.70px" class="cls_047"><span class="cls_047">H    I    J   K   L   M   N   O</span></div>
<div style="position:absolute;left:205.36px;top:221.70px" class="cls_047"><span class="cls_047">H    I    J   K   L   M   N   O</span></div>
<div style="position:absolute;left:366.42px;top:221.70px" class="cls_047"><span class="cls_047">H    I    J   K   L   M   N   O</span></div>
<div style="position:absolute;left:527.48px;top:221.70px" class="cls_046"><span class="cls_046">H    I</span><span class="cls_047">    J   K   L   M   N   O</span></div>
<div style="position:absolute;left:112.24px;top:252.30px" class="cls_046"><span class="cls_046">A</span></div>
<div style="position:absolute;left:273.30px;top:252.30px" class="cls_046"><span class="cls_046">A</span></div>
<div style="position:absolute;left:434.36px;top:252.30px" class="cls_046"><span class="cls_046">A</span></div>
<div style="position:absolute;left:595.42px;top:252.30px" class="cls_046"><span class="cls_046">A</span></div>
<div style="position:absolute;left:73.88px;top:276.46px" class="cls_046"><span class="cls_046">B</span></div>
<div style="position:absolute;left:150.89px;top:276.46px" class="cls_046"><span class="cls_046">C</span></div>
<div style="position:absolute;left:234.94px;top:276.46px" class="cls_046"><span class="cls_046">B</span></div>
<div style="position:absolute;left:311.95px;top:276.46px" class="cls_046"><span class="cls_046">C</span></div>
<div style="position:absolute;left:396.00px;top:276.46px" class="cls_046"><span class="cls_046">B</span></div>
<div style="position:absolute;left:473.01px;top:276.46px" class="cls_046"><span class="cls_046">C</span></div>
<div style="position:absolute;left:557.05px;top:276.46px" class="cls_046"><span class="cls_046">B</span></div>
<div style="position:absolute;left:634.07px;top:276.46px" class="cls_046"><span class="cls_046">C</span></div>
<div style="position:absolute;left:53.97px;top:300.62px" class="cls_046"><span class="cls_046">D</span></div>
<div style="position:absolute;left:92.91px;top:300.62px" class="cls_046"><span class="cls_046">E</span></div>
<div style="position:absolute;left:131.57px;top:300.62px" class="cls_047"><span class="cls_047">F</span></div>
<div style="position:absolute;left:169.93px;top:300.62px" class="cls_047"><span class="cls_047">G</span></div>
<div style="position:absolute;left:215.02px;top:300.62px" class="cls_046"><span class="cls_046">D</span></div>
<div style="position:absolute;left:253.97px;top:300.62px" class="cls_046"><span class="cls_046">E</span></div>
<div style="position:absolute;left:292.63px;top:300.62px" class="cls_047"><span class="cls_047">F</span></div>
<div style="position:absolute;left:330.99px;top:300.62px" class="cls_047"><span class="cls_047">G</span></div>
<div style="position:absolute;left:376.08px;top:300.62px" class="cls_046"><span class="cls_046">D</span></div>
<div style="position:absolute;left:415.03px;top:300.62px" class="cls_046"><span class="cls_046">E</span></div>
<div style="position:absolute;left:453.68px;top:300.62px" class="cls_047"><span class="cls_047">F</span></div>
<div style="position:absolute;left:492.05px;top:300.62px" class="cls_047"><span class="cls_047">G</span></div>
<div style="position:absolute;left:537.14px;top:300.62px" class="cls_046"><span class="cls_046">D</span></div>
<div style="position:absolute;left:576.09px;top:300.62px" class="cls_046"><span class="cls_046">E</span></div>
<div style="position:absolute;left:614.74px;top:300.62px" class="cls_047"><span class="cls_047">F</span></div>
<div style="position:absolute;left:653.11px;top:300.62px" class="cls_047"><span class="cls_047">G</span></div>
<div style="position:absolute;left:44.30px;top:324.77px" class="cls_046"><span class="cls_046">H    I</span><span class="cls_047">    J   K   L   M   N   O</span></div>
<div style="position:absolute;left:205.36px;top:324.77px" class="cls_046"><span class="cls_046">H    I</span><span class="cls_047">    J   K   L   M   N   O</span></div>
<div style="position:absolute;left:366.42px;top:324.77px" class="cls_046"><span class="cls_046">H    I    J   K</span><span class="cls_047">   L   M   N   O</span></div>
<div style="position:absolute;left:527.48px;top:324.77px" class="cls_046"><span class="cls_046">H    I    J   K</span><span class="cls_047">   L   M   N   O</span></div>
<div style="position:absolute;left:112.24px;top:355.38px" class="cls_046"><span class="cls_046">A</span></div>
<div style="position:absolute;left:273.30px;top:355.38px" class="cls_046"><span class="cls_046">A</span></div>
<div style="position:absolute;left:434.36px;top:355.38px" class="cls_046"><span class="cls_046">A</span></div>
<div style="position:absolute;left:595.42px;top:355.38px" class="cls_046"><span class="cls_046">A</span></div>
<div style="position:absolute;left:73.88px;top:379.53px" class="cls_046"><span class="cls_046">B</span></div>
<div style="position:absolute;left:150.89px;top:379.53px" class="cls_046"><span class="cls_046">C</span></div>
<div style="position:absolute;left:234.94px;top:379.53px" class="cls_046"><span class="cls_046">B</span></div>
<div style="position:absolute;left:311.95px;top:379.53px" class="cls_046"><span class="cls_046">C</span></div>
<div style="position:absolute;left:395.99px;top:379.53px" class="cls_046"><span class="cls_046">B</span></div>
<div style="position:absolute;left:473.01px;top:379.53px" class="cls_046"><span class="cls_046">C</span></div>
<div style="position:absolute;left:557.05px;top:379.53px" class="cls_046"><span class="cls_046">B</span></div>
<div style="position:absolute;left:634.07px;top:379.53px" class="cls_046"><span class="cls_046">C</span></div>
<div style="position:absolute;left:53.96px;top:403.69px" class="cls_046"><span class="cls_046">D</span></div>
<div style="position:absolute;left:92.91px;top:403.69px" class="cls_046"><span class="cls_046">E</span></div>
<div style="position:absolute;left:131.57px;top:403.69px" class="cls_047"><span class="cls_047">F</span></div>
<div style="position:absolute;left:169.93px;top:403.69px" class="cls_047"><span class="cls_047">G</span></div>
<div style="position:absolute;left:215.02px;top:403.69px" class="cls_046"><span class="cls_046">D</span></div>
<div style="position:absolute;left:253.97px;top:403.69px" class="cls_046"><span class="cls_046">E</span></div>
<div style="position:absolute;left:292.62px;top:403.69px" class="cls_046"><span class="cls_046">F</span></div>
<div style="position:absolute;left:330.99px;top:403.69px" class="cls_046"><span class="cls_046">G</span></div>
<div style="position:absolute;left:376.08px;top:403.69px" class="cls_046"><span class="cls_046">D</span></div>
<div style="position:absolute;left:415.03px;top:403.69px" class="cls_046"><span class="cls_046">E</span></div>
<div style="position:absolute;left:453.68px;top:403.69px" class="cls_046"><span class="cls_046">F</span></div>
<div style="position:absolute;left:492.05px;top:403.69px" class="cls_046"><span class="cls_046">G</span></div>
<div style="position:absolute;left:537.14px;top:403.69px" class="cls_046"><span class="cls_046">D</span></div>
<div style="position:absolute;left:576.09px;top:403.69px" class="cls_046"><span class="cls_046">E</span></div>
<div style="position:absolute;left:614.74px;top:403.69px" class="cls_046"><span class="cls_046">F</span></div>
<div style="position:absolute;left:653.10px;top:403.69px" class="cls_046"><span class="cls_046">G</span></div>
<div style="position:absolute;left:44.30px;top:427.85px" class="cls_046"><span class="cls_046">H    I    J   K</span><span class="cls_047">   L   M   N   O</span></div>
<div style="position:absolute;left:205.36px;top:427.85px" class="cls_046"><span class="cls_046">H    I    J   K</span><span class="cls_047">   L   M   N   O</span></div>
<div style="position:absolute;left:366.42px;top:427.85px" class="cls_046"><span class="cls_046">H    I    J   K   L   M</span><span class="cls_047">   N   O</span></div>
<div style="position:absolute;left:527.48px;top:427.85px" class="cls_046"><span class="cls_046">H    I    J   K   L   M</span><span class="cls_047">   N   O</span></div>
<div style="position:absolute;left:589.58px;top:567.57px" class="cls_004"><span class="cls_004">Chapter 3</span></div>
<div style="position:absolute;left:643.54px;top:567.57px" class="cls_004"><span class="cls_004">65</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:33588px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="SolvingProblemsbySearch/background55.jpg" width=792 height=612></div>
<div style="position:absolute;left:95.78px;top:76.03px" class="cls_005"><span class="cls_005">Properties of iterative deepening search</span></div>
<div style="position:absolute;left:40.10px;top:127.68px" class="cls_018"><span class="cls_018">Complete??</span></div>
<div style="position:absolute;left:589.58px;top:567.57px" class="cls_004"><span class="cls_004">Chapter 3</span></div>
<div style="position:absolute;left:643.54px;top:567.57px" class="cls_004"><span class="cls_004">66</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:34210px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="SolvingProblemsbySearch/background56.jpg" width=792 height=612></div>
<div style="position:absolute;left:95.78px;top:76.03px" class="cls_005"><span class="cls_005">Properties of iterative deepening search</span></div>
<div style="position:absolute;left:40.10px;top:127.68px" class="cls_018"><span class="cls_018">Complete??</span><span class="cls_003"> Yes</span></div>
<div style="position:absolute;left:40.10px;top:167.88px" class="cls_018"><span class="cls_018">Time??</span></div>
<div style="position:absolute;left:589.58px;top:567.57px" class="cls_004"><span class="cls_004">Chapter 3</span></div>
<div style="position:absolute;left:643.54px;top:567.57px" class="cls_004"><span class="cls_004">67</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:34832px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="SolvingProblemsbySearch/background57.jpg" width=792 height=612></div>
<div style="position:absolute;left:95.78px;top:76.03px" class="cls_005"><span class="cls_005">Properties of iterative deepening search</span></div>
<div style="position:absolute;left:40.10px;top:127.68px" class="cls_018"><span class="cls_018">Complete??</span><span class="cls_003"> Yes</span></div>
<div style="position:absolute;left:40.10px;top:166.63px" class="cls_018"><span class="cls_018">Time??</span><span class="cls_014"> (d + 1)b</span><span class="cls_037"><sup>0</sup></span><span class="cls_014"> + db</span><span class="cls_037"><sup>1</sup></span><span class="cls_014"> + (d − 1)b</span><span class="cls_037"><sup>2</sup></span><span class="cls_014"> + . . . + b</span><span class="cls_037"><sup>d</sup></span><span class="cls_014"> = O(b</span><span class="cls_037"><sup>d</sup></span><span class="cls_014">)</span></div>
<div style="position:absolute;left:40.10px;top:208.08px" class="cls_018"><span class="cls_018">Space??</span></div>
<div style="position:absolute;left:589.58px;top:567.57px" class="cls_004"><span class="cls_004">Chapter 3</span></div>
<div style="position:absolute;left:643.54px;top:567.57px" class="cls_004"><span class="cls_004">68</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:35454px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="SolvingProblemsbySearch/background58.jpg" width=792 height=612></div>
<div style="position:absolute;left:95.78px;top:76.03px" class="cls_005"><span class="cls_005">Properties of iterative deepening search</span></div>
<div style="position:absolute;left:40.10px;top:127.68px" class="cls_018"><span class="cls_018">Complete??</span><span class="cls_003"> Yes</span></div>
<div style="position:absolute;left:40.10px;top:166.63px" class="cls_018"><span class="cls_018">Time??</span><span class="cls_014"> (d + 1)b</span><span class="cls_037"><sup>0</sup></span><span class="cls_014"> + db</span><span class="cls_037"><sup>1</sup></span><span class="cls_014"> + (d − 1)b</span><span class="cls_037"><sup>2</sup></span><span class="cls_014"> + . . . + b</span><span class="cls_037"><sup>d</sup></span><span class="cls_014"> = O(b</span><span class="cls_037"><sup>d</sup></span><span class="cls_014">)</span></div>
<div style="position:absolute;left:40.10px;top:208.08px" class="cls_018"><span class="cls_018">Space??</span><span class="cls_014"> O(bd)</span></div>
<div style="position:absolute;left:40.10px;top:248.28px" class="cls_018"><span class="cls_018">Optimal??</span></div>
<div style="position:absolute;left:589.58px;top:567.57px" class="cls_004"><span class="cls_004">Chapter 3</span></div>
<div style="position:absolute;left:643.54px;top:567.57px" class="cls_004"><span class="cls_004">69</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:36076px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="SolvingProblemsbySearch/background59.jpg" width=792 height=612></div>
<div style="position:absolute;left:95.78px;top:76.03px" class="cls_005"><span class="cls_005">Properties of iterative deepening search</span></div>
<div style="position:absolute;left:40.10px;top:127.68px" class="cls_018"><span class="cls_018">Complete??</span><span class="cls_003"> Yes</span></div>
<div style="position:absolute;left:40.10px;top:166.63px" class="cls_018"><span class="cls_018">Time??</span><span class="cls_014"> (d + 1)b</span><span class="cls_037"><sup>0</sup></span><span class="cls_014"> + db</span><span class="cls_037"><sup>1</sup></span><span class="cls_014"> + (d − 1)b</span><span class="cls_037"><sup>2</sup></span><span class="cls_014"> + . . . + b</span><span class="cls_037"><sup>d</sup></span><span class="cls_014"> = O(b</span><span class="cls_037"><sup>d</sup></span><span class="cls_014">)</span></div>
<div style="position:absolute;left:40.10px;top:208.08px" class="cls_018"><span class="cls_018">Space??</span><span class="cls_014"> O(bd)</span></div>
<div style="position:absolute;left:40.10px;top:248.28px" class="cls_018"><span class="cls_018">Optimal??</span><span class="cls_003"> Yes, if step cost = 1</span></div>
<div style="position:absolute;left:97.70px;top:273.12px" class="cls_003"><span class="cls_003">Can be modified to explore uniform-cost tree</span></div>
<div style="position:absolute;left:40.10px;top:313.32px" class="cls_003"><span class="cls_003">Numerical comparison for</span><span class="cls_014"> b = 10</span><span class="cls_003"> and</span><span class="cls_014"> d = 5</span><span class="cls_003">, solution at far right leaf:</span></div>
<div style="position:absolute;left:69.62px;top:353.64px" class="cls_014"><span class="cls_014">N (IDS) = 50 + 400 + 3, 000 + 20, 000 + 100, 000 = 123, 450</span></div>
<div style="position:absolute;left:65.06px;top:381.48px" class="cls_014"><span class="cls_014">N (BFS) = 10 + 100 + 1, 000 + 10, 000 + 100, 000 + 999, 990 = 1, 111, 100</span></div>
<div style="position:absolute;left:40.10px;top:461.88px" class="cls_003"><span class="cls_003">IDS does better because other nodes at depth</span><span class="cls_014"> d</span><span class="cls_003"> are not expanded</span></div>
<div style="position:absolute;left:40.10px;top:502.08px" class="cls_003"><span class="cls_003">BFS can be modified to apply goal test when a node is</span><span class="cls_015"> generated</span></div>
<div style="position:absolute;left:589.58px;top:567.57px" class="cls_004"><span class="cls_004">Chapter 3</span></div>
<div style="position:absolute;left:643.54px;top:567.57px" class="cls_004"><span class="cls_004">70</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:36698px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="SolvingProblemsbySearch/background60.jpg" width=792 height=612></div>
<div style="position:absolute;left:198.62px;top:76.03px" class="cls_005"><span class="cls_005">Summary of algorithms</span></div>
<div style="position:absolute;left:50.06px;top:131.52px" class="cls_003"><span class="cls_003">Criterion</span></div>
<div style="position:absolute;left:160.02px;top:131.52px" class="cls_003"><span class="cls_003">Breadth-   Uniform-   Depth-</span></div>
<div style="position:absolute;left:454.31px;top:131.52px" class="cls_003"><span class="cls_003">Depth-</span></div>
<div style="position:absolute;left:566.59px;top:131.52px" class="cls_003"><span class="cls_003">Iterative</span></div>
<div style="position:absolute;left:177.26px;top:156.36px" class="cls_003"><span class="cls_003">First</span></div>
<div style="position:absolute;left:273.09px;top:156.36px" class="cls_003"><span class="cls_003">Cost</span></div>
<div style="position:absolute;left:361.26px;top:156.36px" class="cls_003"><span class="cls_003">First</span></div>
<div style="position:absolute;left:452.17px;top:156.36px" class="cls_003"><span class="cls_003">Limited</span></div>
<div style="position:absolute;left:557.47px;top:156.36px" class="cls_003"><span class="cls_003">Deepening</span></div>
<div style="position:absolute;left:50.06px;top:192.36px" class="cls_003"><span class="cls_003">Complete?</span></div>
<div style="position:absolute;left:178.03px;top:191.23px" class="cls_003"><span class="cls_003">Yes</span><span class="cls_048"><sup>∗</sup></span></div>
<div style="position:absolute;left:273.86px;top:191.23px" class="cls_003"><span class="cls_003">Yes</span><span class="cls_048"><sup>∗</sup></span></div>
<div style="position:absolute;left:368.06px;top:192.36px" class="cls_003"><span class="cls_003">No    Yes, if l ≥ d</span></div>
<div style="position:absolute;left:586.27px;top:192.36px" class="cls_003"><span class="cls_003">Yes</span></div>
<div style="position:absolute;left:50.06px;top:218.16px" class="cls_003"><span class="cls_003">Time</span></div>
<div style="position:absolute;left:178.42px;top:216.91px" class="cls_003"><span class="cls_003">b</span><span class="cls_048"><sup>d+1</sup></span></div>
<div style="position:absolute;left:265.34px;top:214.14px" class="cls_003"><span class="cls_003">b</span><span class="cls_048">⌈C</span><span class="cls_049">∗</span><span class="cls_048">/ϵ⌉</span></div>
<div style="position:absolute;left:369.02px;top:216.91px" class="cls_003"><span class="cls_003">b</span><span class="cls_048"><sup>m</sup></span></div>
<div style="position:absolute;left:475.94px;top:216.91px" class="cls_003"><span class="cls_003">b</span><span class="cls_048"><sup>l</sup></span></div>
<div style="position:absolute;left:591.86px;top:216.91px" class="cls_003"><span class="cls_003">b</span><span class="cls_048"><sup>d</sup></span></div>
<div style="position:absolute;left:50.06px;top:243.84px" class="cls_003"><span class="cls_003">Space</span></div>
<div style="position:absolute;left:178.42px;top:242.71px" class="cls_003"><span class="cls_003">b</span><span class="cls_048"><sup>d+1</sup></span></div>
<div style="position:absolute;left:265.34px;top:239.82px" class="cls_003"><span class="cls_003">b</span><span class="cls_048">⌈C</span><span class="cls_049">∗</span><span class="cls_048">/ϵ⌉</span></div>
<div style="position:absolute;left:366.62px;top:243.84px" class="cls_003"><span class="cls_003">bm</span></div>
<div style="position:absolute;left:475.08px;top:243.84px" class="cls_003"><span class="cls_003">bl</span></div>
<div style="position:absolute;left:590.39px;top:243.84px" class="cls_003"><span class="cls_003">bd</span></div>
<div style="position:absolute;left:50.06px;top:268.80px" class="cls_003"><span class="cls_003">Optimal?</span></div>
<div style="position:absolute;left:178.01px;top:267.55px" class="cls_003"><span class="cls_003">Yes</span><span class="cls_048"><sup>∗</sup></span></div>
<div style="position:absolute;left:277.70px;top:268.80px" class="cls_003"><span class="cls_003">Yes</span></div>
<div style="position:absolute;left:368.05px;top:268.80px" class="cls_003"><span class="cls_003">No</span></div>
<div style="position:absolute;left:471.00px;top:268.80px" class="cls_003"><span class="cls_003">No</span></div>
<div style="position:absolute;left:582.47px;top:267.55px" class="cls_003"><span class="cls_003">Yes</span><span class="cls_048"><sup>∗</sup></span></div>
<div style="position:absolute;left:589.58px;top:567.57px" class="cls_004"><span class="cls_004">Chapter 3</span></div>
<div style="position:absolute;left:643.54px;top:567.57px" class="cls_004"><span class="cls_004">71</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:37320px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="SolvingProblemsbySearch/background61.jpg" width=792 height=612></div>
<div style="position:absolute;left:246.14px;top:76.03px" class="cls_005"><span class="cls_005">Repeated states</span></div>
<div style="position:absolute;left:40.10px;top:127.68px" class="cls_003"><span class="cls_003">Failure to detect repeated states can turn a linear problem into an exponential</span></div>
<div style="position:absolute;left:40.10px;top:152.52px" class="cls_003"><span class="cls_003">one!</span></div>
<div style="position:absolute;left:135.49px;top:194.44px" class="cls_050"><span class="cls_050">A</span></div>
<div style="position:absolute;left:431.10px;top:194.44px" class="cls_050"><span class="cls_050">A</span></div>
<div style="position:absolute;left:135.49px;top:236.21px" class="cls_050"><span class="cls_050">B</span></div>
<div style="position:absolute;left:364.55px;top:238.54px" class="cls_050"><span class="cls_050">B</span></div>
<div style="position:absolute;left:502.25px;top:238.54px" class="cls_050"><span class="cls_050">B</span></div>
<div style="position:absolute;left:135.36px;top:277.97px" class="cls_050"><span class="cls_050">C</span></div>
<div style="position:absolute;left:344.71px;top:277.97px" class="cls_050"><span class="cls_050">C</span></div>
<div style="position:absolute;left:386.48px;top:277.97px" class="cls_050"><span class="cls_050">C</span></div>
<div style="position:absolute;left:485.68px;top:277.97px" class="cls_050"><span class="cls_050">C</span></div>
<div style="position:absolute;left:527.45px;top:277.97px" class="cls_050"><span class="cls_050">C</span></div>
<div style="position:absolute;left:135.36px;top:319.74px" class="cls_050"><span class="cls_050">D</span></div>
<div style="position:absolute;left:589.58px;top:567.57px" class="cls_004"><span class="cls_004">Chapter 3</span></div>
<div style="position:absolute;left:643.54px;top:567.57px" class="cls_004"><span class="cls_004">72</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:37942px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="SolvingProblemsbySearch/background62.jpg" width=792 height=612></div>
<div style="position:absolute;left:262.34px;top:76.03px" class="cls_005"><span class="cls_005">Graph search</span></div>
<div style="position:absolute;left:56.54px;top:141.68px" class="cls_006"><span class="cls_006">function</span><span class="cls_007"> Graph-Search</span><span class="cls_008">(</span><span class="cls_009"> problem, fringe</span><span class="cls_008">)</span><span class="cls_006"> returns</span><span class="cls_008"> a solution, or failure</span></div>
<div style="position:absolute;left:78.02px;top:170.72px" class="cls_009"><span class="cls_009">closed</span><span class="cls_008"> ← an empty set</span></div>
<div style="position:absolute;left:78.02px;top:192.68px" class="cls_009"><span class="cls_009">fringe</span><span class="cls_008"> ← Insert(Make-Node(Initial-State[</span><span class="cls_009">problem</span><span class="cls_008">]),</span><span class="cls_009"> fringe</span><span class="cls_008">)</span></div>
<div style="position:absolute;left:78.02px;top:214.64px" class="cls_006"><span class="cls_006">loop do</span></div>
<div style="position:absolute;left:110.42px;top:236.48px" class="cls_006"><span class="cls_006">if</span><span class="cls_009"> fringe</span><span class="cls_008"> is empty</span><span class="cls_006"> then return</span><span class="cls_008"> failure</span></div>
<div style="position:absolute;left:110.42px;top:258.44px" class="cls_009"><span class="cls_009">node</span><span class="cls_008"> ← Remove-Front(</span><span class="cls_009">fringe</span><span class="cls_008">)</span></div>
<div style="position:absolute;left:110.42px;top:280.40px" class="cls_006"><span class="cls_006">if</span><span class="cls_008"> Goal-Test(</span><span class="cls_009">problem</span><span class="cls_008">, State[</span><span class="cls_009">node</span><span class="cls_008">])</span><span class="cls_006"> then return</span><span class="cls_009"> node</span></div>
<div style="position:absolute;left:110.42px;top:302.24px" class="cls_006"><span class="cls_006">if</span><span class="cls_008"> State[</span><span class="cls_009">node</span><span class="cls_008">] is not in</span><span class="cls_009"> closed</span><span class="cls_006"> then</span></div>
<div style="position:absolute;left:142.82px;top:324.20px" class="cls_008"><span class="cls_008">add State[</span><span class="cls_009">node</span><span class="cls_008">] to</span><span class="cls_009"> closed</span></div>
<div style="position:absolute;left:142.82px;top:346.16px" class="cls_009"><span class="cls_009">fringe</span><span class="cls_008"> ← InsertAll(Expand(</span><span class="cls_009">node</span><span class="cls_008">,</span><span class="cls_009"> problem</span><span class="cls_008">),</span><span class="cls_009"> fringe</span><span class="cls_008">)</span></div>
<div style="position:absolute;left:78.02px;top:368.00px" class="cls_006"><span class="cls_006">end</span></div>
<div style="position:absolute;left:589.58px;top:567.57px" class="cls_004"><span class="cls_004">Chapter 3</span></div>
<div style="position:absolute;left:643.54px;top:567.57px" class="cls_004"><span class="cls_004">73</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:38564px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="SolvingProblemsbySearch/background63.jpg" width=792 height=612></div>
<div style="position:absolute;left:285.86px;top:75.79px" class="cls_005"><span class="cls_005">Summary</span></div>
<div style="position:absolute;left:40.10px;top:127.44px" class="cls_003"><span class="cls_003">Problem formulation usually requires abstracting away real-world details to</span></div>
<div style="position:absolute;left:40.10px;top:152.40px" class="cls_003"><span class="cls_003">define a state space that can feasibly be explored</span></div>
<div style="position:absolute;left:40.10px;top:192.60px" class="cls_003"><span class="cls_003">Variety of uninformed search strategies</span></div>
<div style="position:absolute;left:40.10px;top:232.80px" class="cls_003"><span class="cls_003">Iterative deepening search uses only linear space</span></div>
<div style="position:absolute;left:40.10px;top:257.64px" class="cls_003"><span class="cls_003">and not much more time than other uninformed algorithms</span></div>
<div style="position:absolute;left:40.10px;top:297.84px" class="cls_003"><span class="cls_003">Graph search can be exponentially more efficient than tree search</span></div>
<div style="position:absolute;left:589.58px;top:567.57px" class="cls_004"><span class="cls_004">Chapter 3</span></div>
<div style="position:absolute;left:643.54px;top:567.57px" class="cls_004"><span class="cls_004">74</span></div>
</div>

</body>
</html>
