<html>
<head><meta http-equiv=Content-Type content="text/html; charset=UTF-8">
<style type="text/css">
<!--
span.cls_002{font-family:Arial,serif;font-size:24.8px;color:rgb(43,42,41);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_002{font-family:Arial,serif;font-size:24.8px;color:rgb(43,42,41);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_003{font-family:Arial,serif;font-size:20.7px;color:rgb(43,42,41);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_003{font-family:Arial,serif;font-size:20.7px;color:rgb(43,42,41);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_004{font-family:Arial,serif;font-size:8.0px;color:rgb(43,42,41);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_004{font-family:Arial,serif;font-size:8.0px;color:rgb(43,42,41);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_005{font-family:Arial,serif;font-size:25.0px;color:rgb(43,42,41);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_005{font-family:Arial,serif;font-size:25.0px;color:rgb(43,42,41);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_006{font-family:Arial,serif;font-size:15.2px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_006{font-family:Arial,serif;font-size:15.2px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_007{font-family:Arial,serif;font-size:20.7px;color:rgb(0,0,127);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_007{font-family:Arial,serif;font-size:20.7px;color:rgb(0,0,127);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_008{font-family:Arial,serif;font-size:14.4px;color:rgb(43,42,41);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_008{font-family:Arial,serif;font-size:14.4px;color:rgb(43,42,41);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_009{font-family:Times,serif;font-size:32.6px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
div.cls_009{font-family:Times,serif;font-size:32.6px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
span.cls_010{font-family:Arial,serif;font-size:17.3px;color:rgb(0,0,127);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_010{font-family:Arial,serif;font-size:17.3px;color:rgb(0,0,127);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_011{font-family:Arial,serif;font-size:17.3px;color:rgb(178,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_011{font-family:Arial,serif;font-size:17.3px;color:rgb(178,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_012{font-family:Arial,serif;font-size:17.3px;color:rgb(43,42,41);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_012{font-family:Arial,serif;font-size:17.3px;color:rgb(43,42,41);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_013{font-family:Arial,serif;font-size:17.3px;color:rgb(0,76,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_013{font-family:Arial,serif;font-size:17.3px;color:rgb(0,76,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_014{font-family:Arial,serif;font-size:20.7px;color:rgb(127,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_014{font-family:Arial,serif;font-size:20.7px;color:rgb(127,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_015{font-family:Arial,serif;font-size:20.7px;color:rgb(0,76,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_015{font-family:Arial,serif;font-size:20.7px;color:rgb(0,76,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_016{font-family:Arial,serif;font-size:20.7px;color:rgb(178,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_016{font-family:Arial,serif;font-size:20.7px;color:rgb(178,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_017{font-family:Arial,serif;font-size:20.7px;color:rgb(229,12,128);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_017{font-family:Arial,serif;font-size:20.7px;color:rgb(229,12,128);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_018{font-family:Arial,serif;font-size:24.2px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_018{font-family:Arial,serif;font-size:24.2px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_019{font-family:Arial,serif;font-size:16.8px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_019{font-family:Arial,serif;font-size:16.8px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_020{font-family:Arial,serif;font-size:16.8px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_020{font-family:Arial,serif;font-size:16.8px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_021{font-family:Arial,serif;font-size:17.3px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_021{font-family:Arial,serif;font-size:17.3px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_022{font-family:Arial,serif;font-size:16.9px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_022{font-family:Arial,serif;font-size:16.9px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_023{font-family:Arial,serif;font-size:17.3px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_023{font-family:Arial,serif;font-size:17.3px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_025{font-family:Arial,serif;font-size:17.3px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: line-through}
div.cls_025{font-family:Arial,serif;font-size:17.3px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_024{font-family:Arial,serif;font-size:24.3px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_024{font-family:Arial,serif;font-size:24.3px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
-->
</style>
<script type="text/javascript" src="Agents/wz_jsgraphics.js"></script>
</head>
<body>
<div style="position:absolute;left:50%;margin-left:-396px;top:0px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="Agents/background01.jpg" width=792 height=612></div>
<div style="position:absolute;left:223.00px;top:195.85px" class="cls_002"><span class="cls_002">Intelligent Agents</span></div>
<div style="position:absolute;left:291.76px;top:310.38px" class="cls_003"><span class="cls_003">Chapter 2</span></div>
<div style="position:absolute;left:593.68px;top:567.87px" class="cls_004"><span class="cls_004">Chapter 2</span></div>
<div style="position:absolute;left:647.78px;top:567.87px" class="cls_004"><span class="cls_004">1</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:622px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="Agents/background02.jpg" width=792 height=612></div>
<div style="position:absolute;left:299.20px;top:76.33px" class="cls_005"><span class="cls_005">Outline</span></div>
<div style="position:absolute;left:40.00px;top:126.66px" class="cls_003"><span class="cls_003">♦ Agents and environments</span></div>
<div style="position:absolute;left:40.00px;top:166.86px" class="cls_003"><span class="cls_003">♦ Rationality</span></div>
<div style="position:absolute;left:40.00px;top:207.06px" class="cls_003"><span class="cls_003">♦ PEAS (Performance measure, Environment, Actuators, Sensors)</span></div>
<div style="position:absolute;left:40.00px;top:247.26px" class="cls_003"><span class="cls_003">♦ Environment types</span></div>
<div style="position:absolute;left:40.00px;top:287.46px" class="cls_003"><span class="cls_003">♦ Agent types</span></div>
<div style="position:absolute;left:593.68px;top:567.87px" class="cls_004"><span class="cls_004">Chapter 2</span></div>
<div style="position:absolute;left:647.78px;top:567.87px" class="cls_004"><span class="cls_004">2</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:1244px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="Agents/background03.jpg" width=792 height=612></div>
<div style="position:absolute;left:186.28px;top:76.33px" class="cls_005"><span class="cls_005">Agents and environments</span></div>
<div style="position:absolute;left:376.34px;top:117.12px" class="cls_006"><span class="cls_006">sensors</span></div>
<div style="position:absolute;left:276.70px;top:158.01px" class="cls_006"><span class="cls_006">percepts</span></div>
<div style="position:absolute;left:459.05px;top:180.48px" class="cls_006"><span class="cls_006">?</span></div>
<div style="position:absolute;left:160.25px;top:194.05px" class="cls_006"><span class="cls_006">environment</span></div>
<div style="position:absolute;left:462.34px;top:206.99px" class="cls_006"><span class="cls_006">agent</span></div>
<div style="position:absolute;left:285.90px;top:219.20px" class="cls_006"><span class="cls_006">actions</span></div>
<div style="position:absolute;left:296.33px;top:277.43px" class="cls_006"><span class="cls_006">actuators</span></div>
<div style="position:absolute;left:40.00px;top:318.06px" class="cls_007"><span class="cls_007">Agents</span><span class="cls_003"> include humans, robots, softbots, thermostats, etc.</span></div>
<div style="position:absolute;left:40.00px;top:358.26px" class="cls_003"><span class="cls_003">The</span><span class="cls_007"> agent function</span><span class="cls_003"> maps from percept histories to actions:</span></div>
<div style="position:absolute;left:64.96px;top:396.25px" class="cls_003"><span class="cls_003">f :P</span><span class="cls_008"><sup>∗</sup></span><span class="cls_003"> →A</span></div>
<div style="position:absolute;left:40.00px;top:438.66px" class="cls_003"><span class="cls_003">The</span><span class="cls_007"> agent program</span><span class="cls_003"> runs on the physical</span><span class="cls_007"> architecture</span><span class="cls_003"> to produce f</span></div>
<div style="position:absolute;left:593.68px;top:567.87px" class="cls_004"><span class="cls_004">Chapter 2</span></div>
<div style="position:absolute;left:647.79px;top:567.87px" class="cls_004"><span class="cls_004">3</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:1866px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="Agents/background04.jpg" width=792 height=612></div>
<div style="position:absolute;left:206.44px;top:76.33px" class="cls_005"><span class="cls_005">Vacuum-cleaner world</span></div>
<div style="position:absolute;left:158.50px;top:139.41px" class="cls_009"><span class="cls_009">A</span></div>
<div style="position:absolute;left:354.53px;top:139.41px" class="cls_009"><span class="cls_009">B</span></div>
<div style="position:absolute;left:40.00px;top:361.74px" class="cls_003"><span class="cls_003">Percepts: location and contents, e.g., [A, Dirty]</span></div>
<div style="position:absolute;left:40.00px;top:401.94px" class="cls_003"><span class="cls_003">Actions: Lef t, Right, Suck, N oOp</span></div>
<div style="position:absolute;left:593.68px;top:567.87px" class="cls_004"><span class="cls_004">Chapter 2</span></div>
<div style="position:absolute;left:647.79px;top:567.87px" class="cls_004"><span class="cls_004">4</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:2488px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="Agents/background05.jpg" width=792 height=612></div>
<div style="position:absolute;left:194.80px;top:76.33px" class="cls_005"><span class="cls_005">A vacuum-cleaner agent</span></div>
<div style="position:absolute;left:117.88px;top:131.46px" class="cls_003"><span class="cls_003">Percept sequence</span></div>
<div style="position:absolute;left:560.32px;top:131.46px" class="cls_003"><span class="cls_003">Action</span></div>
<div style="position:absolute;left:117.88px;top:157.14px" class="cls_003"><span class="cls_003">[A, Clean]</span></div>
<div style="position:absolute;left:560.32px;top:157.14px" class="cls_003"><span class="cls_003">Right</span></div>
<div style="position:absolute;left:117.88px;top:182.10px" class="cls_003"><span class="cls_003">[A, Dirty]</span></div>
<div style="position:absolute;left:560.32px;top:182.10px" class="cls_003"><span class="cls_003">Suck</span></div>
<div style="position:absolute;left:117.88px;top:207.06px" class="cls_003"><span class="cls_003">[B, Clean]</span></div>
<div style="position:absolute;left:560.32px;top:207.06px" class="cls_003"><span class="cls_003">Lef t</span></div>
<div style="position:absolute;left:117.88px;top:231.90px" class="cls_003"><span class="cls_003">[B, Dirty]</span></div>
<div style="position:absolute;left:560.32px;top:231.90px" class="cls_003"><span class="cls_003">Suck</span></div>
<div style="position:absolute;left:117.88px;top:256.86px" class="cls_003"><span class="cls_003">[A, Clean], [A, Clean]</span></div>
<div style="position:absolute;left:560.32px;top:256.86px" class="cls_003"><span class="cls_003">Right</span></div>
<div style="position:absolute;left:117.88px;top:281.70px" class="cls_003"><span class="cls_003">[A, Clean], [A, Dirty]</span></div>
<div style="position:absolute;left:560.32px;top:281.70px" class="cls_003"><span class="cls_003">Suck</span></div>
<div style="position:absolute;left:56.44px;top:366.50px" class="cls_010"><span class="cls_010">function</span><span class="cls_011"> Reflex-Vacuum-Agent</span><span class="cls_012">(</span><span class="cls_013"> [location,status]</span><span class="cls_012">)</span><span class="cls_010"> returns</span><span class="cls_012"> an action</span></div>
<div style="position:absolute;left:77.92px;top:395.66px" class="cls_010"><span class="cls_010">if</span><span class="cls_013"> status</span><span class="cls_012"> =</span><span class="cls_013"> Dirty</span><span class="cls_010"> then return</span><span class="cls_013"> Suck</span></div>
<div style="position:absolute;left:77.92px;top:417.62px" class="cls_010"><span class="cls_010">else if</span><span class="cls_013"> location</span><span class="cls_012"> =</span><span class="cls_013"> A</span><span class="cls_010"> then return</span><span class="cls_013"> Right</span></div>
<div style="position:absolute;left:77.92px;top:439.46px" class="cls_010"><span class="cls_010">else if</span><span class="cls_013"> location</span><span class="cls_012"> =</span><span class="cls_013"> B</span><span class="cls_010"> then return</span><span class="cls_013"> Left</span></div>
<div style="position:absolute;left:40.00px;top:491.58px" class="cls_003"><span class="cls_003">What is the</span><span class="cls_014"> right</span><span class="cls_003"> function?</span></div>
<div style="position:absolute;left:40.00px;top:516.54px" class="cls_003"><span class="cls_003">Can it be implemented in a small agent program?</span></div>
<div style="position:absolute;left:593.68px;top:567.87px" class="cls_004"><span class="cls_004">Chapter 2</span></div>
<div style="position:absolute;left:647.78px;top:567.87px" class="cls_004"><span class="cls_004">5</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:3110px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="Agents/background06.jpg" width=792 height=612></div>
<div style="position:absolute;left:276.04px;top:76.33px" class="cls_005"><span class="cls_005">Rationality</span></div>
<div style="position:absolute;left:40.00px;top:127.98px" class="cls_003"><span class="cls_003">Fixed</span><span class="cls_007"> performance measure</span><span class="cls_003"> evaluates the</span><span class="cls_015"> environment sequence</span></div>
<div style="position:absolute;left:68.80px;top:152.82px" class="cls_003"><span class="cls_003">- one point per square cleaned up in time T ?</span></div>
<div style="position:absolute;left:68.80px;top:177.78px" class="cls_003"><span class="cls_003">- one point per clean square per time step, minus one per move?</span></div>
<div style="position:absolute;left:68.80px;top:202.62px" class="cls_003"><span class="cls_003">- penalize for</span></div>
<div style="position:absolute;left:190.12px;top:202.62px" class="cls_003"><span class="cls_003">> k dirty squares?</span></div>
<div style="position:absolute;left:40.00px;top:242.94px" class="cls_003"><span class="cls_003">A</span><span class="cls_016"> rational agent</span><span class="cls_003"> chooses whichever action maximizes the</span><span class="cls_016"> expected</span><span class="cls_003"> value of</span></div>
<div style="position:absolute;left:40.00px;top:267.78px" class="cls_003"><span class="cls_003">the performance measure</span><span class="cls_016"> given the percept sequence to date</span></div>
<div style="position:absolute;left:40.00px;top:307.98px" class="cls_003"><span class="cls_003">Rational = omniscient</span></div>
<div style="position:absolute;left:97.60px;top:332.94px" class="cls_003"><span class="cls_003">- percepts may not supply all relevant information</span></div>
<div style="position:absolute;left:40.00px;top:357.78px" class="cls_003"><span class="cls_003">Rational = clairvoyant</span></div>
<div style="position:absolute;left:97.60px;top:382.74px" class="cls_003"><span class="cls_003">- action outcomes may not be as expected</span></div>
<div style="position:absolute;left:40.00px;top:407.58px" class="cls_003"><span class="cls_003">Hence, rational = successful</span></div>
<div style="position:absolute;left:40.00px;top:447.78px" class="cls_003"><span class="cls_003">Rational</span></div>
<div style="position:absolute;left:123.88px;top:447.78px" class="cls_003"><span class="cls_003">⇒ exploration, learning, autonomy</span></div>
<div style="position:absolute;left:593.68px;top:567.87px" class="cls_004"><span class="cls_004">Chapter 2</span></div>
<div style="position:absolute;left:647.79px;top:567.87px" class="cls_004"><span class="cls_004">6</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:3732px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="Agents/background07.jpg" width=792 height=612></div>
<div style="position:absolute;left:307.96px;top:76.09px" class="cls_005"><span class="cls_005">PEAS</span></div>
<div style="position:absolute;left:40.00px;top:126.54px" class="cls_003"><span class="cls_003">To design a rational agent, we must specify the</span><span class="cls_016"> task environment</span></div>
<div style="position:absolute;left:40.00px;top:166.74px" class="cls_003"><span class="cls_003">Consider, e.g., the task of designing an automated taxi:</span></div>
<div style="position:absolute;left:40.00px;top:206.94px" class="cls_017"><span class="cls_017">Performance measure??</span></div>
<div style="position:absolute;left:40.00px;top:247.14px" class="cls_017"><span class="cls_017">Environment??</span></div>
<div style="position:absolute;left:40.00px;top:287.34px" class="cls_017"><span class="cls_017">Actuators??</span></div>
<div style="position:absolute;left:40.00px;top:327.54px" class="cls_017"><span class="cls_017">Sensors??</span></div>
<div style="position:absolute;left:593.68px;top:567.87px" class="cls_004"><span class="cls_004">Chapter 2</span></div>
<div style="position:absolute;left:647.78px;top:567.87px" class="cls_004"><span class="cls_004">7</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:4354px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="Agents/background08.jpg" width=792 height=612></div>
<div style="position:absolute;left:307.96px;top:76.09px" class="cls_005"><span class="cls_005">PEAS</span></div>
<div style="position:absolute;left:40.00px;top:126.54px" class="cls_003"><span class="cls_003">To design a rational agent, we must specify the</span><span class="cls_016"> task environment</span></div>
<div style="position:absolute;left:40.00px;top:166.74px" class="cls_003"><span class="cls_003">Consider, e.g., the task of designing an automated taxi:</span></div>
<div style="position:absolute;left:40.00px;top:206.94px" class="cls_017"><span class="cls_017">Performance measure??</span><span class="cls_003"> safety, destination, profits, legality, comfort, . . .</span></div>
<div style="position:absolute;left:40.00px;top:247.14px" class="cls_017"><span class="cls_017">Environment??</span><span class="cls_003"> US streets/freeways, traffic, pedestrians, weather, . . .</span></div>
<div style="position:absolute;left:40.00px;top:287.34px" class="cls_017"><span class="cls_017">Actuators??</span><span class="cls_003"> steering, accelerator, brake, horn, speaker/display, . . .</span></div>
<div style="position:absolute;left:40.00px;top:327.54px" class="cls_017"><span class="cls_017">Sensors??</span><span class="cls_003"> video, accelerometers, gauges, engine sensors, keyboard, GPS, . . .</span></div>
<div style="position:absolute;left:593.68px;top:567.87px" class="cls_004"><span class="cls_004">Chapter 2</span></div>
<div style="position:absolute;left:647.78px;top:567.87px" class="cls_004"><span class="cls_004">8</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:4976px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="Agents/background09.jpg" width=792 height=612></div>
<div style="position:absolute;left:195.76px;top:76.33px" class="cls_005"><span class="cls_005">Internet shopping agent</span></div>
<div style="position:absolute;left:40.00px;top:127.98px" class="cls_017"><span class="cls_017">Performance measure??</span></div>
<div style="position:absolute;left:40.00px;top:168.18px" class="cls_017"><span class="cls_017">Environment??</span></div>
<div style="position:absolute;left:40.00px;top:208.38px" class="cls_017"><span class="cls_017">Actuators??</span></div>
<div style="position:absolute;left:40.00px;top:248.58px" class="cls_017"><span class="cls_017">Sensors??</span></div>
<div style="position:absolute;left:593.68px;top:567.87px" class="cls_004"><span class="cls_004">Chapter 2</span></div>
<div style="position:absolute;left:647.78px;top:567.87px" class="cls_004"><span class="cls_004">9</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:5598px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="Agents/background10.jpg" width=792 height=612></div>
<div style="position:absolute;left:195.76px;top:76.33px" class="cls_005"><span class="cls_005">Internet shopping agent</span></div>
<div style="position:absolute;left:40.00px;top:127.98px" class="cls_017"><span class="cls_017">Performance measure??</span><span class="cls_003"> price, quality, appropriateness, efficiency</span></div>
<div style="position:absolute;left:40.00px;top:168.18px" class="cls_017"><span class="cls_017">Environment??</span><span class="cls_003"> current and future WWW sites, vendors, shippers</span></div>
<div style="position:absolute;left:40.00px;top:208.38px" class="cls_017"><span class="cls_017">Actuators??</span><span class="cls_003"> display to user, follow URL, fill in form</span></div>
<div style="position:absolute;left:40.00px;top:248.58px" class="cls_017"><span class="cls_017">Sensors??</span><span class="cls_003"> HTML pages (text, graphics, scripts)</span></div>
<div style="position:absolute;left:589.48px;top:567.87px" class="cls_004"><span class="cls_004">Chapter 2</span></div>
<div style="position:absolute;left:643.46px;top:567.87px" class="cls_004"><span class="cls_004">10</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:6220px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="Agents/background11.jpg" width=792 height=612></div>
<div style="position:absolute;left:226.84px;top:76.33px" class="cls_005"><span class="cls_005">Environment types</span></div>
<div style="position:absolute;left:214.36px;top:131.46px" class="cls_003"><span class="cls_003">Solitaire</span></div>
<div style="position:absolute;left:305.82px;top:131.46px" class="cls_003"><span class="cls_003">Backgammon   Internet shopping   Taxi</span></div>
<div style="position:absolute;left:49.96px;top:157.14px" class="cls_017"><span class="cls_017">Observable??</span></div>
<div style="position:absolute;left:49.96px;top:182.10px" class="cls_017"><span class="cls_017">Deterministic??</span></div>
<div style="position:absolute;left:49.96px;top:207.06px" class="cls_017"><span class="cls_017">Episodic??</span></div>
<div style="position:absolute;left:49.96px;top:231.90px" class="cls_017"><span class="cls_017">Static??</span></div>
<div style="position:absolute;left:49.96px;top:256.86px" class="cls_017"><span class="cls_017">Discrete??</span></div>
<div style="position:absolute;left:49.96px;top:281.70px" class="cls_017"><span class="cls_017">Single-agent??</span></div>
<div style="position:absolute;left:589.48px;top:567.87px" class="cls_004"><span class="cls_004">Chapter 2</span></div>
<div style="position:absolute;left:643.46px;top:567.87px" class="cls_004"><span class="cls_004">11</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:6842px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="Agents/background12.jpg" width=792 height=612></div>
<div style="position:absolute;left:226.84px;top:76.33px" class="cls_005"><span class="cls_005">Environment types</span></div>
<div style="position:absolute;left:214.36px;top:131.46px" class="cls_003"><span class="cls_003">Solitaire</span></div>
<div style="position:absolute;left:305.82px;top:131.46px" class="cls_003"><span class="cls_003">Backgammon   Internet shopping   Taxi</span></div>
<div style="position:absolute;left:49.96px;top:157.14px" class="cls_017"><span class="cls_017">Observable??</span></div>
<div style="position:absolute;left:233.68px;top:157.14px" class="cls_003"><span class="cls_003">Yes</span></div>
<div style="position:absolute;left:347.08px;top:157.14px" class="cls_003"><span class="cls_003">Yes</span></div>
<div style="position:absolute;left:500.08px;top:157.14px" class="cls_003"><span class="cls_003">No</span></div>
<div style="position:absolute;left:613.36px;top:157.14px" class="cls_003"><span class="cls_003">No</span></div>
<div style="position:absolute;left:49.96px;top:182.10px" class="cls_017"><span class="cls_017">Deterministic??</span></div>
<div style="position:absolute;left:49.96px;top:207.06px" class="cls_017"><span class="cls_017">Episodic??</span></div>
<div style="position:absolute;left:49.96px;top:231.90px" class="cls_017"><span class="cls_017">Static??</span></div>
<div style="position:absolute;left:49.96px;top:256.86px" class="cls_017"><span class="cls_017">Discrete??</span></div>
<div style="position:absolute;left:49.96px;top:281.70px" class="cls_017"><span class="cls_017">Single-agent??</span></div>
<div style="position:absolute;left:589.48px;top:567.87px" class="cls_004"><span class="cls_004">Chapter 2</span></div>
<div style="position:absolute;left:643.46px;top:567.87px" class="cls_004"><span class="cls_004">12</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:7464px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="Agents/background13.jpg" width=792 height=612></div>
<div style="position:absolute;left:226.84px;top:76.33px" class="cls_005"><span class="cls_005">Environment types</span></div>
<div style="position:absolute;left:214.36px;top:131.46px" class="cls_003"><span class="cls_003">Solitaire</span></div>
<div style="position:absolute;left:305.82px;top:131.46px" class="cls_003"><span class="cls_003">Backgammon   Internet shopping   Taxi</span></div>
<div style="position:absolute;left:49.96px;top:157.14px" class="cls_017"><span class="cls_017">Observable??</span></div>
<div style="position:absolute;left:233.68px;top:157.14px" class="cls_003"><span class="cls_003">Yes</span></div>
<div style="position:absolute;left:347.08px;top:157.14px" class="cls_003"><span class="cls_003">Yes</span></div>
<div style="position:absolute;left:500.08px;top:157.14px" class="cls_003"><span class="cls_003">No</span></div>
<div style="position:absolute;left:613.36px;top:157.14px" class="cls_003"><span class="cls_003">No</span></div>
<div style="position:absolute;left:49.96px;top:182.10px" class="cls_017"><span class="cls_017">Deterministic??</span></div>
<div style="position:absolute;left:233.68px;top:182.10px" class="cls_003"><span class="cls_003">Yes</span></div>
<div style="position:absolute;left:349.00px;top:182.10px" class="cls_003"><span class="cls_003">No</span></div>
<div style="position:absolute;left:487.84px;top:182.10px" class="cls_003"><span class="cls_003">Partly</span></div>
<div style="position:absolute;left:613.39px;top:182.10px" class="cls_003"><span class="cls_003">No</span></div>
<div style="position:absolute;left:49.96px;top:207.06px" class="cls_017"><span class="cls_017">Episodic??</span></div>
<div style="position:absolute;left:49.96px;top:231.90px" class="cls_017"><span class="cls_017">Static??</span></div>
<div style="position:absolute;left:49.96px;top:256.86px" class="cls_017"><span class="cls_017">Discrete??</span></div>
<div style="position:absolute;left:49.96px;top:281.70px" class="cls_017"><span class="cls_017">Single-agent??</span></div>
<div style="position:absolute;left:589.48px;top:567.87px" class="cls_004"><span class="cls_004">Chapter 2</span></div>
<div style="position:absolute;left:643.46px;top:567.87px" class="cls_004"><span class="cls_004">13</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:8086px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="Agents/background14.jpg" width=792 height=612></div>
<div style="position:absolute;left:226.84px;top:76.33px" class="cls_005"><span class="cls_005">Environment types</span></div>
<div style="position:absolute;left:214.36px;top:131.46px" class="cls_003"><span class="cls_003">Solitaire</span></div>
<div style="position:absolute;left:305.82px;top:131.46px" class="cls_003"><span class="cls_003">Backgammon   Internet shopping   Taxi</span></div>
<div style="position:absolute;left:49.96px;top:157.14px" class="cls_017"><span class="cls_017">Observable??</span></div>
<div style="position:absolute;left:233.68px;top:157.14px" class="cls_003"><span class="cls_003">Yes</span></div>
<div style="position:absolute;left:347.08px;top:157.14px" class="cls_003"><span class="cls_003">Yes</span></div>
<div style="position:absolute;left:500.08px;top:157.14px" class="cls_003"><span class="cls_003">No</span></div>
<div style="position:absolute;left:613.36px;top:157.14px" class="cls_003"><span class="cls_003">No</span></div>
<div style="position:absolute;left:49.96px;top:182.10px" class="cls_017"><span class="cls_017">Deterministic??</span></div>
<div style="position:absolute;left:233.68px;top:182.10px" class="cls_003"><span class="cls_003">Yes</span></div>
<div style="position:absolute;left:349.00px;top:182.10px" class="cls_003"><span class="cls_003">No</span></div>
<div style="position:absolute;left:487.84px;top:182.10px" class="cls_003"><span class="cls_003">Partly</span></div>
<div style="position:absolute;left:613.39px;top:182.10px" class="cls_003"><span class="cls_003">No</span></div>
<div style="position:absolute;left:49.96px;top:207.06px" class="cls_017"><span class="cls_017">Episodic??</span></div>
<div style="position:absolute;left:235.60px;top:207.06px" class="cls_003"><span class="cls_003">No</span></div>
<div style="position:absolute;left:348.99px;top:207.06px" class="cls_003"><span class="cls_003">No</span></div>
<div style="position:absolute;left:500.07px;top:207.06px" class="cls_003"><span class="cls_003">No</span></div>
<div style="position:absolute;left:613.34px;top:207.06px" class="cls_003"><span class="cls_003">No</span></div>
<div style="position:absolute;left:49.96px;top:231.90px" class="cls_017"><span class="cls_017">Static??</span></div>
<div style="position:absolute;left:49.96px;top:256.86px" class="cls_017"><span class="cls_017">Discrete??</span></div>
<div style="position:absolute;left:49.96px;top:281.70px" class="cls_017"><span class="cls_017">Single-agent??</span></div>
<div style="position:absolute;left:589.48px;top:567.87px" class="cls_004"><span class="cls_004">Chapter 2</span></div>
<div style="position:absolute;left:643.46px;top:567.87px" class="cls_004"><span class="cls_004">14</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:8708px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="Agents/background15.jpg" width=792 height=612></div>
<div style="position:absolute;left:226.84px;top:76.33px" class="cls_005"><span class="cls_005">Environment types</span></div>
<div style="position:absolute;left:214.36px;top:131.46px" class="cls_003"><span class="cls_003">Solitaire</span></div>
<div style="position:absolute;left:305.82px;top:131.46px" class="cls_003"><span class="cls_003">Backgammon   Internet shopping   Taxi</span></div>
<div style="position:absolute;left:49.96px;top:157.14px" class="cls_017"><span class="cls_017">Observable??</span></div>
<div style="position:absolute;left:233.68px;top:157.14px" class="cls_003"><span class="cls_003">Yes</span></div>
<div style="position:absolute;left:347.08px;top:157.14px" class="cls_003"><span class="cls_003">Yes</span></div>
<div style="position:absolute;left:500.08px;top:157.14px" class="cls_003"><span class="cls_003">No</span></div>
<div style="position:absolute;left:613.36px;top:157.14px" class="cls_003"><span class="cls_003">No</span></div>
<div style="position:absolute;left:49.96px;top:182.10px" class="cls_017"><span class="cls_017">Deterministic??</span></div>
<div style="position:absolute;left:233.68px;top:182.10px" class="cls_003"><span class="cls_003">Yes</span></div>
<div style="position:absolute;left:349.00px;top:182.10px" class="cls_003"><span class="cls_003">No</span></div>
<div style="position:absolute;left:487.84px;top:182.10px" class="cls_003"><span class="cls_003">Partly</span></div>
<div style="position:absolute;left:613.39px;top:182.10px" class="cls_003"><span class="cls_003">No</span></div>
<div style="position:absolute;left:49.96px;top:207.06px" class="cls_017"><span class="cls_017">Episodic??</span></div>
<div style="position:absolute;left:235.60px;top:207.06px" class="cls_003"><span class="cls_003">No</span></div>
<div style="position:absolute;left:348.99px;top:207.06px" class="cls_003"><span class="cls_003">No</span></div>
<div style="position:absolute;left:500.07px;top:207.06px" class="cls_003"><span class="cls_003">No</span></div>
<div style="position:absolute;left:613.34px;top:207.06px" class="cls_003"><span class="cls_003">No</span></div>
<div style="position:absolute;left:49.96px;top:231.90px" class="cls_017"><span class="cls_017">Static??</span></div>
<div style="position:absolute;left:233.68px;top:231.90px" class="cls_003"><span class="cls_003">Yes</span></div>
<div style="position:absolute;left:340.96px;top:231.90px" class="cls_003"><span class="cls_003">Semi</span></div>
<div style="position:absolute;left:492.04px;top:231.90px" class="cls_003"><span class="cls_003">Semi</span></div>
<div style="position:absolute;left:613.36px;top:231.90px" class="cls_003"><span class="cls_003">No</span></div>
<div style="position:absolute;left:49.96px;top:256.86px" class="cls_017"><span class="cls_017">Discrete??</span></div>
<div style="position:absolute;left:49.96px;top:281.70px" class="cls_017"><span class="cls_017">Single-agent??</span></div>
<div style="position:absolute;left:589.48px;top:567.87px" class="cls_004"><span class="cls_004">Chapter 2</span></div>
<div style="position:absolute;left:643.46px;top:567.87px" class="cls_004"><span class="cls_004">15</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:9330px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="Agents/background16.jpg" width=792 height=612></div>
<div style="position:absolute;left:226.84px;top:76.33px" class="cls_005"><span class="cls_005">Environment types</span></div>
<div style="position:absolute;left:214.36px;top:131.46px" class="cls_003"><span class="cls_003">Solitaire</span></div>
<div style="position:absolute;left:305.82px;top:131.46px" class="cls_003"><span class="cls_003">Backgammon   Internet shopping   Taxi</span></div>
<div style="position:absolute;left:49.96px;top:157.14px" class="cls_017"><span class="cls_017">Observable??</span></div>
<div style="position:absolute;left:233.68px;top:157.14px" class="cls_003"><span class="cls_003">Yes</span></div>
<div style="position:absolute;left:347.08px;top:157.14px" class="cls_003"><span class="cls_003">Yes</span></div>
<div style="position:absolute;left:500.08px;top:157.14px" class="cls_003"><span class="cls_003">No</span></div>
<div style="position:absolute;left:613.36px;top:157.14px" class="cls_003"><span class="cls_003">No</span></div>
<div style="position:absolute;left:49.96px;top:182.10px" class="cls_017"><span class="cls_017">Deterministic??</span></div>
<div style="position:absolute;left:233.68px;top:182.10px" class="cls_003"><span class="cls_003">Yes</span></div>
<div style="position:absolute;left:349.00px;top:182.10px" class="cls_003"><span class="cls_003">No</span></div>
<div style="position:absolute;left:487.84px;top:182.10px" class="cls_003"><span class="cls_003">Partly</span></div>
<div style="position:absolute;left:613.39px;top:182.10px" class="cls_003"><span class="cls_003">No</span></div>
<div style="position:absolute;left:49.96px;top:207.06px" class="cls_017"><span class="cls_017">Episodic??</span></div>
<div style="position:absolute;left:235.60px;top:207.06px" class="cls_003"><span class="cls_003">No</span></div>
<div style="position:absolute;left:348.99px;top:207.06px" class="cls_003"><span class="cls_003">No</span></div>
<div style="position:absolute;left:500.07px;top:207.06px" class="cls_003"><span class="cls_003">No</span></div>
<div style="position:absolute;left:613.34px;top:207.06px" class="cls_003"><span class="cls_003">No</span></div>
<div style="position:absolute;left:49.96px;top:231.90px" class="cls_017"><span class="cls_017">Static??</span></div>
<div style="position:absolute;left:233.68px;top:231.90px" class="cls_003"><span class="cls_003">Yes</span></div>
<div style="position:absolute;left:340.96px;top:231.90px" class="cls_003"><span class="cls_003">Semi</span></div>
<div style="position:absolute;left:492.04px;top:231.90px" class="cls_003"><span class="cls_003">Semi</span></div>
<div style="position:absolute;left:613.36px;top:231.90px" class="cls_003"><span class="cls_003">No</span></div>
<div style="position:absolute;left:49.96px;top:256.86px" class="cls_017"><span class="cls_017">Discrete??</span></div>
<div style="position:absolute;left:233.68px;top:256.86px" class="cls_003"><span class="cls_003">Yes</span></div>
<div style="position:absolute;left:347.08px;top:256.86px" class="cls_003"><span class="cls_003">Yes</span></div>
<div style="position:absolute;left:498.04px;top:256.86px" class="cls_003"><span class="cls_003">Yes</span></div>
<div style="position:absolute;left:613.37px;top:256.86px" class="cls_003"><span class="cls_003">No</span></div>
<div style="position:absolute;left:49.96px;top:281.70px" class="cls_017"><span class="cls_017">Single-agent??</span></div>
<div style="position:absolute;left:589.48px;top:567.87px" class="cls_004"><span class="cls_004">Chapter 2</span></div>
<div style="position:absolute;left:643.46px;top:567.87px" class="cls_004"><span class="cls_004">16</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:9952px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="Agents/background17.jpg" width=792 height=612></div>
<div style="position:absolute;left:226.84px;top:76.33px" class="cls_005"><span class="cls_005">Environment types</span></div>
<div style="position:absolute;left:206.20px;top:131.46px" class="cls_003"><span class="cls_003">Solitaire  Backgammon</span></div>
<div style="position:absolute;left:433.03px;top:131.46px" class="cls_003"><span class="cls_003">Internet shopping</span></div>
<div style="position:absolute;left:607.89px;top:131.46px" class="cls_003"><span class="cls_003">Taxi</span></div>
<div style="position:absolute;left:49.96px;top:157.14px" class="cls_017"><span class="cls_017">Observable??</span></div>
<div style="position:absolute;left:225.52px;top:157.14px" class="cls_003"><span class="cls_003">Yes</span></div>
<div style="position:absolute;left:330.64px;top:157.14px" class="cls_003"><span class="cls_003">Yes</span></div>
<div style="position:absolute;left:491.80px;top:157.14px" class="cls_003"><span class="cls_003">No</span></div>
<div style="position:absolute;left:613.36px;top:157.14px" class="cls_003"><span class="cls_003">No</span></div>
<div style="position:absolute;left:49.96px;top:182.10px" class="cls_017"><span class="cls_017">Deterministic??</span></div>
<div style="position:absolute;left:225.52px;top:182.10px" class="cls_003"><span class="cls_003">Yes</span></div>
<div style="position:absolute;left:332.56px;top:182.10px" class="cls_003"><span class="cls_003">No</span></div>
<div style="position:absolute;left:479.68px;top:182.10px" class="cls_003"><span class="cls_003">Partly</span></div>
<div style="position:absolute;left:613.39px;top:182.10px" class="cls_003"><span class="cls_003">No</span></div>
<div style="position:absolute;left:49.96px;top:207.06px" class="cls_017"><span class="cls_017">Episodic??</span></div>
<div style="position:absolute;left:227.44px;top:207.06px" class="cls_003"><span class="cls_003">No</span></div>
<div style="position:absolute;left:332.55px;top:207.06px" class="cls_003"><span class="cls_003">No</span></div>
<div style="position:absolute;left:491.79px;top:207.06px" class="cls_003"><span class="cls_003">No</span></div>
<div style="position:absolute;left:613.34px;top:207.06px" class="cls_003"><span class="cls_003">No</span></div>
<div style="position:absolute;left:49.96px;top:231.90px" class="cls_017"><span class="cls_017">Static??</span></div>
<div style="position:absolute;left:225.52px;top:231.90px" class="cls_003"><span class="cls_003">Yes</span></div>
<div style="position:absolute;left:324.64px;top:231.90px" class="cls_003"><span class="cls_003">Semi</span></div>
<div style="position:absolute;left:483.88px;top:231.90px" class="cls_003"><span class="cls_003">Semi</span></div>
<div style="position:absolute;left:613.36px;top:231.90px" class="cls_003"><span class="cls_003">No</span></div>
<div style="position:absolute;left:49.96px;top:256.86px" class="cls_017"><span class="cls_017">Discrete??</span></div>
<div style="position:absolute;left:225.52px;top:256.86px" class="cls_003"><span class="cls_003">Yes</span></div>
<div style="position:absolute;left:330.64px;top:256.86px" class="cls_003"><span class="cls_003">Yes</span></div>
<div style="position:absolute;left:489.88px;top:256.86px" class="cls_003"><span class="cls_003">Yes</span></div>
<div style="position:absolute;left:613.36px;top:256.86px" class="cls_003"><span class="cls_003">No</span></div>
<div style="position:absolute;left:49.96px;top:281.70px" class="cls_017"><span class="cls_017">Single-agent??</span></div>
<div style="position:absolute;left:225.52px;top:281.70px" class="cls_003"><span class="cls_003">Yes</span></div>
<div style="position:absolute;left:332.56px;top:281.70px" class="cls_003"><span class="cls_003">No</span></div>
<div style="position:absolute;left:416.56px;top:281.70px" class="cls_003"><span class="cls_003">Yes (except auctions)   No</span></div>
<div style="position:absolute;left:40.00px;top:333.66px" class="cls_014"><span class="cls_014">The environment type largely determines the agent design</span></div>
<div style="position:absolute;left:40.00px;top:373.86px" class="cls_003"><span class="cls_003">The real world is (of course) partially observable, stochastic, sequential,</span></div>
<div style="position:absolute;left:40.00px;top:398.70px" class="cls_003"><span class="cls_003">dynamic, continuous, multi-agent</span></div>
<div style="position:absolute;left:589.48px;top:567.87px" class="cls_004"><span class="cls_004">Chapter 2</span></div>
<div style="position:absolute;left:643.46px;top:567.87px" class="cls_004"><span class="cls_004">17</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:10574px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="Agents/background18.jpg" width=792 height=612></div>
<div style="position:absolute;left:270.28px;top:76.09px" class="cls_005"><span class="cls_005">Agent types</span></div>
<div style="position:absolute;left:40.00px;top:127.74px" class="cls_003"><span class="cls_003">Four basic types in order of increasing generality:</span></div>
<div style="position:absolute;left:68.80px;top:152.70px" class="cls_003"><span class="cls_003">- simple reflex agents</span></div>
<div style="position:absolute;left:68.80px;top:177.54px" class="cls_003"><span class="cls_003">- reflex agents with state</span></div>
<div style="position:absolute;left:68.80px;top:202.50px" class="cls_003"><span class="cls_003">- goal-based agents</span></div>
<div style="position:absolute;left:68.80px;top:227.34px" class="cls_003"><span class="cls_003">- utility-based agents</span></div>
<div style="position:absolute;left:40.00px;top:267.54px" class="cls_003"><span class="cls_003">All these can be turned into learning agents</span></div>
<div style="position:absolute;left:589.48px;top:567.87px" class="cls_004"><span class="cls_004">Chapter 2</span></div>
<div style="position:absolute;left:643.46px;top:567.87px" class="cls_004"><span class="cls_004">18</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:11196px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="Agents/background19.jpg" width=792 height=612></div>
<div style="position:absolute;left:220.12px;top:76.33px" class="cls_005"><span class="cls_005">Simple reflex agents</span></div>
<div style="position:absolute;left:86.68px;top:131.00px" class="cls_018"><span class="cls_018">Agent</span></div>
<div style="position:absolute;left:374.97px;top:147.07px" class="cls_019"><span class="cls_019">Sensors</span></div>
<div style="position:absolute;left:356.39px;top:195.38px" class="cls_020"><span class="cls_020">What the world</span></div>
<div style="position:absolute;left:356.39px;top:212.15px" class="cls_020"><span class="cls_020">is like now</span></div>
<div style="position:absolute;left:358.79px;top:365.54px" class="cls_020"><span class="cls_020">What action I</span></div>
<div style="position:absolute;left:113.13px;top:373.93px" class="cls_020"><span class="cls_020">Condition−action rules</span></div>
<div style="position:absolute;left:358.79px;top:382.31px" class="cls_020"><span class="cls_020">should do now</span></div>
<div style="position:absolute;left:373.17px;top:443.13px" class="cls_019"><span class="cls_019">Actuators</span></div>
<div style="position:absolute;left:589.48px;top:567.87px" class="cls_004"><span class="cls_004">Chapter 2</span></div>
<div style="position:absolute;left:643.46px;top:567.87px" class="cls_004"><span class="cls_004">19</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:11818px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="Agents/background20.jpg" width=792 height=612></div>
<div style="position:absolute;left:291.64px;top:76.33px" class="cls_005"><span class="cls_005">Example</span></div>
<div style="position:absolute;left:56.44px;top:147.98px" class="cls_010"><span class="cls_010">function</span><span class="cls_011"> Reflex-Vacuum-Agent</span><span class="cls_012">(</span><span class="cls_013"> [location,status]</span><span class="cls_012">)</span><span class="cls_010"> returns</span><span class="cls_012"> an action</span></div>
<div style="position:absolute;left:77.92px;top:177.02px" class="cls_010"><span class="cls_010">if</span><span class="cls_013"> status</span><span class="cls_012"> =</span><span class="cls_013"> Dirty</span><span class="cls_010"> then return</span><span class="cls_013"> Suck</span></div>
<div style="position:absolute;left:77.92px;top:198.98px" class="cls_010"><span class="cls_010">else if</span><span class="cls_013"> location</span><span class="cls_012"> =</span><span class="cls_013"> A</span><span class="cls_010"> then return</span><span class="cls_013"> Right</span></div>
<div style="position:absolute;left:77.92px;top:220.94px" class="cls_010"><span class="cls_010">else if</span><span class="cls_013"> location</span><span class="cls_012"> =</span><span class="cls_013"> B</span><span class="cls_010"> then return</span><span class="cls_013"> Left</span></div>
<div style="position:absolute;left:40.00px;top:284.94px" class="cls_003"><span class="cls_003">(setq joe (make-agent :name ’joe :body (make-agent-body)</span></div>
<div style="position:absolute;left:273.88px;top:309.90px" class="cls_003"><span class="cls_003">:program (make-reflex-vacuum-agent-program)))</span></div>
<div style="position:absolute;left:40.00px;top:359.70px" class="cls_003"><span class="cls_003">(defun make-reflex-vacuum-agent-program ()</span></div>
<div style="position:absolute;left:61.24px;top:384.54px" class="cls_003"><span class="cls_003">#’(lambda (percept)</span></div>
<div style="position:absolute;left:103.84px;top:409.50px" class="cls_003"><span class="cls_003">(let ((location (first percept)) (status (second percept)))</span></div>
<div style="position:absolute;left:125.08px;top:434.34px" class="cls_003"><span class="cls_003">(cond ((eq status ’dirty) ’Suck)</span></div>
<div style="position:absolute;left:188.80px;top:459.30px" class="cls_003"><span class="cls_003">((eq location ’A) ’Right)</span></div>
<div style="position:absolute;left:188.80px;top:484.26px" class="cls_003"><span class="cls_003">((eq location ’B) ’Left)))))</span></div>
<div style="position:absolute;left:589.48px;top:567.87px" class="cls_004"><span class="cls_004">Chapter 2</span></div>
<div style="position:absolute;left:643.46px;top:567.87px" class="cls_004"><span class="cls_004">20</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:12440px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="Agents/background21.jpg" width=792 height=612></div>
<div style="position:absolute;left:194.68px;top:76.33px" class="cls_005"><span class="cls_005">Reflex agents with state</span></div>
<div style="position:absolute;left:374.97px;top:147.07px" class="cls_019"><span class="cls_019">Sensors</span></div>
<div style="position:absolute;left:179.64px;top:166.62px" class="cls_020"><span class="cls_020">State</span></div>
<div style="position:absolute;left:356.39px;top:195.38px" class="cls_020"><span class="cls_020">What the world</span></div>
<div style="position:absolute;left:113.73px;top:203.76px" class="cls_020"><span class="cls_020">How the world evolves</span></div>
<div style="position:absolute;left:356.39px;top:212.15px" class="cls_020"><span class="cls_020">is like now</span></div>
<div style="position:absolute;left:122.72px;top:261.28px" class="cls_020"><span class="cls_020">What my actions do</span></div>
<div style="position:absolute;left:358.79px;top:365.54px" class="cls_020"><span class="cls_020">What action I</span></div>
<div style="position:absolute;left:113.13px;top:373.93px" class="cls_020"><span class="cls_020">Condition−action rules</span></div>
<div style="position:absolute;left:358.79px;top:382.31px" class="cls_020"><span class="cls_020">should do now</span></div>
<div style="position:absolute;left:86.68px;top:430.58px" class="cls_018"><span class="cls_018">Agent</span></div>
<div style="position:absolute;left:373.17px;top:444.26px" class="cls_019"><span class="cls_019">Actuators</span></div>
<div style="position:absolute;left:589.48px;top:567.87px" class="cls_004"><span class="cls_004">Chapter 2</span></div>
<div style="position:absolute;left:643.46px;top:567.87px" class="cls_004"><span class="cls_004">21</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:13062px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="Agents/background22.jpg" width=792 height=612></div>
<div style="position:absolute;left:291.64px;top:76.33px" class="cls_005"><span class="cls_005">Example</span></div>
<div style="position:absolute;left:56.44px;top:147.98px" class="cls_010"><span class="cls_010">function</span><span class="cls_011"> Reflex-Vacuum-Agent</span><span class="cls_012">(</span><span class="cls_013"> [location,status]</span><span class="cls_012">)</span><span class="cls_010"> returns</span><span class="cls_012"> an action</span></div>
<div style="position:absolute;left:56.44px;top:169.82px" class="cls_010"><span class="cls_010">static</span><span class="cls_012">:</span><span class="cls_013"> last A, last B</span><span class="cls_012">, numbers, initially ∞</span></div>
<div style="position:absolute;left:77.92px;top:198.98px" class="cls_010"><span class="cls_010">if</span><span class="cls_013"> status</span><span class="cls_012"> =</span><span class="cls_013"> Dirty</span><span class="cls_010"> then</span><span class="cls_012"> ...</span></div>
<div style="position:absolute;left:40.00px;top:262.98px" class="cls_003"><span class="cls_003">(defun make-reflex-vacuum-agent-with-state-program ()</span></div>
<div style="position:absolute;left:61.24px;top:287.94px" class="cls_003"><span class="cls_003">(let ((last-A infinity) (last-B infinity))</span></div>
<div style="position:absolute;left:61.24px;top:312.90px" class="cls_003"><span class="cls_003">#’(lambda (percept)</span></div>
<div style="position:absolute;left:103.84px;top:337.74px" class="cls_003"><span class="cls_003">(let ((location (first percept)) (status (second percept)))</span></div>
<div style="position:absolute;left:125.08px;top:362.70px" class="cls_003"><span class="cls_003">(incf last-A) (incf last-B)</span></div>
<div style="position:absolute;left:125.08px;top:387.54px" class="cls_003"><span class="cls_003">(cond</span></div>
<div style="position:absolute;left:135.64px;top:412.50px" class="cls_003"><span class="cls_003">((eq status ’dirty)</span></div>
<div style="position:absolute;left:146.32px;top:437.34px" class="cls_003"><span class="cls_003">(if (eq location ’A) (setq last-A 0) (setq last-B 0))</span></div>
<div style="position:absolute;left:146.32px;top:462.30px" class="cls_003"><span class="cls_003">’Suck)</span></div>
<div style="position:absolute;left:135.64px;top:487.14px" class="cls_003"><span class="cls_003">((eq location ’A) (if (> last-B 3) ’Right ’NoOp))</span></div>
<div style="position:absolute;left:135.64px;top:512.10px" class="cls_003"><span class="cls_003">((eq location ’B) (if (> last-A 3) ’Left ’NoOp)))))))</span></div>
<div style="position:absolute;left:589.48px;top:567.87px" class="cls_004"><span class="cls_004">Chapter 2</span></div>
<div style="position:absolute;left:643.46px;top:567.87px" class="cls_004"><span class="cls_004">22</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:13684px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="Agents/background23.jpg" width=792 height=612></div>
<div style="position:absolute;left:232.60px;top:76.33px" class="cls_005"><span class="cls_005">Goal-based agents</span></div>
<div style="position:absolute;left:374.97px;top:147.07px" class="cls_019"><span class="cls_019">Sensors</span></div>
<div style="position:absolute;left:179.64px;top:166.62px" class="cls_020"><span class="cls_020">State</span></div>
<div style="position:absolute;left:356.39px;top:195.38px" class="cls_020"><span class="cls_020">What the world</span></div>
<div style="position:absolute;left:113.73px;top:203.76px" class="cls_020"><span class="cls_020">How the world evolves</span></div>
<div style="position:absolute;left:356.39px;top:212.15px" class="cls_020"><span class="cls_020">is like now</span></div>
<div style="position:absolute;left:346.21px;top:252.89px" class="cls_020"><span class="cls_020">What it will be like</span></div>
<div style="position:absolute;left:122.72px;top:261.28px" class="cls_020"><span class="cls_020">What my actions do</span></div>
<div style="position:absolute;left:355.53px;top:269.67px" class="cls_020"><span class="cls_020">if I do action A</span></div>
<div style="position:absolute;left:358.79px;top:365.54px" class="cls_020"><span class="cls_020">What action I</span></div>
<div style="position:absolute;left:179.04px;top:373.93px" class="cls_020"><span class="cls_020">Goals</span></div>
<div style="position:absolute;left:358.79px;top:382.31px" class="cls_020"><span class="cls_020">should do now</span></div>
<div style="position:absolute;left:86.68px;top:430.58px" class="cls_018"><span class="cls_018">Agent</span></div>
<div style="position:absolute;left:373.17px;top:444.26px" class="cls_019"><span class="cls_019">Actuators</span></div>
<div style="position:absolute;left:589.48px;top:567.87px" class="cls_004"><span class="cls_004">Chapter 2</span></div>
<div style="position:absolute;left:643.46px;top:567.87px" class="cls_004"><span class="cls_004">23</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:14306px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="Agents/background24.jpg" width=792 height=612></div>
<div style="position:absolute;left:220.60px;top:76.33px" class="cls_005"><span class="cls_005">Utility-based agents</span></div>
<div style="position:absolute;left:374.97px;top:147.07px" class="cls_019"><span class="cls_019">Sensors</span></div>
<div style="position:absolute;left:179.64px;top:166.62px" class="cls_020"><span class="cls_020">State</span></div>
<div style="position:absolute;left:356.39px;top:195.38px" class="cls_020"><span class="cls_020">What the world</span></div>
<div style="position:absolute;left:113.73px;top:203.76px" class="cls_020"><span class="cls_020">How the world evolves</span></div>
<div style="position:absolute;left:356.39px;top:212.15px" class="cls_020"><span class="cls_020">is like now</span></div>
<div style="position:absolute;left:346.21px;top:252.89px" class="cls_020"><span class="cls_020">What it will be like</span></div>
<div style="position:absolute;left:122.72px;top:261.28px" class="cls_020"><span class="cls_020">What my actions do</span></div>
<div style="position:absolute;left:355.53px;top:269.67px" class="cls_020"><span class="cls_020">if I do action A</span></div>
<div style="position:absolute;left:341.41px;top:310.41px" class="cls_020"><span class="cls_020">How happy I will be</span></div>
<div style="position:absolute;left:179.04px;top:318.80px" class="cls_020"><span class="cls_020">Utility</span></div>
<div style="position:absolute;left:355.40px;top:327.19px" class="cls_020"><span class="cls_020">in such a state</span></div>
<div style="position:absolute;left:358.79px;top:365.54px" class="cls_020"><span class="cls_020">What action I</span></div>
<div style="position:absolute;left:358.79px;top:382.31px" class="cls_020"><span class="cls_020">should do now</span></div>
<div style="position:absolute;left:86.68px;top:430.58px" class="cls_018"><span class="cls_018">Agent</span></div>
<div style="position:absolute;left:373.17px;top:444.26px" class="cls_019"><span class="cls_019">Actuators</span></div>
<div style="position:absolute;left:589.48px;top:567.87px" class="cls_004"><span class="cls_004">Chapter 2</span></div>
<div style="position:absolute;left:643.46px;top:567.87px" class="cls_004"><span class="cls_004">24</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:14928px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="Agents/background25.jpg" width=792 height=612></div>
<div style="position:absolute;left:246.64px;top:76.33px" class="cls_005"><span class="cls_005">Learning agents</span></div>
<div style="position:absolute;left:103.08px;top:118.36px" class="cls_021"><span class="cls_021">Performance standard</span></div>
<div style="position:absolute;left:168.96px;top:185.29px" class="cls_021"><span class="cls_021">Critic</span></div>
<div style="position:absolute;left:375.76px;top:185.05px" class="cls_022"><span class="cls_022">Sensors</span></div>
<div style="position:absolute;left:103.46px;top:250.62px" class="cls_023"><span class="cls_023">feedback</span></div>
<div style="position:absolute;left:259.16px;top:296.82px" class="cls_023"><span class="cls_023">changes</span></div>
<div style="position:absolute;left:153.36px;top:318.90px" class="cls_021"><span class="cls_021">Learning</span></div>
<div style="position:absolute;left:360.29px;top:318.90px" class="cls_021"><span class="cls_021">Performance</span></div>
<div style="position:absolute;left:158.17px;top:336.18px" class="cls_021"><span class="cls_021">element</span></div>
<div style="position:absolute;left:374.70px;top:336.18px" class="cls_021"><span class="cls_021">element</span></div>
<div style="position:absolute;left:250.52px;top:348.12px" class="cls_025"><span class="cls_025">knowledge</span></div>
<div style="position:absolute;left:116.24px;top:368.89px" class="cls_023"><span class="cls_023">learning</span></div>
<div style="position:absolute;left:125.85px;top:386.17px" class="cls_023"><span class="cls_023">goals</span></div>
<div style="position:absolute;left:155.52px;top:428.38px" class="cls_021"><span class="cls_021">Problem</span></div>
<div style="position:absolute;left:150.72px;top:445.67px" class="cls_021"><span class="cls_021">generator</span></div>
<div style="position:absolute;left:373.96px;top:482.81px" class="cls_022"><span class="cls_022">Actuators</span></div>
<div style="position:absolute;left:80.89px;top:479.15px" class="cls_024"><span class="cls_024">Agent</span></div>
<div style="position:absolute;left:589.48px;top:567.87px" class="cls_004"><span class="cls_004">Chapter 2</span></div>
<div style="position:absolute;left:643.46px;top:567.87px" class="cls_004"><span class="cls_004">25</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:15550px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="Agents/background26.jpg" width=792 height=612></div>
<div style="position:absolute;left:285.76px;top:76.09px" class="cls_005"><span class="cls_005">Summary</span></div>
<div style="position:absolute;left:40.00px;top:127.74px" class="cls_007"><span class="cls_007">Agents</span><span class="cls_003"> interact with</span><span class="cls_007"> environments</span><span class="cls_003"> through</span><span class="cls_007"> actuators</span><span class="cls_003"> and</span><span class="cls_007"> sensors</span></div>
<div style="position:absolute;left:40.00px;top:167.94px" class="cls_003"><span class="cls_003">The</span><span class="cls_007"> agent function</span><span class="cls_003"> describes what the agent does in all circumstances</span></div>
<div style="position:absolute;left:40.00px;top:208.14px" class="cls_003"><span class="cls_003">The</span><span class="cls_007"> performance measure</span><span class="cls_003"> evaluates the environment sequence</span></div>
<div style="position:absolute;left:40.00px;top:248.34px" class="cls_003"><span class="cls_003">A</span><span class="cls_007"> perfectly rational</span><span class="cls_003"> agent maximizes expected performance</span></div>
<div style="position:absolute;left:40.00px;top:288.54px" class="cls_007"><span class="cls_007">Agent programs</span><span class="cls_003"> implement (some) agent functions</span></div>
<div style="position:absolute;left:40.00px;top:328.74px" class="cls_007"><span class="cls_007">PEAS</span><span class="cls_003"> descriptions define task environments</span></div>
<div style="position:absolute;left:40.00px;top:368.94px" class="cls_003"><span class="cls_003">Environments are categorized along several dimensions:</span></div>
<div style="position:absolute;left:97.60px;top:393.90px" class="cls_007"><span class="cls_007">observable</span><span class="cls_003">?</span><span class="cls_007"> deterministic</span><span class="cls_003">?</span><span class="cls_007"> episodic</span><span class="cls_003">?</span><span class="cls_007"> static</span><span class="cls_003">?</span><span class="cls_007"> discrete</span><span class="cls_003">?</span><span class="cls_007"> single-agent</span><span class="cls_003">?</span></div>
<div style="position:absolute;left:40.00px;top:434.10px" class="cls_003"><span class="cls_003">Several basic agent architectures exist:</span></div>
<div style="position:absolute;left:97.60px;top:458.94px" class="cls_007"><span class="cls_007">reflex</span><span class="cls_003">,</span><span class="cls_007"> reflex with state</span><span class="cls_003">,</span><span class="cls_007"> goal-based</span><span class="cls_003">,</span><span class="cls_007"> utility-based</span></div>
<div style="position:absolute;left:589.48px;top:567.87px" class="cls_004"><span class="cls_004">Chapter 2</span></div>
<div style="position:absolute;left:643.46px;top:567.87px" class="cls_004"><span class="cls_004">26</span></div>
</div>

</body>
</html>
