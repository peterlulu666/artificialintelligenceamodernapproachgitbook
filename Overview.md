<html>
<head><meta http-equiv=Content-Type content="text/html; charset=UTF-8">
<style type="text/css">
<!--
span.cls_002{font-family:"Calibri",serif;font-size:44.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_002{font-family:"Calibri",serif;font-size:44.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_003{font-family:"Calibri",serif;font-size:32.0px;color:rgb(137,136,137);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_003{font-family:"Calibri",serif;font-size:32.0px;color:rgb(137,136,137);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_004{font-family:"Calibri",serif;font-size:12.1px;color:rgb(137,136,137);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_004{font-family:"Calibri",serif;font-size:12.1px;color:rgb(137,136,137);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_005{font-family:Arial,serif;font-size:32.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_005{font-family:Arial,serif;font-size:32.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_006{font-family:"Calibri",serif;font-size:32.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_006{font-family:"Calibri",serif;font-size:32.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_007{font-family:"Calibri",serif;font-size:18.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_007{font-family:"Calibri",serif;font-size:18.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_008{font-family:Arial,serif;font-size:28.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_008{font-family:Arial,serif;font-size:28.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_009{font-family:"Calibri",serif;font-size:28.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_009{font-family:"Calibri",serif;font-size:28.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_010{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_010{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_011{font-family:"Calibri",serif;font-size:24.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_011{font-family:"Calibri",serif;font-size:24.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_012{font-family:Arial,serif;font-size:20.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_012{font-family:Arial,serif;font-size:20.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_013{font-family:"Calibri",serif;font-size:20.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_013{font-family:"Calibri",serif;font-size:20.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_014{font-family:Arial,serif;font-size:30.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_014{font-family:Arial,serif;font-size:30.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_015{font-family:"Calibri",serif;font-size:30.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_015{font-family:"Calibri",serif;font-size:30.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_016{font-family:Arial,serif;font-size:26.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_016{font-family:Arial,serif;font-size:26.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_017{font-family:"Calibri",serif;font-size:26.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_017{font-family:"Calibri",serif;font-size:26.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_018{font-family:Arial,serif;font-size:22.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_018{font-family:Arial,serif;font-size:22.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_019{font-family:"Calibri",serif;font-size:22.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_019{font-family:"Calibri",serif;font-size:22.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_020{font-family:"Calibri Bold",serif;font-size:26.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_020{font-family:"Calibri Bold",serif;font-size:26.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_021{font-family:Arial,serif;font-size:19.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_021{font-family:Arial,serif;font-size:19.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_022{font-family:"Calibri",serif;font-size:19.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_022{font-family:"Calibri",serif;font-size:19.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_023{font-family:"Cambria Math",serif;font-size:15.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_023{font-family:"Cambria Math",serif;font-size:15.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_024{font-family:"Calibri Italic",serif;font-size:22.0px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
div.cls_024{font-family:"Calibri Italic",serif;font-size:22.0px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
span.cls_025{font-family:Arial,serif;font-size:27.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_025{font-family:Arial,serif;font-size:27.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_026{font-family:"Calibri",serif;font-size:27.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_026{font-family:"Calibri",serif;font-size:27.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
-->
</style>
<script type="text/javascript" src="Overview/wz_jsgraphics.js"></script>
</head>
<body>
<div style="position:absolute;left:50%;margin-left:-360px;top:0px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="Overview/background01.jpg" width=720 height=540></div>
<div style="position:absolute;left:145.25px;top:169.84px" class="cls_002"><span class="cls_002">CSE 4308/5360 Artificial</span></div>
<div style="position:absolute;left:246.31px;top:222.88px" class="cls_002"><span class="cls_002">Intelligence I</span></div>
<div style="position:absolute;left:278.97px;top:307.60px" class="cls_003"><span class="cls_003">Introduction</span></div>
<div style="position:absolute;left:297.27px;top:506.64px" class="cls_004"><span class="cls_004">Vamsikrishna Gopikrishna</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:550px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="Overview/background02.jpg" width=720 height=540></div>
<div style="position:absolute;left:269.14px;top:37.12px" class="cls_002"><span class="cls_002">What is AI</span></div>
<div style="position:absolute;left:43.20px;top:127.60px" class="cls_005"><span class="cls_005">•</span><span class="cls_006"> What is Artificial Intelligence?</span></div>
<div style="position:absolute;left:297.27px;top:506.64px" class="cls_004"><span class="cls_004">Vamsikrishna Gopikrishna</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:1100px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="Overview/background03.jpg" width=720 height=540></div>
<div style="position:absolute;left:269.14px;top:37.12px" class="cls_002"><span class="cls_002">What is AI</span></div>
<div style="position:absolute;left:43.20px;top:127.60px" class="cls_005"><span class="cls_005">•</span><span class="cls_006"> What is Artificial Intelligence?</span></div>
<div style="position:absolute;left:94.01px;top:241.68px" class="cls_007"><span class="cls_007">Systems that think like humans</span></div>
<div style="position:absolute;left:397.49px;top:241.68px" class="cls_007"><span class="cls_007">Systems that think rationally</span></div>
<div style="position:absolute;left:101.51px;top:331.68px" class="cls_007"><span class="cls_007">Systems that act like humans</span></div>
<div style="position:absolute;left:404.99px;top:331.68px" class="cls_007"><span class="cls_007">Systems that act rationally</span></div>
<div style="position:absolute;left:297.27px;top:506.64px" class="cls_004"><span class="cls_004">Vamsikrishna Gopikrishna</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:1650px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="Overview/background04.jpg" width=720 height=540></div>
<div style="position:absolute;left:269.14px;top:37.12px" class="cls_002"><span class="cls_002">What is AI</span></div>
<div style="position:absolute;left:43.20px;top:127.60px" class="cls_005"><span class="cls_005">•</span><span class="cls_006"> Acting like Humans</span></div>
<div style="position:absolute;left:79.20px;top:172.64px" class="cls_008"><span class="cls_008">-</span><span class="cls_009"> Turing Test (Imitation Game) - Alan Turing (1950)</span></div>
<div style="position:absolute;left:115.20px;top:212.64px" class="cls_010"><span class="cls_010">•</span><span class="cls_011"> Suggested major components of AI: knowledge,</span></div>
<div style="position:absolute;left:133.20px;top:241.68px" class="cls_011"><span class="cls_011">reasoning, language understanding, learning</span></div>
<div style="position:absolute;left:115.20px;top:275.52px" class="cls_010"><span class="cls_010">•</span><span class="cls_011"> Still relevant today but not really useful</span></div>
<div style="position:absolute;left:297.27px;top:506.64px" class="cls_004"><span class="cls_004">Vamsikrishna Gopikrishna</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:2200px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="Overview/background05.jpg" width=720 height=540></div>
<div style="position:absolute;left:269.14px;top:37.12px" class="cls_002"><span class="cls_002">What is AI</span></div>
<div style="position:absolute;left:43.20px;top:127.60px" class="cls_005"><span class="cls_005">•</span><span class="cls_006"> Thinking like Humans</span></div>
<div style="position:absolute;left:79.20px;top:172.64px" class="cls_008"><span class="cls_008">-</span><span class="cls_009"> To build such a system we need to know how the</span></div>
<div style="position:absolute;left:101.70px;top:206.48px" class="cls_009"><span class="cls_009">brain works</span></div>
<div style="position:absolute;left:115.20px;top:246.48px" class="cls_010"><span class="cls_010">•</span><span class="cls_011"> What level of abstraction?</span></div>
<div style="position:absolute;left:115.20px;top:280.56px" class="cls_010"><span class="cls_010">•</span><span class="cls_011"> How to validate the system</span></div>
<div style="position:absolute;left:151.20px;top:314.56px" class="cls_012"><span class="cls_012">-</span><span class="cls_013"> Predicting and testing human behavior (Cognitive Science)</span></div>
<div style="position:absolute;left:151.20px;top:343.60px" class="cls_012"><span class="cls_012">-</span><span class="cls_013"> Identify from neurological data (Cognitive Neuroscience)</span></div>
<div style="position:absolute;left:115.20px;top:372.48px" class="cls_010"><span class="cls_010">•</span><span class="cls_011"> These are approaches now considered distinct from AI</span></div>
<div style="position:absolute;left:297.27px;top:506.64px" class="cls_004"><span class="cls_004">Vamsikrishna Gopikrishna</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:2750px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="Overview/background06.jpg" width=720 height=540></div>
<div style="position:absolute;left:269.14px;top:37.12px" class="cls_002"><span class="cls_002">What is AI</span></div>
<div style="position:absolute;left:43.20px;top:123.60px" class="cls_014"><span class="cls_014">•</span><span class="cls_015"> Thinking rationally</span></div>
<div style="position:absolute;left:79.20px;top:163.60px" class="cls_016"><span class="cls_016">-</span><span class="cls_017"> What is rational?</span></div>
<div style="position:absolute;left:115.20px;top:197.60px" class="cls_018"><span class="cls_018">•</span><span class="cls_019"> Is all human thought rational?</span></div>
<div style="position:absolute;left:79.20px;top:226.48px" class="cls_016"><span class="cls_016">-</span><span class="cls_017"> Rational thought has been studied since time of</span></div>
<div style="position:absolute;left:101.70px;top:254.56px" class="cls_017"><span class="cls_017">Aristotle</span></div>
<div style="position:absolute;left:115.20px;top:288.56px" class="cls_018"><span class="cls_018">•</span></div>
<div style="position:absolute;left:133.20px;top:288.56px" class="cls_019"><span class="cls_019">“Socrates is a man; all men are mortal; Therefore, Socrates is</span></div>
<div style="position:absolute;left:133.20px;top:312.56px" class="cls_019"><span class="cls_019">mortal”.</span></div>
<div style="position:absolute;left:79.20px;top:341.68px" class="cls_016"><span class="cls_016">-</span><span class="cls_017"> We can build systems that can, in principle, solve</span></div>
<div style="position:absolute;left:101.70px;top:370.48px" class="cls_017"><span class="cls_017">problems given in such a </span><span class="cls_020">logical</span><span class="cls_017"> notation</span></div>
<div style="position:absolute;left:115.20px;top:404.48px" class="cls_018"><span class="cls_018">•</span><span class="cls_019"> Not exactly easy to represent all problems in this way</span></div>
<div style="position:absolute;left:115.20px;top:433.52px" class="cls_018"><span class="cls_018">•</span><span class="cls_019"> This may be computationally intractable</span></div>
<div style="position:absolute;left:297.27px;top:506.64px" class="cls_004"><span class="cls_004">Vamsikrishna Gopikrishna</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:3300px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="Overview/background07.jpg" width=720 height=540></div>
<div style="position:absolute;left:269.14px;top:37.12px" class="cls_002"><span class="cls_002">What is AI</span></div>
<div style="position:absolute;left:43.20px;top:123.60px" class="cls_014"><span class="cls_014">•</span><span class="cls_015"> Acting Rationally</span></div>
<div style="position:absolute;left:79.20px;top:163.60px" class="cls_016"><span class="cls_016">-</span><span class="cls_017"> Rational Behavior: Doing the ‘right’ thing</span></div>
<div style="position:absolute;left:115.20px;top:197.60px" class="cls_018"><span class="cls_018">•</span><span class="cls_019"> How to define right?</span></div>
<div style="position:absolute;left:151.20px;top:226.52px" class="cls_021"><span class="cls_021">-</span><span class="cls_022"> Whichever maximizes Goal Payoff</span></div>
<div style="position:absolute;left:79.20px;top:251.68px" class="cls_016"><span class="cls_016">-</span><span class="cls_017"> Doesn’t necessarily involve thinking (reflex).</span></div>
<div style="position:absolute;left:115.20px;top:285.68px" class="cls_018"><span class="cls_018">•</span><span class="cls_019"> Any thinking should be in service of rational action</span></div>
<div style="position:absolute;left:79.20px;top:315.52px" class="cls_016"><span class="cls_016">-</span><span class="cls_017"> Aristotle (Nichomachean Ethics):</span></div>
<div style="position:absolute;left:115.20px;top:349.52px" class="cls_018"><span class="cls_018">•</span><span class="cls_019"> Every art and every inquiry, and similarly every action and</span></div>
<div style="position:absolute;left:133.20px;top:373.52px" class="cls_019"><span class="cls_019">pursuit, is thought to aim at some good</span></div>
<div style="position:absolute;left:79.20px;top:402.64px" class="cls_016"><span class="cls_016">-</span><span class="cls_017"> This is the approach the textbook (and therefore the</span></div>
<div style="position:absolute;left:101.70px;top:430.48px" class="cls_017"><span class="cls_017">course) will be studying.</span></div>
<div style="position:absolute;left:297.27px;top:506.64px" class="cls_004"><span class="cls_004">Vamsikrishna Gopikrishna</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:3850px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="Overview/background08.jpg" width=720 height=540></div>
<div style="position:absolute;left:228.84px;top:37.12px" class="cls_002"><span class="cls_002">Rational Agent</span></div>
<div style="position:absolute;left:43.20px;top:123.68px" class="cls_019"><span class="cls_019">An agent is an entity that perceives and acts</span></div>
<div style="position:absolute;left:43.20px;top:176.48px" class="cls_019"><span class="cls_019">This course is about designing rational agents</span></div>
<div style="position:absolute;left:43.20px;top:228.56px" class="cls_019"><span class="cls_019">Abstractly, an agent is a function from percept histories to actions:</span></div>
<div style="position:absolute;left:318.08px;top:255.48px" class="cls_019"><span class="cls_019">f : P</span><span class="cls_023"><sup>∗</sup></span><span class="cls_019"> → A</span></div>
<div style="position:absolute;left:43.20px;top:308.48px" class="cls_019"><span class="cls_019">For any given class of environments and tasks, we seek the</span></div>
<div style="position:absolute;left:43.20px;top:334.64px" class="cls_019"><span class="cls_019">agent (or class of agents) with the best performance</span></div>
<div style="position:absolute;left:43.20px;top:387.68px" class="cls_024"><span class="cls_024">Caveat: computational limitations make perfect rationality</span></div>
<div style="position:absolute;left:70.20px;top:408.56px" class="cls_024"><span class="cls_024">unachievable so design best program for given machine resources</span></div>
<div style="position:absolute;left:297.27px;top:506.64px" class="cls_004"><span class="cls_004">Vamsikrishna Gopikrishna</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:4400px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="Overview/background09.jpg" width=720 height=540></div>
<div style="position:absolute;left:202.63px;top:37.12px" class="cls_002"><span class="cls_002">Foundations of AI</span></div>
<div style="position:absolute;left:297.27px;top:506.64px" class="cls_004"><span class="cls_004">Vamsikrishna Gopikrishna</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:4950px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="Overview/background10.jpg" width=720 height=540></div>
<div style="position:absolute;left:249.73px;top:37.12px" class="cls_002"><span class="cls_002">History of AI</span></div>
<div style="position:absolute;left:297.27px;top:506.64px" class="cls_004"><span class="cls_004">Vamsikrishna Gopikrishna</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:5500px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="Overview/background11.jpg" width=720 height=540></div>
<div style="position:absolute;left:267.71px;top:37.12px" class="cls_002"><span class="cls_002">State of AI</span></div>
<div style="position:absolute;left:43.20px;top:121.56px" class="cls_025"><span class="cls_025">•</span><span class="cls_026"> Pattern Recognition/Data Mining</span></div>
<div style="position:absolute;left:43.20px;top:154.68px" class="cls_025"><span class="cls_025">•</span><span class="cls_026"> Speech Recognition</span></div>
<div style="position:absolute;left:43.20px;top:186.60px" class="cls_025"><span class="cls_025">•</span><span class="cls_026"> Spam Control</span></div>
<div style="position:absolute;left:43.20px;top:219.48px" class="cls_025"><span class="cls_025">•</span><span class="cls_026"> Autonomous planning and scheduling</span></div>
<div style="position:absolute;left:43.20px;top:251.64px" class="cls_025"><span class="cls_025">•</span><span class="cls_026"> Automated Logistics</span></div>
<div style="position:absolute;left:43.20px;top:283.56px" class="cls_025"><span class="cls_025">•</span><span class="cls_026"> Navigation</span></div>
<div style="position:absolute;left:43.20px;top:316.68px" class="cls_025"><span class="cls_025">•</span><span class="cls_026"> Robotic Vehicles</span></div>
<div style="position:absolute;left:43.20px;top:348.60px" class="cls_025"><span class="cls_025">•</span><span class="cls_026"> Game Playing</span></div>
<div style="position:absolute;left:43.20px;top:381.48px" class="cls_025"><span class="cls_025">•</span><span class="cls_026"> Robotics</span></div>
<div style="position:absolute;left:43.20px;top:413.64px" class="cls_025"><span class="cls_025">•</span><span class="cls_026"> Machine Translation</span></div>
<div style="position:absolute;left:297.27px;top:506.64px" class="cls_004"><span class="cls_004">Vamsikrishna Gopikrishna</span></div>
</div>

</body>
</html>
